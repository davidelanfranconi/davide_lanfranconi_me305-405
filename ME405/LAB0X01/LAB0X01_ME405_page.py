'''@page LAB0X01_ME405_page.py LAB0X01: Vendotron Finite State Machine

@section lAB0X01_summary Summary
The scope of this project is to write a program that behaves like a vending machine
off of the features and constraints that were derived from the state transition
diagram which can be seen in Figure 2. The Vending machine code is written so that
it does not contain any blocking code.

@section lAB0X01_design Design Requirements
The Vendotron has several drinks available to choose from, in this case we have
Cuke, Popsi, Spryte, and Dr. Pupsi, available. The vending machine also has a 
small two-line LCD text display which can display staus and promotional messages,
as well as a standard coin return button as can be found on any average vending machine. 

To give a better visualization of the vending machine please reference the figure below, which depicts Vendotron.

@image html Vendotron.jpg "Figure 1: A diagram of the Vendotron (Source: HW 0x00 Handout)"

The Vendotron has the following behavior programmed into it's logic board:
- On startup, Vendotron displays an initialization message on the LCD screen.
- At any time a coin may be inserted. When a coin is inserted, the balance 
displayed on the screen will update to reflects the current balance.
- At any time, a drink may be selected. If the current balance is sufficient, 
the Vendotron vends the desired beverage through the flap at the 
bottom of the machine and then computes the correct resulting balance. If 
the current balance is insufficient to make the purchase then the machine 
displays an "Insufficient Funds" message and then displays the price for 
the selected item.
- At any time the Eject button may be pressed, in which case the machine 
will return the full balance through the coin return in the least number of 
denominations.
- The Vendotron will prompt the user to select a second beverage if there is a
remaining balance.
- If the Vendotron is idle for a certain amount of time, it shows a 
"Have you tried Cuke? Only $1.00 for an ice cold soda." scrolling message on the LCD display.\n

@section lAB0X01_inputs User Inputs
The following inputs are available to the user to interface with Vendotron.\n
<b>Beverage Selection:</b>\n
'C' - Cuke\n
'P' - Popsi\n
'S' - Spryte\n
'D' - Dr. Pupper\n\n

<b>Insert Payment</b>\n
'0' - penny\n
'1' - nickle\n
'2' - dime\n
'3' - quarter\n
'4' - dollar bill\n
'5' - five-dollar bill\n
'6' - ten-dollar bill\n
'7' - twenty-dollar bill\n\n
 
<b>Return Change</b>\n
'E' - Eject change\n\n

If any other key is pressed, an invalid input message will be displayed.\n

@section lAB0X01_diagram Vendotron State Transition Diagram

@image html Vendotron-STD.jpg "Figure 2: Vendotron State Transition Diagram"

@section lAB0X01_documentation Documentation
The documentation for this lab can be found here: LAB0X01_ME405.py

@section lAB0X01_source Source Code 
The source code for this lab: 
https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305-405/src/master/ME405/LAB0X01/LAB0X01_ME405.py

- - -
@author Davide Lanfranconi
@copyright Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''