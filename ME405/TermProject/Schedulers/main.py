'''
@file           main.py
@brief          This script runs the priority task scheduler.
@details        This program will automatically run on the Nucleo due to the main.py filename. 
                This script adds tasks to the cotask.py list, and then runs the 
                priority-based scheduler. 
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''

from micropython import alloc_emergency_exception_buf
import gc
import cotask, task_share
from Motor_Task import Motor_Task
from TouchPad_Task import Touchpad_Task
from Encoder_Task import Encoder_Task
from UI_Task import UI_Task
from CNTRL_Task import CNTRL_Task
from Data_Task import Data_Task
from pyb import UART
import pyb

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

if __name__ == "__main__":

    # periods for: [mottask, encTask, touchtask, ctrlTask, uiTask]
    ## @brief A list of periods that specify the frequency of tasks.
    periods =      [    11,       5,         5,       11,      30] 
    
    # kMatrix: [ position    ang_position   velocity    ang_velocity ]
    ## @brief  A list of K values for the x motor.
    kxMatrix = [2.4, 2.9, 10.0, 0.08] # Metal Ball
    ## @brief  A list of K values for the x motor.
    kyMatrix = [3.5, 2.9, 13.0, 0.08] # Metal Ball
    
    # Create objects of the tasks for our specific files
    ## @brief    Object for the Touch Panel Task
    Touchpad_Task = Touchpad_Task(Debug = False)
    ## @brief    Object for theEncoder Task
    Encoder_Task      = Encoder_Task(Debug = False)
    ## @brief    Object for the Motor Task
    Motor_Task      = Motor_Task(Debug = False)
    ## @brief    Object for the Controller Task
    CNTRL_Task     = CNTRL_Task(kxMatrix, kyMatrix, Debug = False)
    ## @brief    Object for the User Interface Task
    UI_Task       = UI_Task(Debug = False)
    ## @brief    Object for the Data Collection Task
    Data_Task  = Data_Task()
    
    # Create the tasks which we will later add to the task list
    ## @brief         Defining motorTask to be added to the Task List
    Motor_Task_Obj      = cotask.Task (Motor_Task.Motor_Task_Fcn, name = 'motorTask', priority = 1, 
                                     period = periods[0], profile = True, trace = False)
    ## @brief         Defining encoderTask to be added to the Task List
    Encoder_Task_Obj    = cotask.Task (Encoder_Task.Encoder_Task_Fcn, name = 'encoderTask', priority = 1, 
                                     period = periods[1], profile = True, trace = False)
    ## @brief         Defining touchPanelTask to be added to the Task List
    Touch_Panel_Task_Obj = cotask.Task (Touchpad_Task.Touch_Panel_Fcn, name = 'touchPanelTask', priority = 1, 
                                     period = periods[2], profile = True, trace = False)
    ## @brief         Defining controllerTask to be added to the Task List
    CNTRL_Task_Obj       = cotask.Task (CNTRL_Task.CNTRL_Task_Fcn, name = 'ctrlTask', priority = 1, 
                                     period = periods[3], profile = True, trace = False)
    ## @brief         Defining uiTask to be added to the Task List
    UI_Task_Obj         = cotask.Task (UI_Task.UI_Task_Fcn, name = 'uiTask', priority = 1, 
                                     period = periods[4], profile = True, trace = False)

    # Add all tasks to the task list for the scheduler to run
    cotask.task_list.append(Motor_Task_Obj)
    cotask.task_list.append(Encoder_Task_Obj)
    cotask.task_list.append(Touch_Panel_Task_Obj)
    cotask.task_list.append(CNTRL_Task_Obj)
    cotask.task_list.append(UI_Task_Obj)

    # Run the memory garbage collector to clear memory before running.
    gc.collect ()
    
    # turn off REPL for the UART
    pyb.repl_uart(None) # disables repl on st-link usb port
    ## @brief Assign variable to UART port 2
    myuart = UART(2)
    
    print('\nTo use the ball balancing platform start by manually setting the platform level.\n'
          'Press `g` once the platform is level.\n'
          'Place the ball in the center of the platform and the autoleveling will begin automatically.\n'
          'Should the motor encounter a fault, you can reset it by press the blue User button on the Nucleo board\n'
          'Then, press `r` to reset, and `g` to start balancing again once the board has been re-leveled.\n'
          'The motors may be stopped at any time by pressing `s` and may be restarted using the same procedure as when the motors fault.\n')
    
    while True:
        try:
            cotask.task_list.pri_sched ()
        
        except KeyboardInterrupt:
            Motor_Task.Motor_Driver.disable()
            print('Ctrl-C has been pressed, exiting program!\n')
            
            print('Please wait while all the data is being transmitted to the Nucleo board.\n')
            Data_Task.Data_Task_Fcn()
            print('Data has finished transmitting and has saved successfully.\n')
            break

    # Print a table of task data and a table of shared information data
    print ('\n' + str (cotask.task_list) + '\n')
    print (task_share.show_all ())
    print ('\r\n')