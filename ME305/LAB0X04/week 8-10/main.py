'''@file        main.py
@brief          This file is what runs on the Nucleo on boot and what is used 
                as a FSM to determine what action to perform.
                
@details        This file is what is used to determine what behavior the board 
                should exhibit. When a G character is recieved, it will move 
                from the Idle state to the CollectData state, where it will enable 
                the motor driver, zero the encoder position, and print the current
                encoder position and speed. It will then run a loop for 30 seconds 
                or until the motor speed reaches the specified value. It will
                then exit the loop and go back to the Idle State. If the FSM recieves
                an S character before the specified speed is reach or 30 seconds
                pass, then it will exit the loop and go back to the Idle state.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/main.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        encoder                
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''
import shares
import utime
from pyb import UART
from array import array

Idle = 1
CollectData = 2
PrintData = 3


myuart = UART(2)
state = Idle

t = 0
n = 0

#times  = array('f', list(t/10 for t in range(301)))
#values = array('f', 301*[])

while True:
    start = utime.ticks_ms()
    if state == Idle:
        if myuart.any():
            val = myuart.readchar()
            if val == 71:
                state = CollectData
                t1 = utime.ticks_ms()
                shares.LastChar = 'G'
                print(shares.LastChar)
                
                
    
    elif state == CollectData:
        print('collect data')
        t2 = utime.ticks_diff(start, t1)/1000
        shares.MotorStart = True
        shares.Zero_Encoder = True
        print(shares.Encoder_Position)
        print(shares.Omega_Meas)
        while shares.Omega_Meas != 1000:
            if t2 == 0.1:
        #    values.append(exp(-times[n]/10)*sin(2*pi/3*times[n]))
                n += 1
                t2 = 0 
                t1 = utime.ticks_ms()
                shares.Get_Omega_Meas = True
                print(shares.Encoder_Position)                
            elif n == 301:
                shares.LastChar = 'S'
                shares.LastChar = None
                state = Idle
                m = n - 1
 
        if myuart.any():
            val = myuart.readchar()
            if val == 83:
                shares.LastChar = 'S'
                shares.LastChar = None
                state = Idle
                m = n - 1
        
   # elif state == PrintData:
    #    n = 0
     #   while n <= m:
          #  results = '{:}, {:}\r\n'.format(times[n], values[n])
      #      myuart.write(results.encode())
       #     n += 1
        #state = Idle   
        #n = 0
        #t = 0
        values = array('f', 301*[])
        times  = array('f', list(t/10 for t in range(301)))
            
            