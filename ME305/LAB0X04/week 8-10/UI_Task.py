'''@file        UI_Task.py
@brief          This file is what is used to send commands from the PC to the 
                Nucleo and decode the serial data it recieves from the Nucleo
                into data we can read and analyze to verify correct behavior.
                
@details        This file is what is used to send commands and recieve data to 
                the Nucleo and driver boards and associated connected hardware.
                I was unable to get the UI_Backend file to function correctly,
                so I did not finish writing the other commands for the Encoder
                section in time. If the file runs for more than 32 seconds 
                without an input it will close and tell the user that it timed 
                out and to rerun the function. If the user presses G or g in that
                timespan, the character G will be sent to the Nucleo, where the 
                UI_Backend will recieve it and run the closedloop controller.
                If an s or S is recieved after the G character is sent, the 
                closedloop controller will stop running and the board will go 
                back to an idle state listening for commands again.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/UI_Task.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        UI_Task                
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

import serial
import time
from sys import exit
#from matplotlib import pyplot
#from array import array
import keyboard
#import csv

last_key = None

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb, suppress = True)

#times  = array('f', 401*[])
#values = array('f', 401*[])

def getChar():
    ser.write('G'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        time.sleep(0.1)
        timeout+=1
        if timeout == 320:
            exit
    return ser.readline().decode()
    
with serial.Serial(port='COM7',baudrate=115273,timeout=1) as ser:
    while True:
        try:
            user_in = input("Enter 'g' to get data from the Nucleo or 'q' to quit\r\n>>")
            if user_in == 'g' or user_in == 'G':
                ser.write('G'.encode())
                timeout = 0
                print("Press 's' when program is running to stop it")
                while ser.in_waiting == 0:
                    time.sleep(0.1)
                    timeout+=1
                    #if user_in == 's' or user_in == 'S':
                     #   ser.write('S'.encode())
                    if last_key == 's' or last_key == 'S':
                       ser.write('S'.encode())
                    elif timeout == 350:
                        print('The program has timed out, please run again')
                        break
                    
            
            
                n = 0
                time.sleep(1)
                while ser.in_waiting != 0:
       #             dataString = ser.readline().decode()
                    #print("The string is: " + dataString)
            
                # Remove line endings
      #              strippedString = dataString.strip()
            
                # split on the commas
     #               splitStrings = dataString.split(',')
                    
 #                   times.append(float(splitStrings[0]))
 #                   values.append(float(splitStrings[1]))
    #                n += 1
                
   #             m = n - 1
                
 #               pyplot.figure()
 #               pyplot.plot(times, values)
 #               pyplot.xlabel('Time')
 #               pyplot.ylabel('Data')
             
 #               with open('data_file.csv', mode = 'w', newline = '') as data_file:
  #                  data_writer = csv.writer(data_file, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)
  #                  n = 0
   #                 data_writer.writerow(['Time', 'Position Value'] )
    #                while n <= m:
#                        data_writer.writerow([times[n], values[n]])
   #                     n += 1
    #            break
            
                    if user_in == 'q' or user_in == 'Q':
                        print('Thanks')
                        break

        except KeyboardInterrupt:
            break
# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
    

