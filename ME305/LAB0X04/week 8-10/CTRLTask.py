'''@file        CTRLTask.py
@brief          This file is what is used to interact with the motor, encoder,
                and closedloop drivers. 
                
@details        This file is what is used to interact with the driver tasks. 
                When an action or parameter is called or set from the PC user,
                the UI_Backend detects that call and relays the action to the 
                CTRLTask. From here, we can execute a variety of tasks including 
                enabling/disabling the motors, setting the motor speed for the 
                closedloop controller, and getting and displaying a variety of 
                encoder data values such as encodeer deltas, position and speed
                values. It will also zero the encoder position when that task 
                is called by the end user.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/CTRLTask.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        ctrltask                
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

import shares
import ControllerDriver
import EncoderDriver
import MotorDriver 

## This file is what is used to interact with the motor, encoder,and closedloop drivers.
#
#  Details
#  @author Davide Lanfranconi
#  @copyright Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
#  @date March 19, 2021
class CTRLTask:

    ## Constructor for the controller task
	#
	#  Detailed info on controller task constructor
    def __init__(self):
        '''@brief, prescaler  0
           @details timer can only be a 4 or an 8.
           for timer 4, pinas are b6 and b7.
           for timer 8, pins are c6 and c7'''
        self.step = 1000
        self.ClosedLoop = ControllerDriver.ClosedLoop()
        self.EncoderDriver = EncoderDriver.EncoderDriver()
        self.MotorDriver =MotorDriver.MotorDriver()
        print('Creating Controller Task')
    
    ## Runs the controller task to control motor speed based on input values and commands.
	#
	#  Detailed info on encoder run method 
    #  Depending on the state of the variables in the shares file, the task will
    #  execute a variety of tasks including enabling/disabling the motors, setting
    #  the motor speed for the closedloop controller, and getting and displaying a 
    #  variety of encoder data values such as encodeer deltas, position and speed
    #  values. It will also zero the encoder position when that task is called 
    #  by the end user.
    def run(self):
        '''@brief'''
        self.ClosedLoop.set_Kp(shares.Kp)
        self.Omega_Meas = 10*self.EncoderDriver.get_delta()
        self.ClosedLoop.run(self.step, self.Omega_Meas)
        self.MotorDriver.set_duty(self.ClosedLoop.PWM)
        self.EncoderDriver.run()
        
        shares.Encoder_Position = self.EncoderDriver.get_position()
        
        if shares.Zero_Encoder:
            self.EncoderDriver.set_position(0)
            
        if shares.Get_Delta:
            shares.Encoder_Delta = self.EncoderDriver.get_delta()
            shares.Get_Delta = False
            
        if shares.Get_Omega_Meas:
            shares.Omega_Meas = self.Omega_Meas()
            shares.Get_Omega_Meas = False
            
        if shares.Set_Omega_Des:
            shares.Omega_Des = self.step()
            print('Omega_Des set to' + self.step + 'RPM')
            shares.Set_Omega_Meas = False
        
        if shares.Motor_Start:
            self.MotorDriver.enable()
            shares.Motor_Stop = False

        if shares.Motor_Stop:
            self.MotorDriver.disable()
            shares.Motor_Start = False        
