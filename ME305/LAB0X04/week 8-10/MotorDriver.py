'''@file        MotorDriver.py
@brief          This file is what is used to define the motor driver pins, 
                enabale or disable the motor and set speed and direction of 
                rotation.
                
@details        This file is what is used to define the pins for the motor
                driver. The functions below are encapsulated in the MotorDriver 
                class. They are only called by the driver control program, 
                which is the only program that interacts with the motor driver.
                You can enable or disable the motor driver and set the duty 
                cycle desired, which in term sets the speed of the motor. If 
                the value is -1 to -100, the motor will turn Cw, otherwise if 
                the value is 1 to 100 the motor will turn CCW. If the duty param
                is set to 0, then the motor will not turn and it will remain 
                stationary.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/MotorDriver.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        motor                
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

import pyb 

## This file is what is used to define the motor driver pins, enable or disable
#  the motor and set speed and direction of rotation.
#
#  Details
#  @author Davide Lanfranconi
#  @copyright Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
#  @date March 19, 2021
class MotorDriver:
    
  	## Constructor for motor driver
	#
	#  Detailed info on motor driver constructor
    def __init__(self):
        '''@brief, prescaler  0
           @details '''
        print('Creating a motor driver')
        self.nsleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        self.IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
        self.IN4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
        self.tim3 = pyb.Timer(3, freq = 20000)
        self.t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin = self.IN3)
        self.t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin = self.IN4)
        self.IN3.low()
        self.IN4.high()
 
   	## Enables the motor driver
	#
	#  Detailed info on motor driver enable function    
    def enable(self):
        print('Enabling Motor')
        self.nsleep.high()
 
   	## Disables the motor driver
	#
	#  Detailed info on motor driver disable function  
    def disable(self):
        print('Disabling Motor')
        self.nsleep.low()
    
    ## Sets duty cycle for motor
	#
	#  Detailed info on motor driver duty cycle function
	#
	#  @param duty The desired duty cycle
    def set_duty(self, duty):
        print('Motor set to self.duty')
        if self.duty >=0 and self.duty <= 100:
            self.t3ch3.pulse_width_percent(self.duty)
            self.t3ch4.pulse_width_percent(0)
        elif self.duty < 0 and self.duty >= -100:
            self.t3ch4.pulse_width_percent(self.duty)
            self.t3ch3.pulse_width_percent(0)
        else:
            print('Please enter a valid integer between -100 and 100')