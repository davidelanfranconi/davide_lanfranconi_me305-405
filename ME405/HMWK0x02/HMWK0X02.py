'''@page       HMWK0X02.py
@brief          This file contains the initial kinematic analysis performed on the dual-axis balance system to determine the equations of motion needed for controlling the balancing platform.
@image          html SystemModel.jpg "Figure 1: The 3D model of the ball balancing platform (Source: HW 0x02 Handout)"
@image          html HM0X02-Part-1.jpg "Figure 2: Simplified schematic of 4-bar linkage OADQ"
@image          html HM0X02-Part-2.jpg "Figure 3: Kinematic expression for the motion of the ball"
@image          html HM0X02-Part-3.jpg "Figure 4: Relationship between the motor torque and moment applied to the platform"
@image          html HM0X02-Part-4A.jpg "Figure 5: Equation of motion for the ball and platform as an isolated system"
@image          html HM0X02-Part-4B.jpg "Figure 6: Equation of motion for the ball as an isolated system"
@image          html HM0X02-Part-5.jpg "Figure 7: Rearranging the EOMs from Figures 5 and 6 for the matrix"
@image          html HM0X02-Part-6.jpg "Figure 8: Analytical model of the system in matrix form"
@author         Davide Lanfranconi
@date           06/04/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved'''