'''@file        main0XFF1.py
@brief          The program starts when a G is recieved and sends data back to
                the PC when an S is recieved.
                
@details        The program below will run when a G is recieved from the 
                frontend program running on the PC console. WHhen a G is 
                recieved, it moves from the Idle state to the Collect Data state
                where it overwrites the vector array filled with zeros with the 
                input time and output position values calculated from the 
                equation. If a G is recieved, the program will compile the time
                and data vectors and will send them to the frontend program
                for plotting and CSV generation. Otherwise if the program is 
                allowed to run for all 30 seconds at the end of the time the
                program will move to the Print Data state where it will send 
                the Time and Position vector arrays to the fron end for 
                processing. In both cases, once all the data has been sent, the 
                program will move back to the Idle state to await reexecution.
                
                The program may be stopped at any time by using the 
                Ctrl+C keystroke. 
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%207/main0XFF1.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

import utime
from pyb import UART
from array import array
from math import exp, sin, pi

Idle = 1
CollectData = 2
PrintData = 3


myuart = UART(2)
state = Idle

t = 0
n = 0

times  = array('f', list(t/10 for t in range(301)))
values = array('f', 301*[])

while True:
    start = utime.ticks_ms()
    if state == Idle:
        if myuart.any():
            val = myuart.readchar()
            if val == 71:
                state = CollectData
                t1 = utime.ticks_ms()
    
    elif state == CollectData:
        t2 = utime.ticks_diff(start, t1)/1000
        if t2 == 0.1:
            values.append(exp(-times[n]/10)*sin(2*pi/3*times[n]))
            n += 1
            t2 = 0 
            t1 = utime.ticks_ms()
        elif n == 301:
            state = PrintData
            m = n - 1
        elif myuart.any():
            val = myuart.readchar()
            if val == 83:
                state = PrintData
                m = n - 1
        
    elif state == PrintData:
        n = 0
        while n <= m:
            results = '{:}, {:}\r\n'.format(times[n], values[n])
            myuart.write(results.encode())
            n += 1
        state = Idle   
        n = 0
        t = 0
        values = array('f', 301*[])
        times  = array('f', list(t/10 for t in range(301)))
            
            