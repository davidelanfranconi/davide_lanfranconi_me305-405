'''
@page TermProject_ME405_page.py Term Project: Ball Balancing Platform

@section TermProject_intro Introduction
This section contains all of the files used to develop and control our closed 
loop controller which we will be using to balance the ball on the platform.
This project was developed by Davide Lanfranconi and Anthony Vuong.\n
Shown below is the platform kit which was used in this project. The platform can 
pivot in two seperate planes and is connected via linkages to two independent motors
and their respective encoders to allow for closed-loop control of the platform. 
The ball itself rests on top of the platform which consists of a resistive touch panel
that can be used for traking the ball in the X and Y directions as well as to determine
if the ball is on the platform or not. 

@image html Platform_Image.jpg "Figure 1: The ball balancing platform used in the project." width = 40%

This project was completed over the last 4 weeks of the spring quarter, and is divided 
into several parts as can be seen below.

@section TermProject_Modeling Modeling and Theoretical Design for Platform Controller.
Before writing any lines of code we first had to do some mechanical modeling so that
we could find the gain parameters that we would later need to properly control our
ball balancing platform. The various parts for the modeling are shown below.

- @subpage HMWK0X02_ME405.py Kinetics and Kinematics modeling for system
- @subpage HMWK0X04_ME405.py Linearization of EOM's and Initial Simulation of Closed-Loop Control
- @subpage HMWK0X05_ME405.py Final Iterization to find Gain Value K for the Controller

@section TermProject_Files Ball Balancing Platform Control and Driver Sections
Shown below are all of the driver and control utilities which were used to develop
our ball balancing platform.

- @subpage Touchpad_Driver_page.py Reading from Resistive Touch Panels
- @subpage Motor_Driver_page.py Driving DC Motors with PWM and H-Bridges
- @subpage Encoder_Driver_page.py Reading from Quadrature Encoders
- @subpage IMU_Driver_page.py Reading from an Inertial Measurement Unit
- @subpage UI.py Platform User Interface Task
- @subpage Controller.py Platform Controller Task
- @subpage Task_Scheduler.py Priority Scheduling Task for Multiple Program Integration and Interaction
- @subpage shares.py Variable and Command Sharing across Multiple Programs 
- @subpage data_task.py Data Collection Task for Collecting Sensor and Parameter Data
- @subpage frontend.py  PC Frontend: Data Plotting and Post-Processing
- @subpage final_results.py Final Results: Project Results and Reflection


@section TermProject_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/

- - -
@author         Davide Lanfranconi, Anthony Vuong
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved   



@page Touchpad_Driver_page.py Touchpad Driver:
@section Touchpad_summary Summary
This file contains the driver class needed to scan across the x, y and Z axis 
for the resisitive touchpad and return all the values as a tuple.
                
@section Touchpad_instructions Reading from Resistive Touch Panels
The documentation and instructions below have been taken from the LAB0XFF term 
project instruction guide written by Charlie Refvem.\n

For many years devices with touch capabilities utilized resistive touch panels.  
Modern devices requiring multi-touch input now use capacitive touch panels, but 
resistive panels are lower cost, work well for touch interfaces with a single 
point of contact, and are simple to interface with a modern microcontroller.  
They also work with gloves and when wet. For your final project, the touch panel 
will be used to indicate the planar position of the ball on the balance platform.\n

Consider the schematic representation of a standard 4-wire resistive touch panel 
shown below in Figure 1.1. The resistive touch panel can be considered as a pair 
of potentiometers with common wipers that are adjusted by the horizontal and 
vertical contact point on the resistive touch panel. The location of the contact 
point in the horizontal direction changes the voltage divider ratio between the 
positive and negative pins associated with the horizontal direction, denoted as
xp and xm respectively. Similarly, the location of the contact point in the vertical 
direction changes the voltage divider ratio between the positive and negative pins 
associated with the vertical direction, denoted as yp and ym respectively. When 
there is no contact with the resistive touch panel, the resistor dividers become 
disconnected from one another; therefore, it is possible to detect when there 
is or is not contact with the resistive panel.\n

@image html Panel_Schematic.jpg "Figure 1.1: Schematic representation of a 4-wire resistive touch panel."

<b>1.2.1 X, Y, and Z Scanning</b>\n
A common way to read from the resistive touch panel is to scan the X, Y, and Z 
components one at a time.  Here the X component refers to the horizontal location 
of the contact point, the Y component refers to the vertical location of the 
contact point, and the Z component refers to whether or not there is contact 
with the screen.\n

<b>1.2.1.1 X Scan</b>\n
To scan the X component, the resistor divider between xp and xm must be energized. 
To do so,the pin connected to xp must be configured as a push-pull output and 
asserted high and the pin connected to xm must be configured as a push-pull 
output and asserted low. Then, the voltage at the center of the divider can be 
measured by floating yp and measuring the voltage on ym. The equivalent schematic 
for scanning the X component is shown below in Figure 1.2.

@image html X_Scan_Schematic.jpg "Figure 1.2: Schematic representation of a 4-wire resistive touch panel while scanning the X component."

<b>1.2.1.2 Y Scan</b>\n
To scan the Y component, the resistor divider between yp and ym must be energized. 
To do so,the pin connected to yp must be configured as a push-pull output and 
asserted high and the pin connected to ym must be configured as a push-pull 
output and asserted low. Then, the voltage at the center of the divider can be 
measured by floating xp and measuring the voltage on xm. 
The equivalent schematic for scanning the Y component is shown below in Figure 1.3.

@image html Y_Scan_Schematic.jpg "Figure 1.3:  Schematic representation of a 4-wire resistive touch panel while scanning the Y component."

<b>1.2.1.3 Z Scan</b>\n
To scan the Z component, the voltage at the center node must be measured while 
both resistor dividers are energized. One way to achieve this is to configure 
the pin connected to yp as a push-pull output asserted high while the pin connected 
to xm is configured as a push-pull output and asserted low. Then the voltage on 
one of the remaining pins, such as ym, can be measured to determine if there is 
contact with the screen. An equivalent schematic for scanning the Z component is 
shown below in Figure 1.4. In the case of no-contact the ym pin would read high 
as it is effectively pulled high to the value of yp. Similarly xp would read low 
as it is effectively pulled low to the value of xm.  If the voltages measured 
on either xp or ym read between high and low, then there is contact with the screen.
 
@image html Z_Scan_Schematic.jpg "Figure 1.4:  Schematic representation of a 4-wire resistive touch panel while scanning the Z component."

<b>1.2.2  Settling Period and Filtering</b>\n
In many applications a dedicated controller is used to rapidly and continually 
sample all three components from the touch panel. As part of this sampling, two 
additional features are implemented to improve the quality of the measurements: 
a settling period and a filter.

<b>1.2.2.1  Settling Period</b>\n
The resistive touch panel is modeled as a purely resistive element, but in 
reality has some capacitance and some inductance associated with the internal 
structure of the touch panel as well as the wiring between the touch panel and 
the microcontroller. Purely resistive elements have an infinitely fast settling 
time, but, due to the additional capacitance and inductance, the touchpanel will 
have a small but non-negligible settling time. The waveform shown below in Figure 
1.5 was captured with an oscilloscope and clearly illustrates the settling time 
associated with a change in output.

@image html Touchpad_Oscope_Capture.jpg "Figure 1.5:  Waveform capture of settling time on xm after scan transition to Y component.  The signal takes approximately 3.6 μs to settle."

A straightforward method to handle the settling time is to wait several μs after 
energizing any of the resistor dividers before reading from the appropriate ADC 
channel. Another method may be to over sample the components and algorithmically 
determine when the reading has settled.

<b>1.2.2.2  Filtering</b>\n
If the resistive touch panel is sampled fast enough, the readings may be improved 
by application of a filter such as a running average or a low pass filter. Filtering 
will improve the signal-to-noise ratio, but will add delay to the measurement, 
so it is important that the samples occur fast enoughthat the filter does not 
cause significant enough delay to create problems in other parts of the system.

<b>1.3 Connecting the Touchpad to the Bench-Top Kit</b>\n
Shown below is a schematic of the connection between the FPC adapter and top plate
of the balancing platform.

@image html Hookup_Schematic.jpg "Figure 1.6: Schematic representation of the connection between the FPC adapter and the top plate of the balancing platform."

The cables are connected between the touch panel and the CPU on the bench-top
kit with the following pin configuration.

@image html Panel_Pinout.jpg "Table 1.1: Touch Panel Pinout"

@section Touchpad_Design_Requirements Design Requirements

The program must be encapsulated in a class with the following functionality:
- A constructor that requires input parameters for four arbitrary pin objects 
representing xp, xm, yp, and ym as well as additional numerical parameters representing 
the width and length of the resistive touch panel and the coordinate representing 
the center of the resistive touchpanel.
- Three individual methods that scan the X, Y, and Z components respectively.
- The X and Y components should return values using the same units as the width 
and length specified in the constructor such that the readings are zero if the 
point of contact is in the middle of the touch panel.
- The Z component should return as a Boolean value representing whether or not 
something is in contact with the touch panel.
- You should confirm that your methods account for the settling time as 
described in the background section above.
- A method that reads all three components and returns their values as a tuple.
- You must be able to read from a single channel in less than 500μs and all three 
channels in less than 1500μs to guarantee that your driver works cooperatively 
with other code you willrun for your project. If you optimize your code properly 
you should be able to get the timeto measure all three channels down to less than 
500μs.

@section Touchpad_verification Touchpad Testing and Code Validation
To test if the platform was working correctly I used the ball to check that the
platform was reading the values correctly. I ran the ball along the vertical axis
and checked that only the Y values were changing. I did the same process running
the ball in the horizontal position to verify that only the X values were changing. 
I tested the Z value by checking that the statement turned True when the ball was 
on the platofrm and False when it was off the platform. I have included some pictures
of the test setup below, as well as a few screenshots of the output from the controller
when I was testing the ball. In all tests, the time between the current and previous
scan was always below 1500 μs, so that design criteria was also met.

@image html X-Location.jpg "Figure 1.7: Ball position while testing X readout"

@image html Y-Location.jpg "Figure 1.8: Ball position while testing Y readout"

@image html X-Test.jpg "Figure 1.9: Controller output while testing X readout"

@image html Y-Test.jpg "Figure 1.10: Controller output while testing Y readout"

@image html Z-Test.jpg "Figure 1.11: Controller output while testing Z readout"


@section Touchpad_documentation Documentation
Documentation of this driver can be found here: Touchpad_Driver.py

@section Touchpad_Driver_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Drivers/Touchpad_Driver/Touchpad_Driver.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved



@page Motor_Driver_page.py Motor Driver:
@section Motor_summary Summary
This file contains the driver class needed to operate the DRV8847 dual H-Bridge
and to operate the two DC motors on our table-top kit.
                
@section Motor_instructions Driving DC Motors with PWM and H-Bridges
The documentation and instructions below have been taken from the LAB0XFF term 
project instruction guide written by Charlie Refvem.\n

DC motors are simple reliable actuators that are ubiquitous in the world around 
us. As part of this exercise you will be spinning a small DC motor by applying 
pulse width modulation usually called PWM. In practice most motors are controlled 
using switching circuitry and PWM because it is cheaper, more efficient, and 
simpler than using analog circuitry.\n

PWM motor control techniques take advantage of the fact that DC Motors act like 
low-pass filters. Therefore, high frequency input signals are filtered out but 
the low frequency average value of the signal passes through. So, if the 
frequency of an applied PWM signal is large enough, the motor will only be 
effected by the average value which is equal to the duty-cycle.\n

Consider the PWM waveform shown below. About halfway through the waveform, the 
duty-cycle changes from about 40% to about 80%. From the motor’s ‘perspective’ 
this is equivalent to the applied voltage across the motor leads changing from
V= 0.4VM to V= 0.8VM, doubling the effort requested from the motor.\n

@image html PWM_Schematic.jpg "Figure 2.1: PWM duty-cycle changing from 40% to 80%."

The STM32 microcontroller on the Nucleo L476 development board that you have 
been using in this course comes equipped with several internal timer modules 
that can generate PWM with sub-microsecond precision. Your first task is to 
become familiar with generating PWM using the hardware drivers available in 
MicroPython.\n

Begin by surveying the documentation available at https://docs.micropython.org/en/latest/library/pyb.html; 
pay special attention to the class list at the bottom of the page which shows 
the available hardware drivers for our Nucleo L476.\n

You will write a motor driver class that encapsulates all of the features a
ssociated with driving one or two DC motors connected to the DRV8847motor driver 
IC that is part of the base unit that you’ve connected your Nucleo to. The base 
unit carries the DRV8847 motor driver from Texas Instruments which can be used 
to drive two DC motors or one stepper motors.\n

An excerpt from the DRV8847 datasheet, available online, is shown below. Each 
one of the chip’stwo H-bridges consists of four N-channel MOSFETs as shown in 
the right half of the diagram forthe first motor output. The first motor would 
be connected to pins OUT1 and OUT2 which are commanded by the microcontroller 
controls pins IN1, and IN2. An additional pin not shown onthe diagram,nSLEEP, 
is an active-high enable pin; the nSLEEP pin is pulled low so that the motor 
driver will be in sleep mode by default.\n

A common way of using these pins is to enable the motor by setting nSLEEP high, 
and then set IN1 low, and send a PWM signal to IN2 to power the first motor in 
one direction. Reverse the signals to IN1 and IN2 to power the motor in the other 
direction. To drive a second DC motor use pins IN3, and IN4 to control the pins
OUT3 and OUT4 respectively.\n

@image html H_Bridge_Schematic.jpg "Figure 2.2: H-bridge detail from DRV8847 datasheet."

A truth table in the DRV8847 datasheet, reproduced below, specifies the logic 
between these pins and what the chip does to the motor.\n

@image html Motor_Truth_Table.jpg "Figure 2.3: Truth table from DRV8847 datasheet depicting the control interface for the two H-bridges."

The truth table below outlines the connections between the CPU, motor driver, and motor.\n

@image html Motor_Connections.jpg "Table 2.1: Connections between the DRV8847 dual H-bridge and the STM32 microcontroller."

@section Motor_Design_Requirements Design Requirements

<b>2.3.2  Motor Driver Class</b>\n
The program must be encapsulated in a class with the following functionality:\n
- A constructor that requires input parameters to be able to control two separate
motors
- A method to enable the motor
- A method to disable the motor
- A method to set the direction and speed of the motor
- Make sure the driver can interact independently with each motor

<b>2.3.3  Fault Detection</b>\n
In addition to implementing the motor driver class as described above, you will 
also need to implement fault tolerance. The DRV8847 has an active-low pin nFAULT 
with a pull-up resistor that is asserted low by the DRV8847 when it detects a 
fault, most often caused by an over-current condition. Refer to the datasheet 
for the DRV8847 for more details on the fault detection features.\n 
Your program must handle these faults in the following manner:\n
- Trigger an external interrupt callback when the nFAULT pin goes low.
- As a result of the interrupt callback, all motor functionality must stop until 
you clear the fault with some form of deliberate user input.
- You must be able to clear the fault and resume operation without resetting 
the microcontroller.

@section Motor_verification Motor Testing and Code Validation
The motors must be set at a minimum duty cycle value of 30 to overcome friction with no load
on the shaft. \n
 \htmlonly <div style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/8EgG7yBP75Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div> \endhtmlonly


@section Motor_documentation Documentation
Documentation of this driver can be found here: Motor_Driver.py

@section Motor_Driver_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Drivers/Motor_Driver/Motor_Driver.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved

@page Encoder_Driver_page.py Encoder Driver:
@section Encoder_summary Summary
This file contains the driver class needed to measure the encoder angle and speed
for the two encoders on our table-top kit.
                
@section Encoder_instructions Reading from Quadrature Encoders
The documentation and instructions below have been taken from the LAB0XFF term 
project instruction guide written by Charlie Refvem.\n

A common way to measure the movement of a motor is with an optical encoder. 
Because most encoders can send out signals at high frequency (sometimes up to 
1MHz or so), we can’t read one with regular code; we need something faster, such 
as interrupts or dedicated hardware. Our STM32 processors have such hardware – 
timers which can read pulses from encoders and count distance and direction of 
motion. Incremental encoders send digital signals over two wires, usually in 
quadrature, as shown below; quadrature means that the two signals are about 
90◦out of phase:\n

@image html Encoder_Schematic.jpg "Figure 3.1: Sample output for a quadrature encoder rotating at constant angular velocity."

Each time the state of either encoder channel, A or B, changes, it is an indication 
that the encoder has moved one "tick." The particular state change tells the 
direction in which the encoder has moved. For example, if the state (A, B) starts 
at (0, 0), then a change to (0, 1) indicates move-ment to the right on the timing 
diagram above, while (0, 1) to (0, 0) indicates movement to the left. The motors 
in your kit have built-in magnetic encoders and are wired as shown in the table below.\n

@image html Encoder_Connections.jpg "Table 3.1: Connections between each encoder and the STM32 microcontroller"

@section Encoder_Design_Requirements Design Requirements
1. Write code which sets up a time in the encoder counting mode. Only specific 
timers will work with pins PB6 and PB7; check the datasheet for the STM32L476RG 
to determine which timer is correct. You may only use that timer’s CH1 and CH2 
channels to properly interface with quadrature encoder\n

2. Verify that timer.counter() outputs sensible numbers – i.e., CCW motion causes 
the count to either increase or decrease and CW motion causes the opposite behavior.\n

3. Write a class which encapsulates the operation of the timer to read from an 
encoder connected to arbitrary pins. Your class should have the following methods:\n
- A constructor init() which does the setup, given appropriate parameters such 
as which pins and timer to use.
- A method update() which, when called regularly, updates the recorded position 
of the encoder.
- A method getposition() which returns the most recently updated position of the encoder.
- A method setposition() which resets the position to to a specified value. This 
will be useful for zeroing out the encoder when the platform is initially leveled.
- A method getdelta() which returns the difference in recorded position between 
the two most recent calls to update().
- Any other methods which may be useful.

The get methods should simply return the value of the associated class attribute 
and the set method should simply update the associated class attribute to the 
desired value. All of the computation and interfacing with the timer should go 
in the update() method. A user should be able to create at least two objects of 
this class on different pins and different timers; ideally, the user would be 
able to specify any valid pin and timer combination to create many encoder objects. 
These two objects must be able to work at the same time without them interfering 
with one another.\n

4. After how many counts will the timer overflow? Might such overflow be a problem 
if your encoder reader is used to control a motor’s position?  A way to solve the 
problem is to read the timer rapidly and repeatedly, and each time compute the 
change, or delta, in timer count since the previous update(), and add the distance 
to a Python variable which holds the position. This position should count total 
movement and not reset for each revolution or when the timer overflows.\n

Your code must work if the timer overflows (counts above 2^16−1) or underflows 
(countsbelow zero). Modify your code so the total position moved is correct 
even in case of over and underflows by adjusting your code to ensure that the 
correct delta between readings is calculated even if the timer overflows or underflows.
 
@section Encoder_verification Encoder Testing and Code Validation
As can be seen in the figure below, the encoder behaves as expected when it overflows 
or underflows.

@image html Encoder_Verification_OF.jpg "Figure 3.2: Behavior of encoder when it overflows."
@image html Encoder_Verification_UF.jpg "Figure 3.2: Behavior of encoder when it underflows."

@section Encoder_LiveDemo Encoder Live Demo
 \htmlonly <div style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/GDwWmAl65Q4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div> \endhtmlonly

@section Encoder_documentation Documentation
Documentation of this driver can be found here: Encoder_Driver.py

@section Encoder_Driver_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Drivers/Encoder_driver/Encoder_Driver.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved

@page IMU_Driver_page.py Inertial Measurement Unit Driver:
@section IMU_summary Summary
This file contains the driver class needed to operate and read values from the
inertial measurement unit sensor on the balancing board.
                
@section IMU_instructions Reading from Quadrature Encoders
The documentation and instructions below have been taken from the LAB0XFF term 
project instruction guide written by Charlie Refvem.\n

Although we can compute relative changes in platform position from your encoders, 
we cannot determine the absolute orientation of the platform. Additionally, 
the relationship between the encoder angle and the platform angle is nonlinear. 
However, this can be overcome by choosing to integrate the BNO055 orientation 
sensor to extract absolute values directly. You can find the datasheet for the
BNO055 here:\n https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf\n
The BNO055 inertial measurement unit can be used several ways in this project:\n
- As a sensor to perform an initial homing sequence to balance the platform by 
itself before switching to feedback from the encoders during balancing operation.
- As a stand-alone sensor to use as feedback during balancing operation.
- As a secondary sensor to provide error checking or enhanced accuracy during 
balancing operation.

@section IMU_Design_Requirements Design Requirements
Develop a driver class that will allow you to interface with the BNO055 sensor. 
You can refer to your work with the MCP9808 temperature sensor as a reference 
for I2C communication.\n
Your driver must include the following methods:\n
- An initializer that takes in an already instantiated pyb.I2C object preconfigured  
in mastermode.
- A method to change the operating mode of the IMU between the various options 
described in the datasheet.
- A method to retrieve the calibration status from the IMU.
- A method to read Euler angles from the IMU to use as state measurements.
- A method to read angular velocity from the IMU to use as state measurements.

@section IMU_verification IMU Testing and Code Validation
As can be seen in the figure below, the encoder behaves as expected when it overflows 
or underflows.

@section IMU_LiveDemo Inertial Measurement Unit Live Demo
 \htmlonly <div style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/jWvzS7Hsql0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div> \endhtmlonly


@section IMU_documentation Documentation
Documentation of this driver can be found here: IMU_Driver.py

@section IMU_Driver_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Drivers/IMU_Driver/IMU_Driver.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved


@page UI.py Platform User Interface:

@section UI_overview UI Overview
The user interface for this project consits of using the `USB_VCP` module from 
the micropython library. We use this module to read keypresses, and it acts as a
direct link between the user and the controller. The VCP abbreviation stands for 
virtual comm port.\n
The methods which were used for the UI_Task.py were `USB_VCP.any()`, which determines 
if a key has been pressed, as well as if the data is ready for reading from the Nucleo. 
`USB_VCP.read()` was also used, which is used to read byte data from the virtual 
comm port object.\n  
If data is available to be read, it is read as an ASCII character. We use a filter
so that a character is only sent to the Nucleo if it is on the list of allowed characters,
otherwise it is ignored.\n
In this case we had 3 letters which were used for platform control:
       1. g, platform is balanced and ready for the ball
       2. s, stop the motors (the emergency stop)
       3. r, reset the platform for a new trial

Each letter will only function at specific stages of the ball balancing platform cycle.
The g key will only function when the platform is not moving, and before starting 
ball balancing or after reseting a motor fault.\n
The s key will on;y function if pressed when the motors are actively running.\n
the r key will only function if pressed after the motors have been stopped.\n

The system will ignore inputs that are not valid at the state the platform is in,
and for the most part the platform is meant to function in a mostly autonomous manner.

@section UI_documentation Documentation
Documentation of this driver can be found here: UI_Task.py

@section UI_Driver_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Tasks/UI_Task.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved


@page Controller.py Platform Controller:
@section controller_overview Controller Overview
The controller task in this project is the brains and head of operations for the
ball balancing platform. The controller takes in information from Shares.py and 
uses this information to make decisions of what duty cycles the motor should be set
to, or what state the finite state machine should be in.\n 

This controller uses the gain values from the modeling completed in @ref HMWK0X05_ME405.py
to implement state-space control with full state feedback allowing the controller
to determine motor duty cycles or logic states from various sensor inputs such as the encoders 
and the resisitive touchpad.\n 

The controller task has 4 individual states it could be in:
- INIT: The initialization state where the controller waits for the letter 'g' to be pressed to start balancing or for the letter 's' to be pressed to stop the platform balancing.
- BALANCE_PLATFORM: This state is where the controller waits for the ball to be detected before moving to state 3, or for the user to press 's to stop, or a motor fault to occur.
- BALANCE_BALL: This state is where the controller is actively balancing the ball, and will only go to state 4 if the motrs fault or if the user presses 's' to stop the motors.
- STOP_MOTORS: This state is where the controller is stopped after a user has pressed S, the motor has faulted, or where the platform waits for the user to press 'r' to reset it. 
  
@section controller_documentation Documentation
Documentation of this driver can be found here: CNTRL_Task.py

@section controller_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Tasks/CNTRL_Task.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved


@page Task_Scheduler.py Task Scheduler:
@section scheduler_overview Scheduler Overview
The tricky part once we have all the individual section written is combining it all
together and making it work in a smooth manner that is invisible to the end user. 
To do this we used a scheduler task to run the tasks in a specific order, which helps
prioritize what tasks need to run and maximize the limited computing power and memory
avaialble on the Nucleo./n

 
@section scheduler_FSM Finite State Diagram for the Ball Balancing Platform.
The finite state diagram shown below showcases some of the data flow and interaction 
between the tasks in the ball balancing platform project.

@image html Balancing_Platform_FSM.jpg "Figure 1: Finite State Machne for the ball balancing platform." width = 60%

@section scheduler_timing Co-Task Scheduler Timing Management
The cotask.py file contains several different utulities that can be useful for 
multi-program or script communication between multiple file. In this case, the 
priority-driven scheduler was used. This scheduler was not written by our group 
but was instead provided to us by the instructors as part of a number of files to
aid us in setting up the task management utilities. \n

The way the scheduler works is by running the tasks according to the priority specified
when we initialize the priority shceduler. All the tasks should be run according to the
specified intervals when we inizialize the scheduler. However, sometime we have programs 
that are delayed due to dropped packets or bytes, causing a cascading delay and leading 
to interference between scheduled tasks. If this interference or conflict were to happen, 
the higher priority task would run first and then the lower priority task would run second.\n

In this case, all of the tasks used in this project had the same priority of 1, and
the only task to have a lower priority of 0 was the Data_Task.py, as it would not
matter as much to the operation of the balancing platform if the data collected was
not perfect, compared to missing a reading from the touchpad or encoder, or a new duty
cycle, causing the platform to overshoot and possibly not recover.\n

When we were deciding how to prioritize the tasks, we needed to keep into account the
run time of each task, as having it run more often or faster than the task would take
to complete could lead to issues down the line. We optimized the tasks so they would run
in the smallest amount of time possible, so we could have the greatest number of cycles
for a single time span, allowing us to improve the accuracy and response time of the balancing platform.

@section scheduler_documentation Documentation
Documentation of this driver can be found here: cotask.py

@section scheduler_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Schedulers/cotask.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved


@page shares.py Data Sharing Across Tasks:
@section shares_overview Shares Variable/Command Sharing Overview  
In order for proper operation to occur, there are several variables which must be 
shared between different tasks simultaneously. These variables are used to implement
the full-state closed-loop feedback controller. Some of these varibales include the
position of the ball, encoder readigns, motor duty cycle values and fault or active states.\n

@section shares_rd_wt Reading and Writing to Variables
In order to share these variables between multiple files we made a Shares.py file whose sole
purpose and function is to share these varibales and queues. Any task which had access to
this file could read the values by using the @c get() command in the form @c Shares.Var.put(),
wherVar is the variable name we want to read. In a similar manner we can write to these variables
by using the @c put() command in the same form as shown above. 

@section shares_queues Accessing Queues
In a similar manner to the read and write commands shown above, we used a first-in, first-out
system for queues. These queues were made up of data duplicates, which would be saved
in the longest queue possible to allow for plotting of the measured values once the 
program was terminated and the Data_Task.py file ran. 

@section shares_documentation Documentation
Documentation of this driver can be found here: Shares.py

@section shares_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Schedulers/Shares.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved

  
@page data_task.py Data Collection Across Tasks and programs
@section data_Collection Collecting Data and Sending it to the PC for Post-Processing
The Nucleo does not have enough memory to run the balancing program, collect data 
and transmit it to the PC for plotting and post-processing at the same time. Because
of this, we use the Shares.py to collect the data from the various tasks and put them
in queues. Depending on what we want to plot we can select which queues to export at the
termination of the balancing program.\n

Once the program ends due to thea user stopping the program by pressing CTRL+C,  
the saved data is sent through serial to the PC_Frontend.py script to be plotted
and to generate a .csv document with the data. We can only plot a few variables 
each time we run the program due to memory constraints, so depending on what data
we wanted to collect and analyze we would specify which queue to export and process.

@section data_documentation Documentation
Documentation of this driver can be found here: Data_Task.py

@section data_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Tasks/Data_Task.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved


@page frontend.py  PC Frontend: Data Plotting and Post-Processing
@section frontend_Overview Overview
The frontend file is in charge or recieving the data that is sent by the Data_Task.py
file and processing it. It recieves the data throguh the serial port, formats it,
writes the data to a .csv file and then plots the data depending on what data is
selected and sent through the serial port. In this case, we have 4 subplots which contain
the balls position and velocity as well as the platforms angle and angular velocity over time 
for the X-axis.

@section frontend_documentation Documentation
Documentation of this driver can be found here: PC_Frontend.py

@section frontend_source Source Code 
The source code for this lab: 
https://bitbucket.org/avuong04/me405-term-project/src/master/Tasks/PC_Frontend.py

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved


@page final_results.py Final Results: Project Results and Reflection
@section final_results Project Results and Findings
Unfortunately we were unable to get the platform working correctly. Through the
use of debug flags and print statements I was able to determine that all of the
finite state machines in the various tasks are working correctly and are being 
called in the correct order by the scheduler. I did see that the nFAULT pin was
having some erratic behavior, which was a problem that was noticed by several of 
my other classmates as well, and we were advised by the professor to comment out 
that section of code for now if the motors are working correctly, which they were.
The issue that my partner and I had was getting the tuning dialed in properly.
As you will be bale to see in the video below, when the ball is set on the platform,
yhe motors do not turn. This is because the duty cycle values fall outside the maximum
parameters set for the motor, and so the motors do not turn. When I checked with my
classmates we all seemed to get similar gain values during the final closed-loop modeling 
which can be found in HMWK0X05_ME405.py. The fact that I am getting out of range duty cycle 
numbers point to it likely being either a math error in the code, or some sort of
bug which we were unfortunately unable to find. Please refer to the video below for a
live demonstration of the platform and the issues which we were encountering. 

@section TermProject_Demo Term Project Live Demonstration
 \htmlonly <div style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/HvG5G6TnvQw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div> \endhtmlonly 

@section final_reflection Term Project Conclusion and Reflection
This project has been a massive learning experience for both my partner and I.
We started the project strong and were doing a good job at meeting all of the 
intermediate deliverables, and we had everything working correctly and meeting all
of the project requirements. We ran into some issues when doing the final tuning
of the system and we were unable to get it working unfortunately, but we still
learned a lot in the process.\n\n

We learned several new ways to interface with hardware such as I2C communication 
protocols, as well as all of the key components that go into making a closed-loop
controller. The proejct covered and used many of the techniques which had been
previously covered in class and in many cases we expanded on these techn iques during the
term project.\n\n

There is definitely room for improvement on the project. We can add belt tensioners
to help us lower the friction on the motor assembly, requiring a lower duty cycle 
than 30 to move the assebmly. This would allow for far greater low speed control of 
the ball which would help us keep it centered more easily compared to when we need to 
use higher movement speeds. We could also include the readings from the inertial
measurement unit to do a calibration of the platform and to aid in error correction
of the platform in case we have an encoder or belt slip and bias a measurement.
We could also include a calibration script for the touchpad to improve the sensitivity 
and accuracy of the touchpad, allowing for greater trakign orecision of the ball.
This has been a fun term project and I will be using many of the concepts and ideas
learned in this project and class in my future career and for future mechatronics design 
projects.

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''