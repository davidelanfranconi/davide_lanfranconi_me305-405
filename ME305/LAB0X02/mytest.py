# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 08:23:25 2021

@author: 14083
"""

import pyb
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
def onButtonPressFCN(IRQ_src):
    print('Stop pushing my buttons!')
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)