# This file implements example student class

class Student:
    ''' @brief Brief description of the class
    '''
    
    def __init__(self, f_name, l_name):
        ''' @brief Brief desc of constructor
        '''
        
        self.first_name = f_name # Assigning input parameter f_name
                                 # to a class attribute, so we can
                                 # keep the data persistently, and
                                 # access it in any methods
                                 
        self.last_name = l_name

    def get_email_address(self):
        #email = self.first_name + '.' + self.last_name + '@calpoly.edu'
        email = '{first}.{last}@calpoly.edu'.format(first=self.first_name, last=self.last_name)
        
        return email