'''
@file           Motor_Task.py
@brief          This class interfaces with the Motor_Driver.py file to enable and     
                disable the motors, clear faults, and set duty cycle and direction 
                for the motors.
@details        This file will create an instance of the Motor_Driver.py class in
                order to send new duty cycles to the motors. Those duty cycles 
                will be passed in from the controller from shares.py.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''

from Motor_Driver import Motor
import Shares
from pyb import Timer
import micropython
from micropython import const


class Motor_Task:
    ''' 
     @brief    Task to call the motor drivers for setting duty cycles.
     @details  This task will be called from the priority scheduler to set the
               duty cycles. This class also will disable the motors under two 
               conditions: a fault occurs, which will cause an interrupt in the
               motor driver, or if the user presses the `s` key.
    '''
    ## @brief       The initialization state used to set up the task.
    INIT         = const(0)
    ## @brief       The main operating state where the task is sending duty cycle values to the motors.
    MOVE_MOTORS  = const(1)
    ## @brief       The FSM will move to this state when a fault is detected, where the user can then restart the driver.
    FAULT        = const(2)
    
    def __init__(self, Debug = True):
        ''' 
        @brief    Create a motor task.
        @details  The task will be called from the priority scheduler to send duty
                  cycke values to the motors through the Motor_Driver.py file. 
                  This task class can also be used to disable the motors under two
                  consitions; either there is a fault detected by the driver or
                  if a user presses the `s` key.
        @param   Debug if True will print debug statements to console.
        '''
        ## @brief           Create an instance of the motor driver.
        #  @details         This object needs the pin values for the nSLEEP, nFAULT,
        #                   IN1, IN2, OUT1, OUT2, timer number and frequency value
        #                   for the timer.
        self.Motor_Driver = Motor('PA15', 'PB2', 'PB5', 'PB1', 'PB4', 'PB0', Timer(3), 800000, Debug = False)
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug  = Debug
         
    @micropython.native
    def Motor_Task_Fcn(self):
        '''
        @brief      The FSM which runs the motor driver and task.
        @details    The FSM below runs the motor task. Once the task is initialized,
                    the FSM moves to the move_motors state where it will constantly
                    send updated duty cycle values to both motors. The FSM will
                    go to the FAULT state when a fault is detected or the 's' key 
                    is pressed, where the user can then clear the fault to restart 
                    the motor driver.
        '''
        ## @brief    Contains the information for which state the FSM is in.  
        state        = self.INIT # start in the init state
        
        while True: 
            
            if state == self.INIT:
                #if self.Motor_Driver.fault_CB:
                    #self.Motor_Driver.disable()
                    #state = self.FSM_Transition(state, self.FAULT)
                #else:
                    self.Motor_Driver.enable()
                    state = self.FSM_Transition(state, self.MOVE_MOTORS)
                
            elif state == self.MOVE_MOTORS:
                #if self.Motor_Driver.fault_CB:
                    #if self.Debug:
                        #print('Motor Fault was detected.') 
                    #self.Motor_Driver.disable()
                    #Shares.MotorFault.put(1) 
                    #state = self.FSM_Transition(state, self.FAULT)
                #else:
                    self.Motor_Driver.set_duty_M1(Shares.dutyX.get())
                    self.Motor_Driver.set_duty_M2(Shares.dutyY.get())
                
            elif state == self.FAULT:
                if not self.Motor_Driver.Fault:
                    if self.Debug:
                        print('Fault cleared and Driver was reset.') 
                    Shares.MotorFault.put(0)
                    self.Motor_Driver.enable()
                    state = self.FSM_Transition(state, self.INIT)
                    
            yield(state)

    @micropython.native
    def FSM_Transition(self, state, Next_State):      # state transition method
        '''
        @brief      This function transitions between states of the FSM.
        @details    In this function you specify the current state and what state 
                    you want to transition to and the function will transition the 
                    FSM and return the new current state.
        @param      state The current state of the system.
        @param      NextState The new state the system should transition to.
        @return     The new state of the finite state machine.            
        '''
        if self.Debug:
            print('The Motor FSM has transitioned from ' + str(state) + ' to ' + str(Next_State) +' state.')
        return Next_State               # now that transition has been declared, update state