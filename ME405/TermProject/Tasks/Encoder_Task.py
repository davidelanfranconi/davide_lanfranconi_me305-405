'''
@file           Encoder_Task.py
@brief          This class interfaces with the Encoder_Driver.py file to read and update
                the values from the two encoders.
@details        This file will create an two instances of the Encoder_Driver.py 
                class in order to read and update the positon and velocity values 
                of the two encoders. These values will be passed in and out of the
                task by Shares.py
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''

from Encoder_Driver import Encoder
import Shares
from pyb import Pin
from utime import ticks_us, ticks_diff
import micropython
from micropython import const

class Encoder_Task:
    ''' 
     @brief    Task to call the encoder driver for reading and setting current  
               angles and reading velocities.
     @details  This task will be called from the priority scheduler to read the
               current angles and angular speeds of both encoders. These values are
               passed in and out of the task via the Shares.py file.
    '''
    ## @brief         The initialization state used to set up the task.
    INIT           = const(0)
    ## @brief         The state used to zero the encoders at the start of the program.
    ZERO_ENCODERS  = const(1)
    ## @brief         The main operating state where the task is reading values from the encoders.
    READ_ENCODERS  = const(2) 

    def __init__(self, Debug = True):
        ''' 
        @brief      Create an encoder task to find position and speeds.
        @details    This task will be called from the priority scheduler to read the
                    current angles and angular speeds of both encoders. These values are
                    passed in and out of the task via the Shares.py file.
        @param      Debug if True will print debug statements to console.
       '''
       
        # create an encoder object to pass in pins and timer
        ## @brief  An instance of the encoder driver on motor 1/X
        self.encY = Encoder(Pin.cpu.C6, Pin.cpu.C7, 8, Debug = True) 
        ## @brief  An instance of the encoder driver on motor 2/Y
        self.encX = Encoder(Pin.cpu.B6, Pin.cpu.B7, 4, Debug = True)  
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug  = Debug

    @micropython.native
    def Encoder_Task_Fcn(self):
        '''
        @brief    The FSM which runs the encoder driver and task.
        @details  The FSM below runs the encoder task. Once the task is initialized,
                  the FSM moves to the ZERO_ENCODERS state where it will zero the
                  current value of both encoders as an initial starting point. The
                  FSM will then move to the READ_ENCODERS state where it uses an
                  alpha-beta filter to determine position and velocity of the platform.
        '''

        ## @brief    Contains the information for which state the FSM is in.  
        state        = self.INIT 
        ## @brief  The previous x encoder angle
        x0  = 0
        ## @brief  The previous x encoder angular velocity
        vx0 = 0
        ## @brief  The previous y encoder angle
        y0  = 0
        ## @brief  The previous y encoder angular velocity
        vy0 = 0

        while True:
            
            if state == self.INIT:
                state = self.FSM_Transition(state, self.ZERO_ENCODERS)
            
            elif state == self.ZERO_ENCODERS:
                if Shares.ZeroEncoder.get() & 1:
                    if self.Debug:
                        print('Zeroing encoders')
                    self.encX.set_position(0)
                    self.encY.set_position(0)
                    ## @brief  The previous time
                    lastTime   = ticks_us()
                    state = self.FSM_Transition(state, self.READ_ENCODERS)
            
            elif state == self.READ_ENCODERS:
                
                ## @brief   The current time in microseconds
                currentTime = ticks_us()
                
                ## @brief  The current X angle
                xAngle     = self.encX.update()
                ## @brief  The current Y angle
                yAngle     = self.encY.update()

                # Find time delta since last callculation.
                dt = ticks_diff(currentTime, lastTime)/1000000
                
                # get X Position and Velocity
                ## @brief  The current x angle
                x1         = x0 + 0.85*(xAngle - x0) + (vx0*dt)
                ## @brief  The current x angular velocity
                vx1        = vx0 + (0.005/dt)*(xAngle - x0)
                x0 = x1
                vx0 = vx1
                
                # get Y Position and Velocity
                ## @brief  The current y angle
                y1 = y0 + 0.85*(yAngle - y0) + (vy0*dt)
                ## @brief  The current y angular velocity
                vy1 = vy0 + (0.005/dt)*(yAngle - y0)
                y0 = y1
                vy0 = vy1

                # Reset lastTime variable
                lastTime = currentTime
                
                Shares.thXE.put(x1*.5217)
                Shares.thDotXE.put(vx1*.5217)
                Shares.thYE.put(y1*-.5217)
                Shares.thDotYE.put(vy1*-.5217)

                if self.Debug:
                    print('Current Angles:     {:}, {:} rad'.format(x1, y1))
                    print('Current Angular speed: {:}, {:} rad/s'.format(vx1, vy1))


            yield(state)
            
    @micropython.native
    def FSM_Transition(self, state, Next_State):      # state transition method
        '''
        @brief      This function transitions between states of the FSM.
        @details    In this function you specify the current state and what state 
                    you want to transition to and the function will transition the 
                    FSM and return the new current state.
        @param      state The current state of the system.
        @param      NextState The new state the system should transition to.
        @return     The new state of the finite state machine.            
        '''
        if self.Debug:
            print('The Encoder FSM has transitioned from ' + str(state) + ' to ' + str(Next_State) +' state.')
        return Next_State               # now that transition has been declared, update state
