"""@file        Touchpad_Driver.py
@brief          This file contains the driver class needed to scan across the x, 
                y and Z axis for the resisitive touchpad and reurn all the values
                as a tuple.
@details        This file contains the driver class needed to scan across the x,
                y and z components of the touchpad. The z axis returns a true or
                false value depending on if the ball is touching the platform or 
                not. The other two axis return a numerical value for the coordinate
                position of the ball from the pad origin. The values are returned
                as a tuple along with the time length between the current and 
                previous scan.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
"""
import pyb
import utime
import micropython
from micropython import const

class TouchPanelDriver:
    ''' @brief      This class allows the end user to determine the ball location 
                    on the platform by scanning the X, Y and Z components about every 
                    1500 microseconds. 
    '''
    def __init__(self, xm, xp, ym, yp, dx, dy, x0, y0, Debug = True):
        ''' @brief  This initializes a new resisitive touchpad object
            @param  xm Negative pin for horizontal direction
            @param  xp Positive pin for horizontal direction
            @param  ym Negative pin for vertical direction
            @param  yp Positive pin for vertical direction
            @param  dx Length of horizontal side
            @param  dy Length of vertical side
            @param  x0 Initial position along horizontal direction
            @param  y0 Initial position along vertical direction
            @param   Debug if True will print debug statements to console.
        '''
        ##@brief Negative pin for horizontal direction.
        self.xm_pin = xm
        ##@brief Positive pin for horizontal direction.
        self.xp_pin = xp
        ##@brief Negative pin for vertical direction.
        self.ym_pin = ym  
        ##@brief Positive pin for vertical direction.
        self.yp_pin = yp
        ##@brief Length of horizontal side.
        self.dx = const(dx)
        ##@brief Length of vertical side.
        self.dy = const(dy)
        ##@brief Initial position along horizontal direction.
        self.x0 = const(x0)
        ##@brief Initial position along vertical direction.
        self.y0 = const(y0)
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug  = Debug
    
    @micropython.native
    def xscan(self):
        ''' @brief      This section determines the location of the ball along  
                        the horizontal axis.
            @details    In order to scan the X component, the resistor divider 
                        between xp and xm must be energized.  To do so, the pin 
                        connected to xp must be configured as a push-pull output
                        and asserted high and the pin connected to xm must be 
                        configured as a push-pull output and asserted low. Then, 
                        the voltage at the center of the divider can be measured 
                        by floating yp and measuring the voltage on ym. 
            @return     A Boolean value, which will state the location of the ball
                        along the horizontal axis relative to the origin.
        '''
        ##@brief Current negative voltage reading along horizontal axis
        self.xm = pyb.Pin(self.xm_pin, mode = pyb.Pin.OUT_PP)
        ##@brief Current positive voltage reading along horizontal axis
        self.xp = pyb.Pin(self.xp_pin, mode = pyb.Pin.OUT_PP)
        ##@brief Current negative voltage reading along vertical axis
        self.ym = pyb.Pin(self.ym_pin, mode = pyb.Pin.IN)
        ##@brief Current positive voltage reading along vertical axis
        self.yp = pyb.ADC(self.yp_pin)
        self.xm.low()
        self.xp.high()
        self.ym.low()
        ##@brief Current horizontal position.
        self.x = (self.yp.read() - self.x0) / self.dx

    @micropython.native        
    def yscan(self):
        ''' @brief      This section determines the location of the ball along  
                        the vertical axis.
            @details    In order to scan the Y component, the resistor divider 
                        between yp and ym must be energized.  To do so, the pin 
                        connected to yp must be configured as a push-pull output
                        and asserted high and the pin connected to ym must be 
                        configured as a push-pull output and asserted low. Then, 
                        the voltage at the center of the divider can be measured 
                        by floating xp and measuring the voltage on xm. 
            @return     A Boolean value, which will state the location of the ball
                        along the vertical axis relative to the origin.
        '''
        self.xm = pyb.Pin(self.xm_pin, mode = pyb.Pin.IN)
        self.xp = pyb.ADC(self.xp_pin)
        self.ym = pyb.Pin(self.ym_pin, mode = pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_pin, mode = pyb.Pin.OUT_PP)
        self.xm.low()
        self.ym.low()
        self.yp.high()
        ##@brief Current vertical position.
        self.y = (self.xp.read() - self.y0) / self.dy

    @micropython.native        
    def zscan(self):
        ''' @brief      This section determines the location of the ball along  
                        the horizontal axis.
            @details    To scan the Z component, the voltage at the center node 
                        must be measured while both resistor dividers are energized.
                        One way to achieve this is to configure the pin connected 
                        to yp as a push-pull output asserted high while the pin 
                        connected to xm is configured as a push-pull output and
                        asserted low. Then the voltage on one of the remaining pins,
                        such as ym, can be measured to determine if there is 
                        contact with the screen. In the case of no-contact the
                        ym pin would read high as it is effectively pulled high 
                        to the value of yp.  Similarly xp would read low as it
                        is effectively pulled low to the value of xm.  If the 
                        voltages measured on either xp or ym read between high 
                        and low, then there is contact with the screen.
            @return     A True/False value, which will state if the ball is on or
                        off the resisitve touchpad.
        '''
        self.xm = pyb.Pin(self.xm_pin, mode = pyb.Pin.OUT_PP)
        self.xp = pyb.ADC(self.xp_pin)
        self.ym = pyb.Pin(self.ym_pin, mode = pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_pin, mode = pyb.Pin.OUT_PP)
        self.xm.low()
        self.ym.low()
        self.yp.high()
        ##@brief Current status of True/False for if ball is on the resistive pad.
        self.z = True if 100 < self.xp.read() and self.xp.read() < 4000 else False
        if self.Debug:
            print(self.z)
        return self.z
 
    @micropython.native           
    def getPoint(self):
        ''' @brief      This section compiles the values for X, Y and Z location
                        as well as the time elapsed since the last measurement
                        or resistive pad scan and returns the values as a tuple.
            @return     A tuple of the X, Y and Z location of the ball on the
                        resisitve touchpad.
        '''
        self.xscan()
        self.yscan()
        self.zscan()
        ##@brief Time elapsed since last resisitve pad scan across all three axis
        self.time = utime.ticks_us()
        ##@brief The output for the current position about the origin.
        self.data = (self.x, self.y, self.z)
        return self.data
        
if __name__ == '__main__':
    ##@brief Initialization values for the resitive touchpad.
    tp = TouchPanelDriver('A1', 'A7', 'A0', 'A6', 20, 32, 2000, 1920)
    while True:
        _start = utime.ticks_us()
        print(tp.getPoint())
        print(str(tp.time - _start) + ' us') #Used to determine the time between current and previous scan for method validation.
        utime.sleep_ms(100) 