'''
@file           Data_Task.py
@brief          This task is used for data collection and serial communication 
                with the PC_Frontend.py file.
@details        This task will be used to send data to the PC Frontend once the ball
                balancing has stopped. We have to wait until the balancing has stopped
                due to the limited memeory available on the Nucleo. This also means
                thatt we are limited on what data we can collect simultaneously.
                In this case, we collected data on time, position/velocity for one 
                axis and tilt angle/angular velocity for one axis.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''

import Shares
from pyb import UART
import pyb
import micropython

# turn off REPL for the UART
pyb.repl_uart(None) # disables repl on st-link usb port
## @brief Assign variable to UART port 2
myuart = UART(2)

class Data_Task:
    ''' 
     @brief    This class is used for data collection from the system for the controller.
     @details  This class sifts through and collects the data from Shares.py and 
               transmits it through the serial port to the PC Frontend to plot it.
    '''
    
    def __init__(self):
        ''' 
        @brief    This initialized the data collection task.
        @details  This task is called at the end of the program execution to transmit
                  the data through the serial port.
        '''

        ## @brief        The variable below is a flag to determine if we're saving x axis or y axis data
        self.dataToSave  = Shares.dataToSave

    @micropython.native           
    def Data_Task_Fcn(self):
        '''
        @brief      This function is used to transmit the data over serial to the PC Front End.
        @details    When the program detects that the user has pressed CTRL-C, 
                    this task is executed to transmit the data over serial, one 
                    line at a time. Once all data has been transmitted, the task
                    sends a command to the frontend to plot the transmitted data.
        '''
        while Shares.Time.any():
            # Send the next row of data to the PC Frontend
            if self.dataToSave == 'x':
                myuart.write('{:}, {:.2f}, {:.2f}, {:.2f}, {:.2f} \r\n'.format(Shares.Time.get(), 
                                                                               Shares.xData.get(), 
                                                                               Shares.xDotData.get(), 
                                                                               Shares.thYData.get(),
                                                                               Shares.thDotYData.get()))
            else:
                myuart.write('{:}, {:.2f}, {:.2f}, {:.2f}, {:.2f} \r\n'.format(Shares.Time.get(), 
                                                                                Shares.yData.get(),
                                                                                Shares.yDotData.get(),
                                                                                Shares.thXData.get(),
                                                                                Shares.thDotXData.get()))
        myuart.write('plot\r\n') # Plot data once all data has been sent