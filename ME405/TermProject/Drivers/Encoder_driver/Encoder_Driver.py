"""@file        Encoder_Driver.py
@brief          This file contains the driver class needed to measure the encoder 
                angle and speed for the two encoders on our table-top kit.
@details        This file contains the driver class needed to read the values of a quadrature encoder.
                The class initializes an encoder with 2 data pins and power/gnd pins. The encoder timer
                channels are initalized to ENC_AB; this is a feature of the timer channels to read
                overflow and unflow for our quadrature encoder. The encoder class has the capability to
                use any to GPIO pins and the appropriate timer for those pins. 
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
"""
from pyb import Timer, Pin
import micropython
from micropython import const
#import time

class Encoder:
    ''' @brief    This class is used to create multiple encoder objects which can be used to gather positional data of the paltform.
        @details  This class will allow us to read and set the position of the encoder, as well
                  as reading the delta between two measurements, and speed of the encoders.
    '''
    
    def __init__(self, pinCH1, pinCH2, timer, Debug = True):
        ''' @brief  This initializes a new Encoder object
            @param  timer Timer used for the specified pins; period=65535, prescaler=0
            @param  channel1 Timerchannel for specified timer, mode=ENC_AB, pin=pinCH1
            @param  channel2 Timerchannel for specified timer, mode=ENC_AB, pin=pinCH2
            @param  CurrentCount A variable representing positional ticks of timer, also represents encoder position
            @param  PreviousCount A variable representing the previous count in ticks
            @param  theta The current angluar position in radians
            @param   Debug if True will print debug statements to console.
        '''
        ##@brief The period of the counter (maximum value is 0XFFFF)
        self.period = const(65535)
        ##@brief Timer used for the specified pins; period=65535, prescaler=0
        self.timer = Timer(timer, prescaler = 0, period = self.period)
        ##@brief Timerchannel for specified timer, mode=ENC_AB, pin=pinCH1
        self.channel1 = self.timer.channel(1, Timer.ENC_AB, pin = pinCH1)
        ##@brief Timerchannel for specified timer, mode=ENC_AB, pin=pinCH2
        self.channel2 = self.timer.channel(2, Timer.ENC_AB, pin = pinCH2)
        ##@brief A variable representing positional ticks of timer, also represents encoder position
        self.CurrentCount = self.timer.counter()
        ##@brief A variable representing the previous tick
        self.PreviousCount = self.CurrentCount
        ##@brief Convert ticks value in radians
        self.theta = self.CurrentCount*.0015708
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug  = Debug
    
    @micropython.native   
    def update(self):
        ''' @brief      This section updates the current position of the encoder.
            @details    Since the encoder is previously initialized to ENC_AB no calculations
                        are needed to find the overflow and underflow values. For this function
                        we only need to call the timer.counter() method to get the position.
        '''
        CurrentTick = self.timer.counter() # Read the current encoder count in Ticks
        delta = CurrentTick -self.PreviousCount # Calculate the delta in ticks
        
        # The code below is used to calculate value in case of under or overflow
        if delta > 0.5*self.period:
            delta -=self.period
        elif delta < -0/5*self.period:
            delta += self.period
        
        # Compute current position in radians
        self.theta += delta*.0015708
        return self.theta
        
        # Update the Previous position variable
        self.PreviousCount = CurrentTick

    @micropython.native        
    def get_position(self):
        ''' @brief      This section gets the encoder's position in radians.
            @details    The encoder value is returned from the object's theta variable
                        which is updated in the Encoder object's update method.
            @return     An integer value, the position of the encoder in radians.
        '''
        if self.Debug:
            print("The current position is: " + str(self.theta))
        return self.theta
   
    @micropython.native    
    def set_position(self, newPosition):
        ''' @brief      This section sets the encoder's position.
            @details    The encoder value is sets the object's currentPos variable. Creates
                        a new point of reference for the Encoder.           
            @return     None
        '''
        if self.Debug:
            print("The current position has been set to: " + str(self.theta))
        self.theta = newPosition
        return self.theta

    @micropython.native    
    def get_delta(self):
        ''' @brief      This section gets the encoder's position.
            @details    The encoder object's delta value is updated in the object's update method.           
            @return     An integer value, the delta value representing the difference of the current position from the previous position.
        '''
        CurrentTick = self.timer.counter() # Read the current encoder count in Ticks
        self.delta = CurrentTick -self.PreviousCount # Calculate the delta in ticks
        if self.Debug:
            print("The current delta is: " + str(self.delta))
        return self.delta
    
#if __name__ == '__main__':
#    encoder1 = Encoder(Pin.cpu.C6, Pin.cpu.C7, 8)
#    encoder2 = Encoder(Pin.cpu.B6, Pin.cpu.B7, 4)
#    encoder1.set_position(0)
#    encoder2.set_position(0)
#    while True:     
#        encoder1.update()
#        encoder2.update()
#        encoder1.get_delta()
#        encoder2.get_delta()
#        encoder1.get_position()
#        encoder2.get_position()
#        time.sleep(1)
