# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 23:30:46 2021

@author: 14083
"""
import math
import time
import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
x = 0
while True:
    sineout = 50 * (math.sin (((2*math.pi)/10)*x)+1)
    t2ch1.pulse_width_percent(sineout)    
    time.sleep(0.1)
    x += 0.1