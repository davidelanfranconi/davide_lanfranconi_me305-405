'''
@file       Touchpad_Task.py
@brief      This class interfaces with the Touchpad_Driver.py file to track the     
            balls location on the touchpad.
@details    This class nteracts with the Touchpad_Driver.py file to constantly 
            update the values in the Shares.py file by reading the balls location 
            from the get_point function in the driver.
@author     Davide Lanfranconi, Anthony Vuong
@date       06/08/21
@copyright  Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''

from Touchpad_Driver import TouchPanelDriver
import Shares
from utime import ticks_us, ticks_diff
from math import fabs
import micropython
from micropython import const


class Touchpad_Task:
    ''' 
     @brief    This class is used to update the Shares.py file with the position
               and velocity of the ball.
    '''
    ## @brief       The initialization state used to set up the task.
    INIT       = const(0)  
    ## @brief       The main operating state where the task is reading from the panel.
    READ_PANEL = const(1)

    def __init__(self, Debug = True):
        ''' 
        @brief    Initialize the touchpad task by creating a touchpad driver inatance.
        @details  The task will be called from the priority scheduler to read 
                  from the resistive touch panel through the Touchpad_Driver.py
                  file. This will give us the position and velocity of the ball, 
                  which will then be updated accordingly in Shares.py.
       @param   Debug if True will print debug statements to console.           
        '''
        ## @brief         Create an instance of the touchpad driver.
        #  @details       This object needs the pin values for the xm, xp, ym, and 
        #                 yp ports on the resistive touch panel. It also needs the 
        #                 length of the board, the width of the board, and the 
        #                 coordinates of the origin.
        self.touchPanel   = TouchPanelDriver('A1', 'A7', 'A0', 'A6', 186, 108, 88, 53, Debug = False)
        ## @brief         The last position of the ball (X, Y, Z coordinates).
        self.lastPosition = (0, 0, True)
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug  = Debug
        
    @micropython.native   
    def Touch_Panel_Fcn(self):
        '''
        @brief      The FSM which runs the touch panel driver and task.
        @details    The FSM below runs the touchpad task. Once the task is initialized,
                    the FSM moves to the read_panel state where it polls the driver
                    to determine the ball's location and speed.
        '''
        ## @brief      Contains the information for which state the FSM is in.  
        state          = self.INIT # start in the sample data state
        ## @brief      The counter used to determine if the ball is on the platform.
        startUpCounter = 0 
        
        ## @brief  Initial x-position for alpha-beta filter
        x0 = 0
        ## @brief  Initial y-position for alpha-beta filter
        y0 = 0
        ## @brief  Initial z-position
        z0 = False

        
        while True:
            if state == self.INIT:
                # Check to see if the ball is on the platform
                if self.touchPanel.zscan():
                    ## @brief   The number of times that the board has been on
                    startUpCounter += 1
                    if self.Debug:
                        print(startUpCounter)
                    
                    
                # if startUpCounter is 10 or greater, start the touch panel readings
                if startUpCounter > 10:
                    if self.Debug:
                        print('Ball is on touchpad.')
                    Shares.BallOnPlatform.put(1)
                    x0, y0, z0 = self.touchPanel.getPoint()
                    ## @brief  Initial x-velocity for alpha-beta filter
                    vx0 = 0
                    ## @brief  Initial y-velocity for alpha-beta filter
                    vy0 = 0
                    ## @brief    The time of the last iteration in microseconds.
                    lastTime     = ticks_us()
                    state = self.FSM_Transition(state, self.READ_PANEL)
               
            elif state == self.READ_PANEL:
                if Shares.BallOnPlatform.get() == 0:
                    startUpCounter = 0 
                    if self.Debug:
                        print('Touchpad has been reset.')
                    state = self.FSM_Transition(state, self.INIT)
                
                ## @brief   The current time in microseconds.
                currentTime = ticks_us()
                
                ## @brief      The current X,Y,Z from the panel
                currentPosition = self.touchPanel.getPoint()
                if self.Debug:
                    print('Current Position = {:}'.format(currentPosition))
                
                # if the positions are valid, start to filter
                if fabs(currentPosition[0]) < 85 and fabs(currentPosition[1]) < 50 and currentPosition[2] == True:     # position reading is valid
                    
                    # Find delta time since last computatation
                    ## @brief  Time between computations of alpha-beta filter values
                    dt = ticks_diff(currentTime, lastTime)/1000000 # microseconds
                    
                    # get X Position and Velocity
                    ## @brief  Next, estimated x-position from alpha-beta filter
                    x1 = x0 + 0.85*(currentPosition[0] - x0) + (vx0*dt)
                    ## @brief  Next, estimated x-velocity from alpha-beta filter
                    vx1 = vx0 + (0.005/dt)*(currentPosition[0] - x0)
                    x0 = x1
                    vx0 = vx1
                    
                    # get Y Position and Velocity
                    ## @brief  Next, estimated y-position from alpha-beta filter
                    y1 = y0 + 0.85*(currentPosition[1] - y0) + (vy0*dt)
                    ## @brief  Next, estimated y-velocity from alpha-beta filter
                    vy1 = vy0 + (0.005/dt)*(currentPosition[1] - y0)
                    y0 = y1
                    vy0 = vy1

                    # reset lastTime
                    lastTime = currentTime
                    
                    # send to shares.py
                    Shares.x.put(x1)    
                    Shares.y.put(y1) 
                    Shares.xDot.put(vx1)  
                    Shares.yDot.put(vy1)

            yield(state)

    @micropython.native
    def FSM_Transition(self, state, Next_State):      # state transition method
        '''
        @brief      This function transitions between states of the FSM.
        @details    In this function you specify the current state and what state 
                    you want to transition to and the function will transition the 
                    FSM and return the new current state.
        @param      state The current state of the system.
        @param      NextState The new state the system should transition to.
        @return     The new state of the finite state machine.            
        '''
        if self.Debug:
            print('The TouchPad FSM has transitioned from ' + str(state) + ' to ' + str(Next_State) +' state.')
        return Next_State               # now that transition has been declared, update state    
    