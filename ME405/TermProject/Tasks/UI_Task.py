'''
@file           UI_Task.py
@brief          This class is used to run the FSM on the Nucleo board and interface with
                the spyder user console window.
@details        This class interfaces with the Spyder Console window through the serial 
                communication. When a key is pressed, the character is sent through 
                serial to this class, where depending on the key pressed a specific
                action will be triggered such as stopping the platform or starting the
                balancing. Additionally, any data that is recorded during the run
                will be sent back through the serial port to be plotted in the Spyder 
                Console.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''
import Shares
import pyb
from pyb import UART, USB_VCP
import micropython
from micropython import const


# turn off REPL for the UART
pyb.repl_uart(None) # disables repl on st-link usb port
## @brief Assign variable to UART port 2
myuart = UART(2)

class UI_Task:
    ''' 
     @brief    This class is used to interface between PC_Frontend.py and the serial port.
     @details  This class will transmit and recieve data through the serial port
               to the PC_Frontend.py script.
    '''
    ## @brief           The initialization state used to set up the task.
    INIT = const(0)
    ## @brief           This state is used while waiting for the user to press a letter and write to task_share
    WRITE_COMMANDS = const(1)   

    def __init__(self, Debug = True):
        ''' 
        @brief    This function initializes the UI task.
        @details  This task will be used to collect and send data to the front 
                  end for when the blue button is pressed to reset the motor fault.
        @param    Debug if True will print debug statements to console.
       '''
        ## @brief            Contains the information for which state the FSM is in.  
        self.state = self.INIT # start in the init state
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug = Debug
 
    @micropython.native        
    def UI_Task_Fcn(self): 
        '''
        @brief      This function is used to report requested values to the front end.
        @details    This function will standby until a 'g' character is recieved.
                    It will then record the data until the 's' button is pressed, 
                    where it will then send the data through serial to the front 
                    end for ploting or further data processing.
        '''
        myVCP = USB_VCP()
        VCPcmd = 0
        
        while True:
                
            if myVCP.any():
                
                VCPcmd = int(myVCP.read(1)[0])
                if self.Debug:
                    print('UI_Task: vcp={:}'.format(VCPcmd))
                
                if VCPcmd in [103, 114, 115]:       
                    Shares.cmd.put(VCPcmd)
                else: 
                    if self.Debug:
                        print('Please enter a valid key')

            yield(0)    
            
    @micropython.native
    def FSM_Transition(self, state, Next_State):      # state transition method
        '''
        @brief      This function transitions between states of the FSM.
        @details    In this function you specify the current state and what state 
                    you want to transition to and the function will transition the 
                    FSM and return the new current state.
        @param      state The current state of the system.
        @param      NextState The new state the system should transition to.
        @return     The new state of the finite state machine.            
        '''
        if self.Debug:
            print('The UI FSM has transitioned from ' + str(state) + ' to ' + str(Next_State) +' state.')
        return Next_State               # now that transition has been declared, update state
            