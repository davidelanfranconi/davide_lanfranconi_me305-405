'''@page LAB0X02_ME405_page.py LAB0X02: Think Fast!

@section LAB0X02_summary Summary
The learning objective of this lab is to learn how to write code which can respond
very quickly to an event using by using interrupts in MicroPython. The ultimate
scope of this lab is to write code which will accurately measure a person's 
reaction time responding to a light and pressing a button in microseconds.

@section LAB0X02_design Design Requirements
In this lab, we have two separate parts, where the first part is using an external
interrupt event when the button is pressed, while the second part of the lab uses
an internal interupt and dedicated inpute capture and output compare hardware on 
the board to respond in a much more timely manner./n
The way the program works is the program waits a random time between 2 and 3 seconds, 
then turns on the green LED for 1 second. An interrupt will be triggered when the 
user presses the blue B2 button in response to the LED lighting up. The board will
then measure the person's reaction time and record it. This cycle repeats until 
the user presses CTRL+C to end the game, at which point their average reaction 
time is calculated and displayed, or a status message will be shown if they 
did not press the button at all and played no rounds. 

@section LAB0X02_overview Overview
As stated above, the lab is split into two separate parts. For the user, there
should be no difference while playing the game other than the fact that the second
part may give them a more accurate time measure as the interrupt using output
compare and input captures are more instantaneous when compared to external hardware
interrupts.\n

For applications that require high-resolution and high accuracy time measurements, using the 
second approach (Part B) may be more appropriate. However, for this particular use case,
the method used in Part A would have sufficed, since high precision is not 
necessary to measure human reaction time, which tends to averages around 200 
milliseconds.

@section LAB0X02_documentation Documentation
Documentation of these labs can be found here: LAB0X02A_ME405.py and LAB0X02B_ME405.py

@section LAB0X02_source Source Code 
The source code for this lab: 
https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305-405/src/master/ME405/LAB0X02/

- - -
@author         Davide Lanfranconi, Nicholas Greco
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved'''

