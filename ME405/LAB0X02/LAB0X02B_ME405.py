'''@file        LAB0X02B_ME405.py
@brief          This file computes the average reaction time for the user to
                press a button after an LED turns on using an IC callback.
                
@details        This file is what is used to calculate the individual reaction 
                time per round and the average reaction time at the end of the
                game for the user. When the user initializes the game, they will
                recieve directions on how to play and how to interact with the 
                game. When enter is pressed the round begins and the user is 
                alerted to be on the lookout for the LED flash. As soon as the
                LED turns on, the user must press the blue button B2 to turn the
                LED off, and the reaction time is recorded. If the user does not
                push the button after a second that the LED is on, it will turn
                off and continue to measure the reaction time. If the time from
                when the LED turned on to when the user pushes the button is 
                greater than five seconds, the user will be notified that the 
                round timed out, and the round is not counted. Once the program 
                is started it will continue to run until it is ethier stopped 
                by the debug window or by exiting pressing Ctrl+C. When the user 
                presses Ctrl+C, the game is ended and it returns an average 
                reaction time to the user as well as the number of rounds that 
                were played.
                
@author         Davide Lanfranconi, Nicholas Greco
@date           06/05/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved'''

import pyb
import random

_cyclestart = None
_cycleend = None
_TimeOut = None
##@brief Holds OC compare time.
LastCompare = 65540
_ledon = 0
_ledoff = 0
_state = 0
_t = True
##@brief Is used to determine if button has been pressed or not.
ButtonPress = 0
##@brief Empty list to store reaction times from rounds for calculating average.
reactiontimes = [0]
_last_key = None

# pinA5 corresponds ot the LED that will toggle when the output compare condition is matched
##@brief Initialize LED.
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
##@brief Configures Timer 2 Channel 2 for IC.
pinB3 = pyb.Pin(pyb.Pin.cpu.B3)

def RandTime():
    ''' @brief This function returns a random value between 2 and 3 seconds after which the LED turns on
        @return r Returns a random value between 2 and 3 
    '''
    ##@brief Random value between 2 and 3 seconds after which the LED will turn on.
    r = random.uniform(2,3)*10000
    return r

def OC_Compare(tim2):
    ''' @brief The function listens for when there is a change in the LED status 
        and will record a start time or end the round if no action is detected. 
        @param tim2 The time when the LED is turned on
    '''
    global _cyclestart
    global _cycleend
    global LastCompare
    global _TimeOut
    global _ledon
    global _ButtonPress
    global _state


    if _cyclestart:
        _cyclestart = False
        _ledon = t2ch1.compare()
        LastCompare += 10000
        _cycleend = False
        return
                    
    elif not _cycleend:
        _cycleend = True
        LastCompare += 40000
        _TimeOut = False
        return
    
    elif not _TimeOut:
        _TimeOut = True
        print('The round has timed out, please wait and try again')
        _state = 3
        return
    
def IC_Capture(tim2):
    ''' @brief The function listens for when there is a falling edge and records the time
        @param tim2 The time when the button is pressed
    '''
    # Input capture serves to capture the falling edge only 

    global _ledoff
    global _t
    global _state

    if not _cyclestart:
        _ledoff = tim2.capture() 
        _t = True
        _state = 2
    
if __name__ == '__main__':
    while True:
        try:
            if _state == 0:
                print('Welcome to the User Reaction Game. ')
                print('The objective of the game is to push the blue button as quickly'
                'as possible after the LED turns on.')
                _last_key = input('Press enter to begin and Ctrl-C to end the game')
                _state = 1
        
            # Turns the LED on when an output compare is made
            elif _state == 1:
                ## @brief Configures Timer 2 on the Nucleo
                # @details The timer has a 0.1 ms resolution and a 6.55 second period.
                tim2 = pyb.Timer(2, period = 0xffff, prescaler = 8000)
                # Linked to the button press detection
                ## @brief Configures Timer 2 Channel 2 for IC
                t2ch2 = tim2.channel(2, pyb.Timer.IC, pin=pinB3, polarity=pyb.Timer.FALLING, callback=IC_Capture)
                ## @brief Configures Timer 2 Channel 1 as an OC to toggle the LED
                t2ch1 = tim2.channel(1, mode = pyb.Timer.OC_TOGGLE, 
                                     pin=pinA5, 
                                     compare=LastCompare, 
                                     callback=OC_Compare)  
                
                if _t:
                    print('the round will begin in a few seconds')
                    _cyclestart = True
                    _cycleend = True
                    _TimeOut = True
                    _t = False
                    tim2.counter(0)
                    LastCompare = RandTime()

           
                    
            # Reaction time calculation
            elif _state == 2:
                ##@brief Reaction time in microseconds.
                reaction = _ledoff - _ledon
                reactiontimes.append(reaction)
                _cyclestart = True
                _cycleend = True
                _state = 1
            
            # Restart the round
            elif _state == 3:
                _t = True
                _cyclestart = True
                _cycleend = True
                _state = 1
                
        except KeyboardInterrupt:  # will stop the program with a control-C interrupt and display the print line below
                tim2.deinit()
                pyb.Pin(pyb.Pin.cpu.A5) # see pyb pin library to reset pin appropriately
                
                _n = len(reactiontimes) - 1
                if _n == 0:
                    print('Thank you for playing. You have no average time as no rounds were played.')
                    break
                elif _n > 0:
                    ##@brief Average reaction time.
                    reactave = sum(reactiontimes) / (_n/10000)
                    print('Thank you for playing. Your average reaction time was', reactave, 'seconds over the course of', _n, ' rounds.')
                    break               
        
    
    
    
    
    




