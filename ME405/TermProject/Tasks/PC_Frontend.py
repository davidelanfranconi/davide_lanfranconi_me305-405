'''
@file           PC_Frontend.py
@brief          This file sets up the UI between the Spyder console window and the
                Nucleo to collect and plot the data transmitted over serial.
@details        The data selected in the Data_Task.py file is transmitted to the
                Nucleo via serial. When it is recieved the script below plots the
                data and exports the values to a .csv file.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''
import serial
import csv
import os 
from matplotlib import pyplot as plt
from matplotlib import rcParams
import numpy as np

## @brief Set up serial communication through serial command
ser = serial.Serial(port='COM5', baudrate=115200, timeout=1)
ser.flushInput()
    
def Write_To_CSV(data, filename):
    ''' 
    @brief   Seperate the data vectors and write the data into a csv file from the Nucleo.
    @details Once data collection has finished, the transmitted data must be stripped
             and split to be in the correct format to be writable to a .csv file.
    @param   data The data from the serial port that needs to be saved in the .csv file.
    @param   filename The specific  name to to which the data should be saved to.                 
    '''
    dataStripped = data.strip()
    dataSplit    = dataStripped.split(', ')
    
    TimeD     = float(dataSplit[0])/1000000              # seconds
    XD        = float(dataSplit[1])                      # mm
    X_DotD     = float(dataSplit[2])                      # mm/s
    thYED     = float(dataSplit[3])*180/3.1415           # deg
    th_DotYED  = float(dataSplit[4])*180/3.1415           # deg/s
               
    with open(filename,"a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([TimeD, XD, X_DotD, thYED, th_DotYED])       

def Clear_Files(fileNames):
    '''
    @brief      This function clears exisitng files before saving a new one.
    @details    Since the .csv writer utility appends to files, the exisitng file must
                be removed each time there is a new round of data collected. 
    @param      fileNames This parameter contains a list of strings which contain
                the various files which are written to each time the PC_Frontend is run. 
    '''    
    _filesRemoved = 0
    
    for n in range(len(fileNames)):
        
        _fileName = fileNames[n]
        
    # check if file exists 
        if os.path.exists(_fileName):
            os.remove(_fileName)
            _filesRemoved += 1
            
    print('{:} All .csv/.png files have been removed from the directory'.format(_filesRemoved))

def Plot_Data(csvName, figNameX, figNameY):
    ''' 
    @brief   This function plots the position and speed data from the encoders.
    @details This function utilizes the subplot feature within matlibplot to plot
             the position and speed of the motors along with the reference data 
             so we can better visualize the relationship between the position 
             and speed at all points in time.
    @param   csvName   Specifies the file name to grab the data from.
    @param   figNameX  Specifies the name of the x-axis data plot.
    @param   figNameY  Specifies the name of the y-axis data plot.
    '''
    TimeD, XD, X_DotD, th_YED,th_DotYED = np.loadtxt(csvName, delimiter=',', usecols=(0,1,2,3,4), unpack=True)
    # font = {'fontname':'Times New Roman'}
    rcParams['font.family'] = 'monospace'
    TimeD -= TimeD[0]
    
    if dataToSave == 'x':
        fig1,axs1 = plt.subplots(2,2,figsize = (6,6), dpi = 600, constrained_layout=True)
        plt.subplots_adjust(hspace = 0.7,wspace = 0.7)
        
        axs1[0,0].plot(TimeD, XD,'k',linewidth = 0.75)
        axs1[0,0].set(title='Ball Position',xlabel = r'Time, $t$ [ s ]',ylabel = r'$x(t)$ [ mm ]')
        axs1[0,1].plot(TimeD, th_YED,'k',linewidth = 0.75)
        axs1[0,1].set(title='Platform Angle',xlabel = r'Time, $t$ [ s ]',ylabel = r'${\theta}_y(t)$ [ deg ]')
        axs1[1,0].plot(TimeD, X_DotD,'k',linewidth = 0.75)
        axs1[1,0].set(title='Ball Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{x}(t)$ [ mm/s ]')
        axs1[1,1].plot(TimeD, th_DotYED,'k',linewidth = 0.75)
        axs1[1,1].set(title='Platform Angular Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{\theta}_y(t)$ [ deg/s ]')
        plt.show()
    
    else:
        fig2,axs2 = plt.subplots(2,2,figsize = (6,6), dpi = 600, constrained_layout=True)
        plt.subplots_adjust(hspace = 0.7,wspace = 0.7)
        
        axs2[0,0].plot(TimeD, XD,'k',linewidth = 0.75)
        axs2[0,0].set(title='Ball Position',xlabel = r'Time, $t$ [ s ]',ylabel = r'$y(t)$ [ mm ]')
        axs2[0,1].plot(TimeD, th_YED,'k',linewidth = 0.75)
        axs2[0,1].set(title='Platform Angle',xlabel = r'Time, $t$ [ s ]',ylabel = r'${\theta}_x(t)$ [ deg ]')
        axs2[1,0].plot(TimeD, X_DotD,'k',linewidth = 0.75)
        axs2[1,0].set(title='Ball Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{y}(t)$ [ mm/s ]')
        axs2[1,1].plot(TimeD, th_DotYED,'k',linewidth = 0.75)
        axs2[1,1].set(title='Platform Angular Velocity',xlabel = r'Time, $t$ [ s ]',ylabel = r'$\dot{\theta}_x(t)$ [ deg/s ]')
        plt.show()


## @brief     File name for initial plot
pngFileNameX  = "Lab0xFF_X_Motor_Data.png"
## @brief     File name for initial plot
pngFileNameY  = "Lab0xFF_Y_Motor_Data.png"
## @brief     File name of the exported data
csvFileName   = "Lab0xFF_Exported_Data.csv"

# Check to see if the csv file exists, and if it does, delete it so it can be written over with new data
Clear_Files([pngFileNameX,pngFileNameY,csvFileName])


## @brief        The outputted data from the serial port
dataFromDataTask = None

## @brief   String that sets whether x or y data is being collected
dataToSave = input('Please input either an x or a y depending on which data is being plotted: ')

while True:
    try:      
        
        ## @brief  Most recent data from serial communciation between PC and dataTask
        dataFromDataTask = ser.readline().decode('ascii')    # read from uart

        print(dataFromDataTask)
        
        if dataFromDataTask:            
            ## @brief Non empty line from Nucleo serial communication
            currentFromNucleo = dataFromDataTask
                     
            if currentFromNucleo == 'plot\r\n':     
                Plot_Data(csvFileName, pngFileNameX, pngFileNameY)            
               
            else:   # this case is for writing to csv file        
                Write_To_CSV(currentFromNucleo,csvFileName)

    except KeyboardInterrupt:
        Plot_Data(csvFileName, pngFileNameX, pngFileNameY)
        break
    
# closer serial port
ser.close()
