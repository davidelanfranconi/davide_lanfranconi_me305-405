"""@file        Motor_Driver.py
@brief          This file contains the driver class needed to operate the DRV8847 
                dual H-Bridge and to operate the two DC motors on our table-top kit.
@details        This file contains the driver class needed to scan across the x,
                y and z components of the touchpad. The z axis returns a true or
                false value depending on if the ball is touching the platform or 
                not. The other two axis return a numerical value for the coordinate
                position of the ball from the pad origin. The values are returned
                as a tuple along with the time length between the current and 
                previous scan.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
"""

from pyb import Pin, ExtInt, Timer
import micropython
import time

class Motor:
    ''' @brief   This class contains the methods and parameters needed to operate 
                the DRV8847 dual H-Bridge and to operate the two DC motors on 
                our table-top kit. '''
    
    def __init__(self, nSLEEP_pin, nFAULT_pin, IN1_pin, IN2_pin, OUT1_pin, OUT2_pin, IN12_timer, frequency, Debug = True):
        '''
            @brief Initializes the motor driver object with given parameters.
            @param nSLEEP_pin Pin for not sleep, defaults to pyb.Pin.board.PA15.
            @param nFAULT_pin  Pin for not fault, defaults to pyb.Pin.board.PB2.
            @param IN1_pin Pin connected to positive lead on motor 1, defaults to pyb.Pin.board.PB5.
            @param IN2_pin Pin connected to positive lead on motor 2, defaults to pyb.Pin.board.PB1.
            @param OUT1_pin Pin connected to negative lead on motor 1, defaults to pyb.Pin.board.PB4.
            @param OUT2_pin Pin connected to negative lead on motor 2, defaults to pyb.Pin.board.PB0.
            @param IN12_timer Chooses which timer to use for PWM, defaults to pyb.Timer(3).
            @param frequency   Defines the PWM frequency, defaults to 80000 Hz
            @param   Debug if True will print debug statements to console.
        '''
        print('Creating a motor driver.\n')
        
        ##@brief Motor is enabled when initialized if no faults are present
        self.enabled = True 
        
        ##@brief Define nSLEEP pin to enable/disable motor driver
        self.nSLEEP_pin = Pin(nSLEEP_pin, mode=Pin.OUT_PP, value=1)
        
        ##@brief Define external callback triggered by the fault pin in the motor driver.
        # @details  When a fault is detected in by the motor driver, pin PB2 is set to low, and when the 
        #           callback detects a falling edge on PB2, the callback below is run and the self.Fault
        #           flag is set to True. The motor driver will then disable and may only be started once
        #           the fault is cleared.
        self.nFAULT_pin = ExtInt(nFAULT_pin, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_UP, callback=self.fault_CB)
        ##@brief This flag is set as false when initialized and is set to True when a second fault is detected after an initial fault.
        self.Fault = False
        ##@brief This flag is set to false initially, set to True on the first fault, and on the second fault the self.fault flag will be set to True.
        self.IgnoreFault = False

        ## @brief      Callback function for when the blue B2 button is pressed.
        #  @details    The blue button on the board can be used to clear any faults detected
        #              by the nFAULT pin. When the button is pressed, the function clear_fault() is called.
        self.ButtonInt = ExtInt( Pin (Pin.cpu.C13)  , mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback= self.clear_fault)        

        ##@brief Motor 1 input pin
        self.m1p = Pin(IN1_pin)
        ##@brief Motor 2 input pin
        self.m2p = Pin(IN2_pin)
        ##@brief Motor 1 outut pin
        self.m1m = Pin(OUT1_pin)
        ##@brief Motor 2 output pin
        self.m2m = Pin(OUT2_pin)
        
       
        ##@brief Define Timer
        self.tim = IN12_timer
        ##@brief Define timer frequency
        self.freq = frequency
        ##@brief Define Timer mode, center is more precise for dutycycle
        self.tim.init(freq=self.freq, mode=Timer.CENTER)
        
        ##@brief Initialize PWM channel for timer for motor pin, inital motor speed is off
        self.ch1 = self.tim.channel(1, Timer.PWM, pin=self.m1m, pulse_width_percent=0)
        ##@brief Initialize PWM channel for timer for motor pin, inital motor speed is off
        self.ch2 = self.tim.channel(2, Timer.PWM, pin=self.m1p, pulse_width_percent=0)
        ##@brief Initialize PWM channel for timer for motor pin, inital motor speed is off
        self.ch3 = self.tim.channel(3, Timer.PWM, pin=self.m2m, pulse_width_percent=0)
        ##@brief Initialize PWM channel for timer for motor pin, inital motor speed is off
        self.ch4 = self.tim.channel(4, Timer.PWM, pin=self.m2p, pulse_width_percent=0)
        
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug  = Debug

    @micropython.native        
    def enable(self):
        '''
        @brief      Enables the motor driver.
        @details    The nSLEEP pin is set high, allowing the motor driver to run
                    and enabling all pins related to motor function.
        '''
        # Set nsleep to true to enable motor driver
        if self.Debug:
            print('Enabling Motor Driver')
        self.nSLEEP_pin.high()
    
    @micropython.native    
    def disable(self):
        '''
        @brief Disables the motor driver and sets all motors to zero.
        @details    The nSLEEP pin is set low, disabling the motor driver, as 
                    well as setting the duty cycle for any running motor to zero.
        '''
        # Turns off all motors
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)
        self.ch3.pulse_width_percent(0)
        self.ch4.pulse_width_percent(0)
        
        # Set nsleep to false so disable motor driver
        if self.Debug:
            print('Disabling Motor Driver')
        self.nSLEEP_pin.low()
    
    @micropython.native
    def fault_CB(self, IRQ_src):
        '''
        @brief      A callback function which will disable and stop all motors upon a fault detection.
        @details    When a fault is detected by the motor driver and this callback is triggered by a 
                    falling edge on PB2, which is connected to the nFAULT pin, the self.Fault variable
                    is set to True on the second fault. On the first fault, IgnoreFault will be set to True,
                    which is used as a false alarm filter.
        @param      IRQ_src Interupt Request from ExtInt nFAULT_pin callback. 
        '''
        if self.IgnoreFault:
            if self.Debug:
                print('First fault has been detected, Ignoring First Fault.\n')
            self.IgnoreFault = False

        else:
            if self.Debug:
                print('Second fault has been detected, please press the blue user button to clear fault condition.\n')
            self.Fault = True

    @micropython.native    
    def clear_fault(self, IRQ_src):
        '''
        @brief      Clears the fault so the motor driver can be reenabled
        @details    When the blue button B2 is pressed, this callback is triggered by a 
                    falling edge on pin PC13. This will cause two variables, self.Fault and
                    self.IgnoreFault to be set back to False so that the flag condition and
                    variable for when a fault is detected can be reset. 
        @param      IRQ_src Interupt Request from ExtInt ButtonInt callback. 
        '''
        # Clear the fault so motors can be used again
        self.Fault = False
        self.IgnoreFault - False
        if self.Debug:
            print('Fault has been cleared.\n')

    @micropython.native    
    def set_duty_M1(self, duty):
        '''
        @brief      Spins motor 1 in specified direction at specified duty cycle value.
        @details    When the duty cycle for the motor is between -100 and -001, Motor 1
                    will sping in a clockwise direction. When the duty cycle is between
                    001 and 100, Motor 1 will sping in a counter clockwise direction. When
                    the duty cycle is 0, the motor will not spin and will remain stationary.
        @param      duty The duty cycle at which to set the motor. 
        '''
        self.enabled = True
        # Checks if a fault has occurred or motor will run
        if self.enabled:
            # If duty is < 0, Motor 1 spins clockwise
            if (duty <= 100 and duty >= -100):
                if (duty < 0):
                    self.ch1.pulse_width_percent(0)
                    self.ch2.pulse_width_percent(abs(duty))
                else:
                    self.ch2.pulse_width_percent(0)
                    self.ch1.pulse_width_percent(abs(duty))
            else:
                print("Not a valid input.")

    @micropython.native            
    def set_duty_M2(self, duty):
        '''
        @brief      Spins motor 2 in specified direction at specified duty cycle value.
        @details    When the duty cycle for the motor is between -100 and -001, Motor 2
                    will sping in a clockwise direction. When the duty cycle is between
                    001 and 100, Motor 2 will sping in a counter clockwise direction. When
                    the duty cycle is 0, the motor will not spin and will remain stationary.
        @param      duty The duty cycle at which to set the motor. 
        '''
        self.enabled = True
        # Checks if a fault has occurred or motor will run
        if self.enabled:
            # If duty is < 0, Motor 1 spins clockwise
            if (duty <= 100 and duty >= -100):
                if (duty < 0):
                    self.ch3.pulse_width_percent(0)
                    self.ch4.pulse_width_percent(abs(duty))
                else:
                    self.ch4.pulse_width_percent(0)
                    self.ch3.pulse_width_percent(abs(duty))
            else:
                print("Not a valid input.")
                
if __name__ == '__main__':
    motor = Motor('PA15', 'PB2', 'PB5', 'PB1', 'PB4', 'PB0', Timer(3), 800000)
    motor.enable()
    while True:     
        motor.set_duty_M1(50)
        motor.set_duty_M2(50)
        time.sleep(1)
        motor.set_duty_M1(0)
        motor.set_duty_M2(0)
        time.sleep(1)

    