"""@file        MCP9808.py
@brief          This file contains the driver code for the MCP9808 temperature sensor
                to communicate with the user via the I2C protocol.
@details        This file contains the initialization module needed to begin I2C 
                communication, as well as multiple methods to verify proper sensor
                operation and wiring to the board. This file also returns the 
                measured ambient temperature, both in degrees celsius and fahrenheit.
                This is done by reading the ambient temperature register from the 
                external temperature sensor, where an algorithm is then used to 
                convert the bytearray data to a useful temperature measurement value.
@author         Davide Lanfranconi
@date           06/06/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
"""

import pyb
from pyb import I2C

class MCP9808:
    ''' @brief      This class allows the end user to  to commmunicate with a 
                    MCP9808 temperature sensor via the I2C protocol.
        @details    This class is used to create a MCP9808 object, which can 
                    interface with an I2C object, or in this case the Nucleo,  
                    via the given bus address. Objects in this class can check that 
                    they are wired correctly and that they are the correct sensor
                    by checking the given bus address against the value present in
                    the manufacturer ID register. This object can also return 
                    the temperature measurement in degrees Celsius or Fahrenheit.\n
                    \n
                    The Datasheet can be found here: https://cdn-shop.adafruit.com/datasheets/MCP9808.pdf\n
                    The Register Pointer Addresses is specified on Datasheet pg. 16
    '''
    ## @brief Temperature register
    T_A = 5
    ## @brief Manufacturer ID register
    MANUFACTURER_ID = 6
    
    def __init__(self, I2C, Address):
        ''' @brief  This initializes a new MCP9808 object
            @param  I2C The I2C object which is used for serial communication
            @param  Address An integer value which represents the bus address of 
                    the sensor
        '''   
        ##@brief The communication protocol uses, in this case I2C.
        self.I2C = I2C
        ##@brief The assigned device address for the thermometer.
        self.Address = 0b11000 + Address
        ##@brief This buffer holds 2 bytes of data from I2C.
        self.buffer= bytearray(2)
        ##@brief The manufacturer id (datasheet pg. 27).
        self.id = 0x0054
        
    def check(self):
        ''' @brief      This section verifies that the sensor is attached at the 
                        given bus address and that the correct sensor is present
            @details    This section verifies that the sensor is attached at the 
                        given bus address by checking that the value in the 
                        manufacturer ID register is correct.
            @return     A Boolean value, which will state if the value in the 
                        manufacturer ID register is correct or not.
        '''
        self.I2C.mem_read(self.buffer, self.Address, self.MANUFACTURER_ID)
        return self.id == self.buffer[1]
                
    def celsius(self):
        ''' @brief This Section measures the temperature from the MCP9808 in degrees Celsius
            @return temp A float value of the temperature in degrees Celsius
        '''  
        
        self.I2C.mem_read(self.buffer, self.Address, self.T_A)
        upper_byte = self.buffer[0] # Read 8 bits and send ACK bit
        lower_byte = self.buffer[1] # Read 8 bits and send NAK bit
        upper_byte &= 0x1F         # Clear flag bits
        
        if ((upper_byte & 0x10) == 0x10):   # if temp ambient < 0°C
            upper_byte &= 0x0F              # Clear SIGN
            Temp = 256 - (upper_byte * 16 + lower_byte / 16)
        else: # if temp ambient >= 0°C
            Temp = (upper_byte * 16 + lower_byte / 16);
        return Temp
    
    
    def fahrenheit(self):
        ''' @brief This Section measures the temperature from the MCP9808 in degrees Fahrenheit
            @return A float value of the temperature in degrees Fahrenheit
        '''
        return (self.Celsius() * 9.0 / 5.0) + 32.0

if __name__ == '__main__':
    
    ## Define the I2C object which will set the Nucleo as the master and the MCP9808 as the slave device
    i2c = I2C(1, I2C.MASTER, baudrate = 115200)
    
    ## MCP9808 object
    sensor = MCP9808(i2c, 0)
    try:
        if sensor.check():
            while True:
                print('The current ambient temperature is' + str(sensor.celsius()) + 'degrees Celsius.')
                print('The current ambient temperature is' + str(sensor.farenheit()) + 'degrees Fahrenheit.')
                print('')
                pyb.delay(1000)
        else:
            print('The temperature sensor was not found or detected.\n')
    
    except KeyboardInterrupt:
        print('Thank you for using the temp sensor. Data collection will now terminate.\n')


        
    
