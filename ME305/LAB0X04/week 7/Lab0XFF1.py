'''@file        Lab0XFF1.py
@brief          The program starts a function on the Nucleo, and plots the output
                data and generates a .CSV file of the data.
                
@details        The program below will run when executed with the debug console
                in Spyder. It will prompt the user to press G to start the data
                collection cycle, q to exit the program, and S to stop the 
                program before the 30 second timespan is over. If the program 
                is terminated before S is press using Q or Crl+C, then the 
                program will exit normally and go to an idle state. If the 
                program is started and eneded early using S, the Nucleo will 
                send the data collected for that time span, and then the 
                program will plot it and generate a .CSV file of the data. If
                the program is allowed to run for 30 seconds, at the endo of the 
                30 seconds the Nucleo will send the data collected for the 
                30 second time span, and then the program will plot it and 
                generate a .CSV file of the data. 
                
                The program may be stopped at any time by using the 
                Ctrl+C keystroke. 
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%207/Lab0XFF1.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

import serial
import time
from sys import exit
from matplotlib import pyplot
from array import array
import keyboard
import csv

last_key = None

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("S", callback=kb_cb, suppress = True)

times  = array('f', 401*[])
values = array('f', 401*[])

def getChar():
    ser.write('G'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        time.sleep(0.1)
        timeout+=1
        if timeout == 320:
            exit
    return ser.readline().decode()
    
with serial.Serial(port='COM7',baudrate=115273,timeout=1) as ser:
    while True:
        try:
            user_in = input("Enter 'g' to get data from the Nucleo or 'q' to quit\r\n>>")
            if user_in == 'g' or user_in == 'G':
                ser.write('G'.encode())
                timeout = 0
                print("Press 's' when program is running to stop it")
                while ser.in_waiting == 0:
                    time.sleep(0.1)
                    timeout+=1
                    #if user_in == 's' or user_in == 'S':
                     #   ser.write('S'.encode())
                    if last_key == 's' or last_key == 'S':
                       ser.write('S'.encode())
                    elif timeout == 350:
                        print('The program has timed out, please run again')
                        break
                    
            
            
                n = 0
                time.sleep(1)
                while ser.in_waiting != 0:
                    dataString = ser.readline().decode()
                    #print("The string is: " + dataString)
            
                # Remove line endings
                    strippedString = dataString.strip()
            
                # split on the commas
                    splitStrings = dataString.split(',')
                    
                    times.append(float(splitStrings[0]))
                    values.append(float(splitStrings[1]))
                    n += 1
                
                m = n - 1
                
                pyplot.figure()
                pyplot.plot(times, values)
                pyplot.xlabel('Time')
                pyplot.ylabel('Data')
             
                with open('data_file.csv', mode = 'w', newline = '') as data_file:
                    data_writer = csv.writer(data_file, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_MINIMAL)
                    n = 0
                    data_writer.writerow(['Time', 'Position Value'] )
                    while n <= m:
                        data_writer.writerow([times[n], values[n]])
                        n += 1
                break
            
            elif user_in == 'q' or user_in == 'Q':
                print('Thanks')
                break

        except KeyboardInterrupt:
            break
# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
    