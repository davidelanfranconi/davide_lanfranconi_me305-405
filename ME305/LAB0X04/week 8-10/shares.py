'''@file        shares.py
@brief          This file is what is used to set and store values which are 
                used across multiple files or classes.
                
@details        The values below are setup to be initial values so the 
                parameter name exists, and when it is called used the 
                shares.FCN_NAME, where FCN_NAME is the parameter we want to 
                read or set, the inital or current value gets overwritten.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/shares.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        shares                
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

Encoder_Position = 0
Send_Data = None
Kp = 0.2
Ki = 50
Get_Delta = None
Encoder_Delta = 0
Get_Omega_Meas = None
Omega_Meas = 0
Set_Omega_Des = None
Omega_Des = 0
Zero_Encoder = None
Motor_Stop = None
Motor_Start = None
LastChar = None
