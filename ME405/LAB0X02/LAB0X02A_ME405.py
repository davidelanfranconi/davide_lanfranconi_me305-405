'''@file        LAB0X02A_ME405.py
@brief          This file computes the average reaction time for the user to
                press a button after an LED turns on using an external interrupt.
                
@details        This file is what is used to calculate the individual reaction 
                time per round and the average reaction time at the end of the
                game for the user. When the user initializes the game, they will
                recieve directions on how to play and how to interact with the 
                game. When enter is pressed the round begins and the user is 
                alerted to be on the lookout for the LED flash. As soon as the
                LED turns on, the user must press the blue button B2 to turn the
                LED off, and the reaction time is recorded. If the user does not
                push the button after a second that the LED is on, it will turn
                off and continue to measure the reaction time. If the time from
                when the LED turned on to when the user pushes the button is 
                greater than five seconds, the user will be notified that the 
                round timed out, and the round is not counted. Once the program 
                is started it will continue to run until it is ethier stopped 
                by the debug window or by exiting pressing Ctrl+C. When the user 
                presses Ctrl+C, the game is ended and it returns an average 
                reaction time to the user as well as the number of rounds that 
                were played.

@author         Davide Lanfranconi, Nicholas Greco
@date           06/05/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved'''

# Import modules needed to run code on Nucleo
import pyb
import utime
import random

# Define timer and Pin locations and rename for quick reference.
##@brief Initialize LED
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
_tim2 = pyb.Timer(2, freq = 20000)
_t2ch1 = _tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
##@brief Initialize Button.
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
_state = 0
_t = True
##@brief Is used to determine if button has been pressed or not.
ButtonAction = 0
##@brief Empty list to store reaction times from rounds for calculating average.
reactiontimes = [0]

#function for the user button. It is the callback function when the button is pressed.    
def ButtonPressIsPattern(IRQ_src):
    '''@brief This function is the callback function for when the button is pressed
       @details This takes in a parameter IRQ_src which recognizes the falling edge. 
       Button Action is a variable that indicates when the button is pressed
       @param IRQ_src The parameter used to  triggers the interrupt service routine. This parameter is ignored.
    '''
    global ButtonPress
    ButtonPress = 1

## @brief The callback for the user button that is triggered by the falling edge.
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=ButtonPressIsPattern)

def RandTime():
    ''' @brief This function returns a random value between 2 and 3 seconds after which the LED turns on
         @return r Returns a random value between 2 and 3 
    '''
    ##@brief Random value between 2 and 3 seconds after which the LED will turn on.
    r = random.uniform(2,3)
    return r

while True:
    try:
        if _state == 0:
            print('Welcome to the User Reaction Game. ')
            print('The objective of the game is to push the blue button as quickly'
            'as possible after the LED turns on.')
            _last_key = input('Press enter to begin and Ctrl-C to end the game')
            _state = 1
    
        elif _state == 1:
            if _t:
                print('the round will begin in a few seconds')
                _t = False
                _starttime = utime.ticks_ms()
                _cyclestart = True
                _cycleend = True
                _ledon = utime.ticks_us()
                _ledoff = utime.ticks_us()
                
            elif utime.ticks_diff(utime.ticks_ms(), _starttime) >= RandTime()*1000 and _cyclestart:
                _cyclestart = False
                _ledon = utime.ticks_us()
                _t2ch1.pulse_width_percent(100) 
                _cycleend = False
                           
            elif utime.ticks_diff(utime.ticks_us(), _ledon) >= 5000000:
                print('The round has timed out, please wait and try again')
                _state = 3
                
            elif utime.ticks_diff(utime.ticks_us(), _ledon) >= 1000000 and not _cycleend:
                _t2ch1.pulse_width_percent(0)
                _cycleend = True
            
            elif ButtonAction == 1 and not _cyclestart:
                _ledoff = utime.ticks_us()
                _t2ch1.pulse_width_percent(0)
                _ButtonAction = 0
                _t = True
                _state = 2
                
        elif _state == 2:
            ##@brief Reaction time in microseconds.
            reaction = _ledoff - _ledon
            reactiontimes.append(reaction)
            _state = 1
            
        elif _state == 3:
            _t = True
            _state = 1
        
    except KeyboardInterrupt:  # will stop the program with a control-C interrupt and display the print line below
        _n = len(reactiontimes) - 1
        if _n == 0:
            print('Thank you for playing. You have no average time as no rounds were played.')
            break
        elif _n > 0:
            ##@brief Average reaction time.
            reactave = sum(reactiontimes) / (_n*1000000)
            print('Thank you for playing. Your average reaction time was', reactave, 'seconds over the course of', _n, ' rounds.')
            _t2ch1.pulse_width_percent(0)  # Will set LED brightness to 0 before ending program
            break 