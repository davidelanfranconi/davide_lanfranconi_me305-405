'''@file        main0X03_ME405.py
@brief          The program starts when a G is recieved and sends data back to
                the PC when an S is recieved.
                
@details        The program below will run when it recieves the command to do so
                which will be in the form of the letter 'G' sent over serial.
                When a G is recieved, it moves from the Idle state to the Collect 
                Data state, where it overwrites the empty vector array filled 
                with zeros with the input time and output voltage values. Once 
                the program is done collecting voltage data, it will compile it
                and send it as a batch to the front end for processing. Once 
                all the data has been sent, the program will move back to the 
                Idle state to await reexecution.
                
                The program may be stopped at any time by using the 
                Ctrl+C keystroke. 

@author         Davide Lanfranconi
@date           06/05/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved   
'''

import pyb
import array
import micropython

## @brief Initialize the UART communication protocol between the PC and Nucleo
uart = pyb.UART(2)

## @brief Initialize the pins to detect the blue user-button on the Nucleo
pinC13 = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)

## @brief Initialize the analog-to-digital converter pin (ADC) on the Nucleo
ADC = pyb.ADC(pyb.Pin.board.PA0)

## A boolean indicating whether user button has been pressed.
ButtonPressed = False

## Buffer to catch exceptions durring interrupts
micropython.alloc_emergency_exception_buf(100)

def ButtonPressIsPattern(IRQ_src):
    '''@brief This function is the callback function for when the button is pressed
       @details This takes in a parameter IRQ_src which recognizes the falling edge. 
       Button Action is a variable that indicates when the button is pressed
       @param IRQ_src The parameter used to  triggers the interrupt service routine. This parameter is ignored.
    '''
    global ButtonPressed
    ButtonPressed = True

## @brief The callback for the user button that is triggered by the falling edge.
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,  pull=pyb.Pin.PULL_NONE, callback=ButtonPressIsPattern)

def run():
    ''' @brief FSM that controls the data collection routine.
    '''  
    ##@brief State 1: Wait for user input
    IDLE = 1
    
    ##@brief State 2: Collect voltage data
    COLLECT_DATA = 2
    
    ##@brief State 3: Send data back to PC
    TRANSMIT_DATA = 3
    
    ##@brief The state to run on the next iteration
    state = IDLE
    
    ##@brief Empty array that holds the data acquired from the ADC
    buffer = array.array('H', (0 for i in range(200)))
    
    global ButtonPressed
    while True:
        # Run State 1 Code
        if state == IDLE:
            while not uart.any() or uart.readline().decode() != 'G': # block until g is received
                pass
            ButtonPressed = False
            state = COLLECT_DATA 
                
        # Run State 2 Code    
        elif state == COLLECT_DATA:
            # Button is being held down
            if ButtonPressed:
                ADC.read_timed(buffer, 40000) # sample values from the ADC
                index = 0
                # verify that the values are in range/valid
                if buffer[-1] - buffer[0] >= 4000:
                    print('START')
                    state = TRANSMIT_DATA
                        
        elif state == TRANSMIT_DATA:
            # Send the data via UART
            print('{:f},{:f}'.format((1000.0 / 40000) * index, buffer[index])) #Convert to correct voltage values for .CSV and for plot    
            index += 1
            if index >= len(buffer):
                print('Stop') # Send a line at a time until the Stop command is recieved from the front end
                state = IDLE

        
if __name__ == '__main__':
    run() # Continuously run the FSM
