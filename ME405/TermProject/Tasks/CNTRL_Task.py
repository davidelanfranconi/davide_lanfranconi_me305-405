"""@file        CNTRL_Task.py
@brief          This task runs the code for the full-state closed-loop feedback controller.
@details        This task reads the data from the shares.py file to find the balls position and
                velocity from the Touchpad_Driver.py file as well as the position and speed of
                the platform according to the Encoder_Driver.py file. There are used to calculate what duty
                cycle the motors should be set to in the Motor_Driver.py file. The calculations for this
                controller can be found in the HMWK0X02_ME405.py, HMWK0X04_ME405.py, and HMWK0X05_ME405.py pages.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
"""
import Shares
from utime import ticks_us
import micropython
from micropython import const

class CNTRL_Task:
    ''' 
     @brief    This class contains the Closed-Loop Controller for the system.
     @details  This class uses a full-state feedback control algorithm that was
               derived in previous sections to balance the ball in the center 
               of the platform.
    '''
    
    ## @brief             Initial state.
    INIT= const(0) 
    ## @brief             Perform initial platform balance.
    BALANCE_PLATFORM = const(1)
    ## @brief             Main running state, ball is being actively balanced.
    BALANCE_BALL= const(2)
    ## @brief             Motors are off because ball was removed, fell off or is perfectly balanced.
    STOP_MOTORS = const(3)
    
    def __init__(self, kMatrixX, kMatrixY, Debug = True):
        ''' 
        @brief    Initialize the controller task.
        @details  This task will be called  by the priority scheduler to read the
                  specified values from shares.py in order to get the most up to date
                  position and velocity for the ball  as well as the angle and 
                  angular velocity for the platform.
        @param    kMatrixX K matrix for motor 1 for the full-state feedback controller. 
        @param    kMatrixY K matrix for motor 2 for the full-state feedback controller. 
        @param   Debug if True will print debug statements to console.          
        '''
        ## @brief         A flag statement for whether the data will be recorded and saved or not.
        self.dataToSave   = Shares.dataToSave
        ## @brief         Conversion factor for going from torque to duty cycle.
        self.kPrimeFactor = const(334)
        ## @brief         K matrix for motor 1 as derived analytically and tuned.
        self.kMatrixX     = kMatrixX
        ## @brief         K matrix for motor 2 as derived analytically and tuned.
        self.kMatrixY     = kMatrixY
        ## @brief         Threshold to determine if the ball is balanced in the center (mm)
        self.xThres      = const(3)
        ## @brief         Threshold to determine if the ball is balanced in the center (mm)
        self.yThres      = const(3)
        ## @brief       Debugging flag for live debug statements when troubleshooting.
        self.Debug  = Debug

    @micropython.native        
    def CNTRL_Task_Fcn(self):
        '''
        @brief      The FSM which runs the controller.
        @details    This generator is used to run the controller. It will read
                    commands from the UI_Task.py as well as send new duty cycles
                    values to the two motors. 
        '''
        ## @brief     The corrected K value for multiplying against the x position.
        K1X           = self.kPrimeFactor*self.kMatrixX[0]
        ## @brief     The corrected K value for multiplying against the y theta.
        K2X           = self.kPrimeFactor*self.kMatrixX[1]
        ## @brief     The corrected K value for multiplying against the x velocity.
        K3X           = self.kPrimeFactor*self.kMatrixX[2]
        ## @brief     The corrected K value for multiplying against the y angular velocity.
        K4X           = self.kPrimeFactor*self.kMatrixX[3]
        
        ## @brief     The corrected K value for multiplying against the y position.
        K1Y           = self.kPrimeFactor*self.kMatrixY[0]
        ## @brief     The corrected K value for multiplying against the x theta.
        K2Y           = self.kPrimeFactor*self.kMatrixY[1]*-1
        ## @brief     The corrected K value for multiplying against the y velocity.
        K3Y           = self.kPrimeFactor*self.kMatrixY[2]
        ## @brief     The corrected K value for multiplying against the x angular velocity.
        K4Y           = self.kPrimeFactor*self.kMatrixY[3]*-1

        ## @brief     Contains the information for which state the FSM is in.   
        state         = self.INIT
        ## @brief     A flag statement for whether the data will be recorded and saved or not.
        saveDataFlag  = True
        
        while True: 
            
            if state == self.INIT:
                if Shares.cmd.any():
                    ## @brief  The current command as sent from the UI_Task.py.
                    currentCmd = Shares.cmd.get()
                    
                    if currentCmd == 103:
                        print('The "g" key has been pressed, the platform is ready to balance the ball.\n')
                        state = self.FSM_Transition(state, self.BALANCE_PLATFORM)
                    elif currentCmd == 115:
                        print('The "s" key has been pressed, both motors will be stopped.\n')
                        state = self.FSM_Transition(state, self.STOP_MOTORS)
            
            elif state == self.BALANCE_PLATFORM:
            
                # If a fault occurs, transition to stop the motors.
                if Shares.MotorFault.get():
                    state = self.FSM_Transition(state, self.STOP_MOTORS)
                
                # If the user presses an `s`, transition to stop motors.
                if Shares.cmd.any():
                    currentCmd = Shares.cmd.get()
                    if currentCmd == 115:
                        print('The "s" key has been pressed, both motors will be stopped.\n')
                        state = self.FSM_Transition(state, self.STOP_MOTORS)
                    
                # Once the ball has been detected, transition states to the balancing state.    
                if Shares.BallOnPlatform.get() & 1:
                    if self.Debug:
                        print('Ball detected on platform.')
                    Shares.ZeroEncoder.put(1) # Send command to zero the encoders.
                    state = self.FSM_Transition(state, self.BALANCE_BALL)

            elif state == self.BALANCE_BALL:
                
                # If a fault occurs, transition states to stop the motors.
                if Shares.MotorFault.get():
                    state = self.FSM_Transition(state, self.STOP_MOTORS)

                # If the user presses the `s` key, transition states to stop motors.
                if Shares.cmd.any():
                    currentCmd = Shares.cmd.get()
                    if currentCmd == 115:
                        print('The "s" has been pressed, both motors will be stopped.\n')
                        state = self.FSM_Transition(state, self.STOP_MOTORS)

                # motor 1, related to x and theta_y
                ## @brief The current x position of the ball        
                cx        = Shares.x.get()
                ## @brief The current y theta of the platform (encoder data)
                cthY     = Shares.thYE.get()  
                ## @brief The current x velocity of the ball
                cxDot     = Shares.xDot.get()  
                ## @brief The current y angular velocity of the platform (encoder data)
                cthDotY  = Shares.thDotYE.get()    
                
                # motor 2, related to y and theta_x
                ## @brief The current y position of the ball
                cy        = Shares.y.get()
                ## @brief The current x theta of the platform (encoder data)
                cthX     = Shares.thXE.get()
                ## @brief The current y velocity of the ball
                cyDot     = Shares.yDot.get()
                ## @brief The current x angular velocity of the platform (encoder data)
                cthDotX  = Shares.thDotXE.get()
                
                # Save either x or y data to shares.py
                if saveDataFlag:
                    Shares.Time.put(ticks_us())
                    if self.dataToSave == 'x':
                        Shares.xData.put(cx)
                        Shares.thYData.put(cthY)
                        Shares.xDotData.put(cxDot)
                        Shares.thDotYData.put(cthDotY)
                    else:
                        Shares.yData.put(cy)
                        Shares.thXData.put(cthX)
                        Shares.yDotData.put(cyDot)
                        Shares.thDotXData.put(cthDotX)
                
                # Toggle the saveDataFlag so that every-other data point is stored
                saveDataFlag = not saveDataFlag
                
                # Determine the new duty cycle values to send to the motors.
                ## @brief The new duty cycle to be applied to motor 1
                newDutyX = (K1X*cx + K3X*cxDot)/1000  + K2X*cthY + K4X*cthDotY
                ## @brief The new duty cycle to be applied to motor 2
                newDutyY = (K1Y*cy + K3Y*cyDot)/1000  + K2Y*cthX + K4Y*cthDotX
                if self.Debug:
                    print(newDutyX, newDutyY)
                
                #Offset values due to friction
                if cx > 0:
                    newDutyX += 6
                else:
                    newDutyX -= 6 
                    
                if cy > 0:
                    newDutyY += 20
                else:
                    newDutyY -= 0
                    
                # Send update duty cycle value to motors 1 and 2.
                Shares.dutyX.put(newDutyX)
                Shares.dutyY.put(newDutyY)

            elif state == self.STOP_MOTORS:
                
                # Set the motors to a duty cycle of 0 when the user wants to stop the platform.
                Shares.dutyX.put(0)
                Shares.dutyY.put(0)
                
                if Shares.cmd.any():
                    currentCmd = Shares.cmd.get()
                    if currentCmd == 114:
                        print('"r" has been pressed, restarting the balancing process.\n'
                              'When the board has been properly reset, press "g" and place the ball back on the platform')
                            
                        # Reset parameters for the next cycle.
                        Shares.BallOnPlatform.put(0)
                        
                        state = self.FSM_Transition(state, self.INIT)
            
            yield(state)
            
    @micropython.native
    def FSM_Transition(self, state, Next_State):      # state transition method
        '''
        @brief      This function transitions between states of the FSM.
        @details    In this function you specify the current state and what state 
                    you want to transition to and the function will transition the 
                    FSM and return the new current state.
        @param      state The current state of the system.
        @param      NextState The new state the system should transition to.
        @return     The new state of the finite state machine.            
        '''
        if self.Debug:
            print('The CNTRL FSM has transitioned from ' + str(state) + ' to ' + str(Next_State) +' state.')
        return Next_State               # now that transition has been declared, update state
              