"""@file        IMU_Driver.py
@brief          This file contains the driver class needed to measure the encoder 
                angle and speed for the two encoders on our table-top kit.
@details        This file contains the Inertial Movement Unit module called BNO055. This device is used to measure
                the angular velocities and postions of our platform. Using the Micropython module I2C, the BNO055 
                can easily be configured and monitored for quick changes in velocity and positioning.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
"""
import pyb, time, ustruct


##    @var int $MODE_REG 
##    Mode register value of MCP9808 device
MODE_REG = const(0x3d)
##    @var int $ID_REG 
##    Manufactuer Identification register value of MCP9808 device
ID_REG = const(0x00)
##    @var int $POWER_REG 
##    Power register value of MCP9808 device
POWER_REG = const(0x3e)
##    @var int $CALIB_REG 
##    Calibaration status register value of MCP9808 device
CALIB_REG = const(0x35)
##    @var int $TRIGGER_REG 
##    System trigger register value of MCP9808 device
TRIGGER_REG = const(0x3f)


##    @var int $CONFIG_MODE 
##    Configuration setting value of MCP9808 device
CONFIG_MODE = const(0)
##    @var int $NDOF_MODE 
##    NDOF seeting  value of MCP9808 device
NDOF_MODE = const(0x0c)

##    @var int $TEMP_REG 
##    Normal power setting value of MCP9808 device
POWER_NORMAL = const(0x00)

class IMU:
    ''' @brief      This class contains the initalization mode of the BNO055 I2C device. With this module,
                    velocity and position can be measured in 3 directions each.
    '''
    
    def __init__(self, i2cObj):
        ''' @brief This initializes a new BNO055 object
            @param address I2C address of the BNO055
            @param i2cObj I2C object configured to MASTER mode
            @param checkIdentification The identification status of the checkID function
            @param calibrated The calibration status of the configure function
        '''
        ##@brief Address I2C address of the BNO055.
        self.address = 40
        ##@brief i2cObj I2C object configured to MASTER mode.
        self.i2cObj = i2cObj
        ##@brief The identification status of the checkID function.
        self.checkIdentification = self.checkId()
        ##@brief The calibration status of the configure function.
        self.calibrated = self.configure()
         
    def printReg(self):
        ''' @brief      This section reads and prints specified register values.
            @details    A simple helper method to read status of mode, calibration, power, and id registers 
            @return     None
        '''
        
        ##@brief The mode status of the device.
        mode = self.readfromDev(MODE_REG)
        ##@brief The calibration status of the device.
        cal = self.readfromDev(CALIB_REG)
        ##@brief The power status of the device.
        power = self.readfromDev(POWER_REG)
        ##@brief The ID status of the device.
        devid = self.readfromDev(ID_REG)
        print(devid)
        
    def checkId(self):
        ''' @brief      This section reads and checks the ID register of the device.
            @details    A simple helper method to read and verify maufacturer ID. 
            @return     An int value, which will set a flag according to a valid ID value or not 
        '''
        devid = self.readfromDev(ID_REG)
        if devid != 160:
            raise Exception("Device ID does not match")
        print("Device has matching ID.")
        return 0
            
    def configure(self):
        ''' @brief      This section triggers the wake up protocol for the device.
            @details    A helper method which kick starts the power up protocol for the BNO055 device.
                        In order to properly configure the BNO055 the write calls must be written in
                        a specific order with appropriate delays in between the write calls. At the end
                        of the wake up protocol, the BNO055 will calibrate.
            @return     An int value, which will set a flag according to a properly configured and calibrated device.
        '''
        self.writetoDev(MODE_REG, CONFIG_MODE)
        time.sleep_ms(20)
        
        self.writetoDev(TRIGGER_REG, 0x20)
        time.sleep_ms(700)
        
        self.writetoDev(POWER_REG, POWER_NORMAL)
        time.sleep_ms(20)
        
        self.writetoDev(MODE_REG, NDOF_MODE)

        print("Configure Complete. Calibration starting in 5 seconds...")
        time.sleep(5)
        self.calibrate()
        return 0
        
    def eulerPositions(self):
        ''' @brief      This section reads the value in the Euler position registers.
            @details    A helper method to read values from Euler position registers. Ustruct module
                        needed to unapck raw byte data into list for easier interpratation. 
            @return     An list structure, which contains the a list of the Euler positons 
        '''
        eulerPos = self.readBytesfromDev(0x1a, 6)
        values = ustruct.unpack('<hhh', eulerPos)
        degrees = list(values)
        return degrees
#             for i in range(len(degrees)):
#                 degrees[i] = degrees[i] / 16
#             print(degrees)

    def pitchReading(self):
        ''' @brief      This section gets the pitch reading of the device.
            @details    A method that gets the pitch reading of the Euler register. Located in
                        degrees second index.
            @return     An float value, which contains the pitch angle of the device.
        '''
        pitch = self.eulerPositions()[1] / 16.0
        return pitch
    

    def rollReading(self):
        ''' @brief      This section gets the toll reading of the device.
            @details    A method that gets the roll reading of the Euler register. Located in
                        degrees third index.
            @return     An float value, which contains the roll angle of the device.
        '''
        roll = self.eulerPositions()[2] / 16.0
        return roll


    def calibration(self, c=bytearray(4)):
        ''' @brief      This section reads from the calibration register of the device
            @details    A helper method that retrieves the calibration status of the device. The intial
                        values will be 0, but after following the physical calibration protocol, the
                        register values will increase until 3, which means that feature is fully calibrated.
            @param bytearray A bytearray used as a buffer for the calibration register values.
            @return     A byte array, which contains calibration values of the device.
        '''
        cal = self.readfromDev(CALIB_REG)
        c[0] = (cal >> 6) & 0x03   #sys
        c[1] = (cal >> 4) & 0x03   #gyro
        c[2] = (cal >> 2) & 0x03   #accel
        c[3] = cal & 0x03   #mag
        return c
    
    def calibrate(self):
        ''' @brief      This section calibrates the device.
            @details    A method that configures the magnometer, gyroscope, and accelerometer of the device.
                        Once all three systems are configured, a print status will prompt stating that the
                        device is configured. Give user 3 seconds to acknowledge.
            @return     None
        '''
        while True:
            calibResults = self.calibration()
            time.sleep(5)
            if(calibResults[0] == 3 and calibResults[1] == 3 and calibResults[2] == 3 and calibResults[3] == 3):
                break;
            print(str(calibResults))
        print("Calibration is complete.")
        time.sleep(3)
           
    def writetoDev(self, memReg, data, buf=bytearray(1)):
        ''' @brief      This section write to the specified memory register of the device.
            @details    A helper method that writes to register values from a buffer byte array.
            @param hex A memory register of the device
            @param hex Data used to write to specified memory register, has to be const.
            @param bytearray A byte array, which contains the value of the specified memory register.
            @return     None
        '''
        buf[0] = data
        self.i2cObj.mem_write(buf, self.address, memReg)
        
    def readfromDev(self, memReg, buf=bytearray(1)):
        ''' @brief      This section reads from the specified memory register of the device.
            @details    A helper method that reads specified register values into a buffer byte array of size 1.
            @param hex A memory register of the device.
            @param bytearray A byte array, which contains the value of the specified memory register.
            @return     raw memory data, Data that contains values from memReg.
        '''
        self.i2cObj.mem_read(buf, self.address, memReg)
        return buf[0]
    
    def readBytesfromDev(self, memReg, numBytes):
        ''' @brief      This section reads from the specified memory register of the device.
            @details    A helper method that reads specified register values of specified bytes.
            @param hex A register memory value
            @param int Number of bytes that should be read from the specified memory register.
            @return     A memory register value, which is interpreted into usable data.
        '''
        data = self.i2cObj.mem_read(numBytes, self.address, memReg)
        return data
    
    
    
if __name__ == "__main__":    
    
    #Configure I2C pins - necessary
    pinSDA = pyb.Pin(pyb.Pin.board.PC1, pyb.Pin.PULL_UP)   #pullup
    pinSCL = pyb.Pin(pyb.Pin.board.PC0)
        
    
    #Bus = 3
    i2cObj = pyb.I2C(3)
    i2cObj.init(pyb.I2C.MASTER, baudrate=115200)
       
    bno055device = IMU(i2cObj)

    
    while True:
        print("Roll:" + str(bno055device.rollReading()) + "      Pitch:" + str(bno055device.pitchReading()))
        time.sleep_ms(500)
    
    
    
        
        

        