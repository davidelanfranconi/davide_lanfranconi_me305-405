'''@file        UI0X03.py
@brief          This file contains the frontend for LAB0X03 to plot the charge 
                voltage curve of the capacitor on the button circuit when the 
                button was released as wel as making an excel file with the voltage values.
@details	    When the front end recieves the data, it plots the data so we 
                can better analyze and visualize the voltage curve, and it also 
                generates a .CSV file with all the data that was collected. 
@author         Davide Lanfranconi
@date           06/05/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved'''

import serial
import keyboard
from matplotlib import pyplot as plot

##@brief Initialize the serial port to communicate with the Nucleo.
ser = serial.Serial(port = 'COM7', baudrate = 115200, timeout = 1)  
##@brief Empty array that will hold the timestamps of each ADC value.
times = []
##@brief empty array that will hold the ADC Voltage values.
values = []
##@brief The user's last input or keypress
last_key = None

def kb_cb(key):
    '''@brief Callback function which is called when a key has been pressed.
       @param key The key which has been detected  as pressed on the keyboard.
       @return last_key The last key press letter or number which was detected
       by the function.
    '''
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to this particular key only
keyboard.on_release_key("g", callback=kb_cb)

def run():
    ''' @brief FSM that controls the behavior of the user interface.
    '''
    global times 
    global values
    
    ##@brief State 0: Initialization
    INIT = 0    
    
    ##@brief State 1: Wait for a response from the Nucleo
    WAIT = 1
    
    ##@brief State 2: Parse data sent by the Nucleo
    EXPORT = 2
    
    ##@brief The state to start with on the upcoming iteration
    state = INIT

    while True:
        # State 0 Code
        if state == INIT:
            global last_key
            print('To start, press G.')
            while(last_key != 'g' and last_key != 'G'):     # block until g is pressed
                pass
            ser.write('G'.encode('ascii'))                  # send g to Nucleo
            print('Press the blue button to record data.')
            state = WAIT #move to the next state
       
        # State 1 Code
        elif(state == WAIT):
            if str(ser.readline().decode('ascii')).strip() == 'START':
                state = EXPORT #move to the next state
                print('export state')
                
        # State 2 Code      
        elif(state == EXPORT):
            print('Export state')
            echo = ser.readline().decode('ascii').strip()
            while echo != 'Stop':                            # add points until 'Stop' message is sent
                point = echo.strip().split(',')
                times.append(float(point[0]))
                values.append(float(point[1]))
                echo = ser.readline().decode('ascii').strip()
            export()
            ser.close() # Close open serial port so it doesn't error on next execution
            break

def export():
    ''' @brief Plots the button press voltage vs. time and exports the individual points to a .csv file
        @details The plot is exported as "Voltage_Response_Plot.jpg", and the data is exported as "Voltage_Response_Data.csv".
    '''
    plot.plot(times, values, '.')                           # Plot ADC Voltage value vs timestamp datapoints
    plot.xlabel('Time, t [ms]')                             # Label x-axis
    plot.ylabel('ADC Voltage Value of Button [V]')    # Label y-axis
    plot.title('Button Voltage Over Time')             # Plot title
    plot.savefig('Voltage_Response_Plot.jpg')               # Save the plot
    print('The plot has been successfully exported as "Voltage_Response_Plot.jpg"')

    # Generate .csv file
    with open('Voltage_Response_Data.csv','w') as fp:
        # Go through each row of data and write data
        for i in range(len(times)):
            fp.write('{:f},{:f}\n'.format(times[i], values[i])) # Write a row of a data
                    
    print('The data has been succesfully exported as "Voltage_Response_Data.csv"')

if __name__ == '__main__':    
    run() # Continuously run the FSMG
            