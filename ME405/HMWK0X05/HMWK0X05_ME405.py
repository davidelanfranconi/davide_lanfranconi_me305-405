'''@page        HMWK0X05_ME405.py HMWK0X05: Pole Placement
@brief          This file contains the closed loop controller which we have been 
                developing in @ref HMWK0X02_ME405.py and @ref HMWK0X04_ME405.py.

@htmlonly
<embed src="HMWK0X05_ME405.html" width="100%" height="4800px">
@endhtmlonly

@image html HMWK0X04_ME405_Simulink_Model.jpg "Figure 1: Simulink model used for tuned closed loop controller simulation of ball and platform"

@section HMWK0X05_source Source Code 
The source code for this lab: 
https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305-405/src/master/ME405/HMWK0X05/

- - -
@author         Davide Lanfranconi, Anthony Vuong
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved'''