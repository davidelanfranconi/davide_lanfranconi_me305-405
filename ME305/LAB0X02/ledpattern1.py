# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 08:23:25 2021

@author: 14083
"""




import pyb
import time

pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

while True:
    t2ch1.pulse_width_percent(100)
    time.sleep(.5)
    t2ch1.pulse_width_percent(0)
    time.sleep(.5)
    


