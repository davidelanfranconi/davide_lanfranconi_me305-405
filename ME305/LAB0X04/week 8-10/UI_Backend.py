'''@file        UI_Backend.py
@brief          This file is what is used to send function calls when a 
                specific letter associated with a function is detected.
                
@details        When a G is detected the file will call on the CTRLTask and
                have it run the closed loop control cycle for 30 seconds and 
                then stop. I was unable to get this part of the code functioning
                correctly but if it was it would start the motor and then stop
                it after 30 seconds when the main.py file changed states to the 
                send data state.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/UI_Backend.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        UI_Backend               
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''
import shares
import CTRLTask
from pyb import UART


## This file is what is used to send function calls when a specific letter
#  associated with a function is detected.
#
#  Details
#  @author Davide Lanfranconi
#  @copyright Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
#  @date March 19, 2021
class main:
   
    ## Constructor for main control function
	#
	#  Detailed info on main control function
    def __init__(self):
        self.Idle = 1
        self.CollectData = 2
        self.PrintData = 3
        self.CTRLTask = CTRLTask.CTRLTask()
        self.myuart = UART(2)
        self.state = 1
        self.t = 0
        self.n = 0
        self.m = 0

#times  = array('f', list(t/10 for t in range(301)))
#values = array('f', 301*[])

    if shares.LastChar == 'G':
        CTRLTask = CTRLTask.CTRLTask()
        print('The Motor control cycle will begin')
        CTRLTask.run()
     
    