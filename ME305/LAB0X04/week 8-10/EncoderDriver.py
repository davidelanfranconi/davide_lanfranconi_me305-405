'''@file        EncoderDriver.py
@brief          This file is what is used to define the encoder driver pins, 
                update the position value, get or set the position value and
                get the difference between the current and last recorded 
                positon of the encoder.
                
@details        This file is what is used to define the pins for the encoder
                driver. The functions below are encapsulated in the EncoderDriver 
                class. They are only called by the driver control program, 
                which is the only program that interacts with the encoder driver.
                You can set the update period, get or set the reported position
                of the encoder which is based off of the actual positon of the
                encoder. You can also get the difference between the current 
                position and the last recorded position via the get_delta 
                function. When the run function is called, the encoder driver 
                modul ewill execute and start to update the encoder position
                in the, shares.py ,file regularly.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/EncoderDriver.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        encoder                
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

import pyb 
import utime

## This file is what is used to define the encoder driver pins, update the 
#  position value, get or set the position value and get the difference between
#  the current and last recorded positon of the encoder.
#
#  Details
#  @author Davide Lanfranconi
#  @copyright Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
#  @date March 19, 2021
class EncoderDriver:

    
	## Constructor for encoder driver
	#
	#  Detailed info on encoder driver constructor
    def __init__(self):
        self.state = 0
        self.pinPB6 = pyb.Pin(pyb.Pin.cpu.B6)
        self.pinPB7 = pyb.Pin(pyb.Pin.cpu.B7)
        self.timer = pyb.Timer(4, period = 65535, prescaler = 0)
        self.timer.channel(1, pyb.Timer.ENC_AB, pin = self.pinPB6)
        self.timer.channel(2, pyb.Timer.ENC_AB, pin = self.pinPB7)
        #self.pinPC6 = pyb.Pin(pyb.Pin.cpu.C6)
        #self.pinPC7 = pyb.Pin(pyb.Pin.cpu.C7)
        #self.timer = pyb.Timer(8, period = 65535, prescaler = 0)
        #self.timer.channel(1, pyb.Timer.ENC_AB, pin = self.pinPC6)
        #self.timer.channel(2, pyb.Timer.ENC_AB, pin = self.pinPC7)
        self.position = 0
        self.currentTime = utime.ticks_ms()
        self.delta = 0
        self.count = self.timer.counter()
        self.myUART = pyb.UART(2)
        print('Creating an Encoder driver')

    ## Runs the encoder script to update the position regularly
	#
	#  Detailed info on encoder run method  
    def run(self):
        self.start = utime.ticks_ms()
        if utime.ticks_diff(self.start, self.currentTime) >= 100:
            self.update
            self.currentTime = utime.ticks_ms()

    ## Sets update period and parameters for encoder position recording
	#
	#  Detailed info on encoder driver update function
	#
	#  @param period The desired update period for how often the encoder shoulf update             
    def update(self, period):
        self.delta = self.timer.counter() - self.count()
        if self.delta > -0.5*period and self.delta < 0.5*period:
            pass
        elif self.delta > 0.5*period:
            self.delta = self.delta - period
        elif self.delta < 0.5*period:
            self.delta = self.delta + period
        pass
        self.position += self.delta
        self.count = self.timer.counter()

    ## Gets the encoder's position
	#
	#  Detailed info on encoder get_position method    
    def get_position(self):
        return self.position()
   
    ## Sets the encoder position to specified value
	#
	#  Detailed info on encoder set_position method
	#
	#  @param pos The requested position for the encoder                    
    def set_position(self, pos):
        self.position = pos
  
    ## Gets the value of the difference between the encoder's current and last recorded position
	#
	#  Detailed info on encoder get_delta method                    
    def get_delta(self):
        return self.delta()