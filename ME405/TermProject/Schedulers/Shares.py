'''@file        Shares.py
@brief          This file is what is used to set and store values which are 
                used across multiple files or classes.
                
@details        The values below are setup to be initial values so the 
                parameter name exists, and when it is called used the 
                shares.FCN_NAME, where FCN_NAME is the parameter we want to 
                read or set, the inital or current value gets overwritten.
@author         Davide Lanfranconi, Anthony Vuong
@date           06/08/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''

from task_share import Share, Queue
from micropython import const


# Define all of the shared variables

BallOnPlatform = Share('I', thread_protect = True, name = 'BallOnPlatform') 

# x and y position/velocity: from touch panel to controller
x = Share('f', thread_protect = True, name ='x')
y = Share('f', thread_protect = True, name ='y')

xDot = Share('f', thread_protect = True, name ='xDot')
yDot = Share('f', thread_protect = True, name ='yDot')

# duty cycle, from controller to motor
dutyX = Share('f', thread_protect = True, name ='dutyX')
dutyY = Share('f', thread_protect = True, name ='dutyY')

MotorFault = Share('I', thread_protect = True, name ='MotorFault')

# platform angles: from Encoders to controller
thXE = Share('f', thread_protect = True, name ='thXE')          
thYE = Share('f', thread_protect = True, name ='thYE')

thDotXE = Share('f', thread_protect = True, name ='thDotXE')
thDotYE = Share('f', thread_protect = True, name ='thDotYE')

ZeroEncoder = Share('I', thread_protect = True, name = 'ZeroEncoders')

# platform angles: from IMU to controller (Will integrate if time allows) 
thXI = Share('f', thread_protect = True, name ='thXI')
thYI = Share('f', thread_protect = True, name ='thYI')

thDotXI = Share('f', thread_protect = True, name ='thDotXI')
thDotYI = Share('f', thread_protect = True, name ='thDotYI')

# Define all queues used by tasks

Queue_Length = const(500)   
overWriteVar = True
dataToSave = 'y'

Time = Queue('I', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'Time')

if dataToSave == 'x':
    xData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'xData')
    xDotData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'xDotData')
    thYData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'thYData')
    thDotYData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'thDotYData')
    
else:     
    yData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'yData')
    yDotData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'yDotData')
    thXData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'thXData')
    thDotXData = Queue('f', Queue_Length, thread_protect = True, overwrite = overWriteVar, name = 'thDotXData')


cmd = Queue('I', 10, thread_protect = True, overwrite = overWriteVar, name = 'Commands')
