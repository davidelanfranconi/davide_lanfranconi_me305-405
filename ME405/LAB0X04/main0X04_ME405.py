'''@file        main0X04_ME405.py
@brief          This file is used to collect the temeperature readings from the
                Nucleo and from the MCP9808 temperature sensor and create a .CSV
                file with the readings over an 8+ hour long time span
@details        This file uses the Nucleo_Temp and MCP9808 classes to take temperature
                recordings about once a minute and record the results in a .CSV file.
                The program checks the corret sensor is plugged in, and once it
                verifies that it will start to collect data. Once a user presses Ctrl+C,
                the program will stop data collection and finish saving the results 
                in the .CSV file, which is accessible below.\n
                 
@author         Davide Lanfranconi
@date           06/06/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
'''
from MCP9808 import MCP9808
import ADCA11
from pyb import I2C
import utime



## Object for internal temperature measurement.
ADCA11_temp = ADCA11.Nucleo_Temp()

## @brief I2C object setting the Nucleo as master device.
i2c = I2C(1, I2C.MASTER, baudrate = 115200)

## MCP9808 object.
sensor = MCP9808(i2c, 0)

try:
    if sensor.check():
        print('MCP98098 sensor has been found. Beginning data collection')
        print('You may press Ctrl+C to stop data collection at any time.')
        
        ##@brief The time when data collection began in milliseconds.
        start = utime.ticks_ms()
        
        ##@brief The end time is set to time out 8 hours after beginning data collection.
        stop = start + 1000*60*60*8
        
        with open('Recorded_Temperatures.csv', 'w') as fp:
            while utime.ticks_ms() < stop:
                ##@brief Time elapsed since starting the data collection in minutes
                Time = utime.ticks_diff(utime.ticks_ms(), start) / 60000 # record time in minutes
                ##@brief Temperature recorded by the STM32 onboard thermometer in degrees Celsius
                Nucleo_STM32_temp = ADCA11_temp.Nucleo_Temp()
                ##@brief Temperature recorded by the MCP8908 board in degrees Celsius
                MCP9808_temp = sensor.celsius() # read MCP9808 sensor temperature
                
                print('Time: ' + str(Time) + 'min')
                print('Nucleo Board Temperature: ' + str(Nucleo_STM32_temp) + ' C')
                print('MCP9808 Board Temperature: ' + str(MCP9808_temp) + ' C\n')
                        
                fp.write('{:},{:},{:}\r\n'.format(Time, Nucleo_STM32_temp, MCP9808_temp))
                utime.sleep(60) # wait 60s before taking next measurement
            print('8 hours has elapsed, Data collection will now conclude.\n')
        
    else:
        print('MCP8908 Sensor was not found, please verify proper wiring and test again.')
            

except KeyboardInterrupt:
    print('Thank you for using the temperature data collection script. Data collection will now terminate.\n')
    


