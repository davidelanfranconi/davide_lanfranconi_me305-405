'''@page        LAB0X03_ME405.py
@brief          This file contains the two parts of lab0X03 which was for making a plot of the charge voltage curve of the capacitor on the button circuit when the button was released.
@details	This lab was composed of two separate parts. In the main.py, which is the part that runs on the Nucleo, we take care of determining when to measure the charge voltage on the button and we use an array to record all of our values. When we have recorded all the values, the data gets transmitted via serial to the frontend of the lab, LAB0X03.py. When the front end recieves the data it plots the data so we can better analyze and visualize the voltage curve, and it also generates a .CSV file with all the data that was collected. 
@author         Davide Lanfranconi
@date           05/06/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved'''




''' @file main0x03.py
    @brief A back-end script to collect data of a button press using ADC.
    @details This script runs on a Nucleo. It waits for communication from the 
             front-end user interface to begin data collection. Upon reciept of 
             a \'G,\' the FSM will wait for the user to press the blue button, 
             and sample the voltage (ADC value) step response. When a good step 
             response has been measured, the data will be transmitted back to the 
             PC in a batch.
    @package Lab0x03
    @brief This package contains main0x03.py and ui.py.
    @author Jeremy Szeto
    @date May 6, 2021
'''
import pyb
import array
import micropython

## @brief Initialize UART communication between PC and Nucleo
uart = pyb.UART(2)
## @brief Initialize the blue user-button on the Nucleo
button = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)
## @brief Initialize the analog-to-digital converter (ADC) on the Nucleo
ADC = pyb.ADC(pyb.Pin.board.PA0)
## A boolean indicating whether user button has been pressed.
pressed = False
## Buffer to catch exceptions durring interrupts
micropython.alloc_emergency_exception_buf(100)
        
def callback(pin):
    ''' @brief An interupt callback function that runs when the user button is pressed.
        @details This function sets a variable high indicating that the button has been pressed.
        @param pin The pin which triggers the interrupt service routine. This parameter is ignored.
    '''
    global pressed
    pressed = True
        
## @brief The callback for the user button that activates on the falling edge.
extint = pyb.ExtInt(button, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback)

def run():
    ''' @brief FSM that controls the data collection routine.
    '''  
    ## @brief State 1: Wait for user input
    S1_WAIT = 1
    ## @brief State 2: Collect voltage data
    S2_COLLECT_DATA = 2
    ## @brief State 3: Send data back to PC
    S3_SEND_DATA = 3
    ## @brief The state to run on the next iteration
    state = S1_WAIT
    ## @brief Array that holds the data acquired from the ADC
    buffy = array.array('H', (0 for i in range(200)))
    
    global pressed
    while True:
        # Run State 1 Code
        if state == S1_WAIT:
            while not uart.any() or uart.readline().decode('ascii') != 'G': # block until g is received
                pass
            pressed = False
            state = S2_COLLECT_DATA 
                
        # Run State 2 Code    
        elif state == S2_COLLECT_DATA:
            # Button is being held down
            if pressed:
                ADC.read_timed(buffy, 40000) # sample values from the ADC
                index = 0
                # Check that the values are good
                if buffy[-1] - buffy[0] >= 4000:
                    print('START')
                    state = S3_SEND_DATA
                        
        elif state == S3_SEND_DATA:
            # Send the data via UART
            print('{:f},{:f}'.format((1000.0 / 40000) * index, buffy[index]))     
            index += 1
            if index >= len(buffy):
                print('END')
                state = S1_WAIT
        
if __name__ == '__main__':
    run() # Continuously run the FSM