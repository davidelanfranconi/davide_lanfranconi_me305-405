# -*- coding: utf-8 -*-
"""
Created on Sun Jan 24 23:31:26 2021

@author: 14083
"""
import math
import time
import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
x = 0.01
while True:
    sawtoothout = 100*((-1*(1/math.pi)*math.atan(math.cos(x*math.pi)/math.sin(x*math.pi)))+.5)
    t2ch1.pulse_width_percent(sawtoothout)
    time.sleep(0.01)
    x += 0.01