'''@file        LAB0X02.py
@brief          Displays three different light patterns which can be toggled 
                with the button B2.
@details        The program has three different light patterns that have been 
                programmed in, and they can be toggled using the built in blue 
                button, B2, on the Nucleo board. All patterns are displayed on 
                the green LED on the Nucleo board, LD2. The program will 
                initialize when run and give the users instructions on how to
                interact with the program, and each time the button is pressed
                the Nucleo will print out the light pattern that is currently
                selected. The light pattern will run continously until the
                button is pressed, at which point it will move to the next
                pattern. The program may be stopped at any time by using the 
                Ctrl+C keystroke. 
                LED Pattern 1 is a square wave varying in intensity from 
                0-100%, and has a period of 1 second.
                LED pattern 2 is a sinusoidal wave with a period of 10 seconds.
                The wave starts at half brightness and goes to full brightness
                then down to 0 brightness and then back to half brightness.
                LED pattern 3 is a saw-tooth wave with a period of 1 second. 
                The wave starts a 0% brightness and builds rapidly and 
                linearly to 100% brightness, then the LED instantly drops to
                0% brightness and the cycle repeats.
                Each press of button B2 will result in the program being 
                cycled to the next LED pattern, and holding down the button 
                will still result in only one LED pattern being cycled.
                
                The documented source code can be found at the following link
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB%200X02/LAB0X02.py
                
                A demonstration of the program can be viewed here:
               
                https://drive.google.com/file/d/1eC7tjvZGR_A46-qFmSXzr19n04ztcu00/view?usp=sharing
                
@image          html LAB0X02_State_Diagram.jpg
@author         Davide Lanfranconi
@date           02/03/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''
# Import modules needed to run code on Nucleo
import pyb
import time
import math

# Define timer and Pin locations and rename for quick reference
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13) 
count = 0
start = time.ticks_ms()


def onButtonPressFCN1(IRQ_src):
    '''@brief Advances count by 1 when button B2 is pressed.
       @details Each time the button is pressed, the count advances by 1, 
       bringing the Nucleo into the next pattern. If the count is not the one
       for that specific pattern, it is terminated and the correct pattern is
       displayed. the Nucleo will print status messages to the console window
       each time the button is pressed so you know which mode you have 
       selected in addition to seeing the pattern on the board itself.
       @param IRQ_src Detects when a falling edge is present from button B2.
       @return Advances the count by 1, no visible output in command prompt.
    '''
    global count
    count += 1
    
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN1)    
    
t2ch1.pulse_width_percent(0) # Set LED brighness to 0 before starting program
print('Welocme to the Blinky LED board. Press the blue B2 button on the Nucleo'
      'board to begin the program. Each button press will advance you by one '
      'program, in the following order:')
print(  'initialize -> sqare wave pattern')
print(  'square wave pattern -> sinusoidal wave pattern')
print(  'sinusoidal wave pattern -> saw-tooth pattern')
print(  'saw-tooth pattern -> square wave pattern')
print('Additionally you will get a status message each time you press the '
     'button as a reminder for which mode you have selected.')
print('push the button and enjoy !!! :-) ')

# Display the above print statement as long as the pattern has not been initialized yet
while True:
    if count == 0:
        start = time.ticks_ms()
        while time.ticks_diff(time.ticks_ms(), start) <= 100:
            a = 0
    else:
        break

# Main code for the LED light patterns
while True:
    try:
        if count == 1:
            print('You have selected the square wave pattern.')
            
            while True:
                start = time.ticks_ms()
                while time.ticks_diff(time.ticks_ms(), start) <= 500:
                    t2ch1.pulse_width_percent(100)
                    if count != 1:
                        break
                    
                while time.ticks_diff(time.ticks_ms(), start) <= 1000:
                    t2ch1.pulse_width_percent(0)
                    if count != 1:
                        break
                
                if count != 1:
                    break
 
        elif count == 2:
            print('You have selected the sine wave pattern.') 
            x = 0
            while True:
                start = time.ticks_ms()
                sineout = 50 * (math.sin (((2*math.pi)/10)*x)+1)
                t2ch1.pulse_width_percent(sineout)    
                while time.ticks_diff(time.ticks_ms(), start) <= 100:
                    a = 0
                    if count != 2:
                        break
                
                x += 0.1
                if count != 2:
                    break
                        
        else:
            count = 0
            print('You have selected the saw-tooth wave pattern.')        
            while True:
                start = time.ticks_ms()
                while time.ticks_diff(time.ticks_ms(), start) <= 1000:    
                    t = time.ticks_ms() / 1000 
                    duty = 100*(t % 1)
                    t2ch1.pulse_width_percent(duty)
                    if count != 0:
                        break
                
                if count != 0:
                    break
                

    except KeyboardInterrupt:  # will stop the program and display the print line below
        print('Thank you for playing with the Nucleo Light Pattern Program ')   
        t2ch1.pulse_width_percent(0)  # Will set LED brightness to 0 before ending program
        break 



