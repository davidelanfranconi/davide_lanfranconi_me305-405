'''@file        LAB0X01_ME405.py
@brief          This file contains the code necessary to run the Vendotron Soda
                vending machine for multiple brands of soda and types of 
                denominations.
                
@details        This file is what is used to run the Vendotron vending machine.
                The machine initializes when you run the program and gives an
                intro message with instructions on how to select and pay for 
                your soda. It can take all valid currency from a penny up to a 
                $20 bill and displays the current balance when money is inserted.
                A drink may be selected at any time, at which point if there are
                sufficinet fund in the machine the drink will be dispensed. If
                ther are not sufficinet funds in the machine you will be prompted
                to enter additional funds. Once the soda has been dispensed you
                can either choose to buy another soda if there are remaining 
                funds or press E to get the remaining change from the machine. 
                If there is no change at the end of the transaction the machine 
                will go back to the initialized state. While the machine is idle
                it also displays a promotional message every 15 seconds. Once 
                the program is started it will continue to run until it is 
                either stopped by the debug window or by exiting pressing Ctrl+C.

@author         Davide Lanfranconi
@date           06/05/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved'''
    

import keyboard 
import time

##@brief The user's last input to Vendotron.
last_key = None

def kb_cb(key):
    '''@brief Callback function which is called when a key has been pressed.
       @param key The key which has been detected  as pressed on the keyboard.
       @return last_key The last key press letter or number which was detected
       by the function.
    '''
    global last_key
    last_key = key.name
    
# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("C", callback=kb_cb)
keyboard.on_release_key("P", callback=kb_cb)
keyboard.on_release_key("S", callback=kb_cb)
keyboard.on_release_key("D", callback=kb_cb)
keyboard.on_release_key("E", callback=kb_cb)
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)

def getChange(price, payment):
    
    ''' @brief This function computes the change for a transaction with the fewest
        nominations possible
        @details This function uses modulus and floor functions to calculate the 
        correct amount of each denomination needed to make change for the transaction
        with the fewest number of denominations possible. It does so by calculating
        the number of the greatest nomination needed first and moves down to the 
        smallest nomination until the total change is the same as what was 
        calculated. The function can be interrupted or stopped by a keyboard 
        interrupt, or with the keystroke of Ctrl+C.    
        @param price An zero or positive value in dollars and cents specifing the
        price of the item 
        @param payment The total payment by the customer for the item, which is 
        calculated based on the resposnes to the prompts about the quantity of the 
        different nomination types.
        @return The function returns the number needed of each bill or coin type to 
        make correct change for the transaction.
    '''
    global change
    change = (payment - price)
    # math floor of 2000
    twentiesout = change // 2000
    # mod of 2000 and floor of 1000
    tensout = change % 2000 // 1000
    # mod of 2000 and 1000 and floor of 500
    fivesout = change % 2000 % 1000 // 500
    # mod of 2000 and 1000 and 500 and floor of 100
    onesout = change % 2000 % 1000 % 500 // 100
    # mod of 2000 and 1000 and 500 and 100 and floor of 25
    quartersout = change % 2000 % 1000 % 500 % 100 // 25
    # mod of 2000 and 1000 and 500 and 100 and 25 and floor of 10
    dimesout = change % 2000 % 1000 % 500 % 100 % 25 // 10
    # mod of 2000 and 1000 and 500 and 100 and 25 and 10 and floor of 5
    nicklesout = change % 2000 % 1000 % 500 % 100 % 25 % 10 // 5
    # mod of 2000 and 1000 and 500 and 100 and 25 and 10 and 5 and floor of 1
    penniesout = change % 2000 % 1000 % 500 % 100 % 25 % 10 % 5 //1

    if twentiesout !=0 or tensout !=0 or fivesout !=0 or onesout !=0 or quartersout != 0 or dimesout!=0 or nicklesout!=0 or penniesout!=0:
        return change
    else:
        pass

def VendotronTask():
    ''' @brief This function contains the FSM which actually runs the Vendotron soda
        vending machine.
        @details This function has an initialization state which tells the user how 
        to purchase and pay for a soda. It then goes to the main state where a soda
        can be selected or money can be inserted. Once a soda is selected and 
        sufficient funds are present the soda is dispensed and the machine will 
        allow you to either purchase another drink or return your change if there 
        is still a positive balance present. If no funds are left it will display a 
        thank you message and return to the initialization state. The function can 
        be interrupted or stopped by a keyboard interrupt, or with the keystroke of 
        Ctrl+C.    
    '''

    state = 0
    global last_key
    last_key = None
    timeout = time.time()
    print(timeout)
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        
        
        if state == 0:
            # Print Introduction Message for user            
            print('Welcome to the Vendotron')
            print('Please select a drink or insert a coin')
            print('Press E for eject, C for Cuke, P for Popsi, S for Spryte, and D for Dr. Pupper.') 
            print('Coins/currency is defined as follows:')
            print('0=Penny, 1=Nickle, 2=Dime, 3=Quarter, 4=OneDollar, 5=FiveDollar, 6=TenDollar, 7=TwentyDollar')
            price = 0
            drink = 0
            state = 1 # on the next iteration, the FSM will run state 1
            global payment
            payment = 0
            
        elif state == 1:
            # perform state 1 operations
            
            if time.time() - timeout > 15:
                print('Have you tried Cuke? Only $1.00 for an ice cold soda.')
                timeout = time.time()
            
            if last_key =='C':
                print(payment)
                last_key = None
                drink = 1
                print("The price of a Cuke is $1.00")
                price = 100
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2
            
            elif last_key =='P':
                last_key = None
                drink = 2
                print("The price of a Popsi is $1.20")
                price = 120
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2
            
            elif last_key =='S':
                last_key = None
                drink = 3
                print("The price of a Spryte is $0.85")
                price = 85
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2
            
            elif last_key =='D':
                last_key = None
                drink = 4
                print("The price of a Dr. Pupper is $1.10")
                price = 110
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2            
            
            elif last_key == '0':
                last_key = None
                print("You have inserted a penny.")
                payment += 1
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2
    
            elif last_key == '1':
                last_key = None
                print("You have inserted a nickle.")
                payment += 5
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2

            elif last_key == '2':
                last_key = None
                print("You have inserted a dime.")
                payment += 10
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2

            elif last_key == '3':
                last_key = None
                print("You have inserted a quarter.")
                payment += 25
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2

            elif last_key == '4':
                last_key = None
                print("You have inserted a one dollar bill.")
                payment += 100
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2

            elif last_key == '5':
                last_key = None
                print("You have inserted a five dollar bill.")
                payment += 500
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2
            
            elif last_key == '6':
                last_key = None
                print("You have inserted a ten dollar bill.")
                payment += 1000
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2
            
            elif last_key == '7':
                last_key = None
                print("You have inserted a twenty dollar bill.")
                payment += 2000
                print('The current balance is $', payment/100,)
                if payment < price:
                    print('Please insert allowed currency into the Vendotron')
                else:
                    state = 2
            
            elif last_key == 'E':
                last_key = None
                getChange(price, payment)
                print('Please collect your change of $', change/100, 'below.')
                payment = 0
                print('The current balance is $', payment/100,)
                state = 0
            
            else:
               pass
        
        elif state == 2:
            # perform state 2 operations
            if drink == 0:
                state = 1
            
            if drink == 1:
                drink = 0
                print('Vending Cuke, please collect from the recpeticle below.')
                getChange(price, payment)
                if change == 0:
                    print('Thank you for your puchase, have a nice day.')
                    state = 0
                elif change > 0:
                    print('Thank you for your purchase, please enjoy your ice cold drink.')
                    print('Your balance is currently $', change/100)
                    print('Please select another drink or press E for your change.')
                    payment = change
                    price = 0
                    state = 1
            
            if drink == 2:
                drink = 0
                print('Vending Popsi, please collect from the recpeticle below.')
                getChange(price, payment)
                if change == 0:
                    print('Thank you for your puchase, have a nice day.')
                    state = 0
                elif change > 0:
                    print('Thank you for your purchase, please enjoy your ice cold drink.')
                    print('Your balance is currently $', change/100)
                    print('Please select another drink or press E for your change.')
                    payment = change
                    price = 0
                    state = 1
            
            if drink == 3:
                drink = 0
                print('Vending Spryte, please collect from the recpeticle below.')
                getChange(price, payment)
                if change == 0:
                    print('Thank you for your puchase, have a nice day.')
                    state = 0
                elif change > 0:
                    print('Thank you for your purchase, please enjoy your ice cold drink.')
                    print('Your balance is currently $', change/100)
                    print('Please select another drink or press E for your change.')
                    payment = change
                    price = 0
                    state = 1
            
            if drink == 4:
                drink = 0
                print('Vending Dr. Pupper, please collect from the recpeticle below.')
                getChange(price, payment)
                if change == 0:
                    print('Thank you for your puchase, have a nice day.')
                    state = 0
                elif change > 0:
                    print('Thank you for your purchase, please enjoy your ice cold drink.')
                    print('Your balance is currently $', change/100)
                    print('Please select another drink or press E for your change.')
                    payment = change
                    price = 0
                    state = 1

if __name__ == "__main__":
    ## Initialize code to run FSM (not the init state). This means building the iterator as generator object
    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            _start = time.time()
            if time.time()/1000 - _start/1000 == 10:
                next(vendo)
                _start = time.time()
                      
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
        keyboard.unhook_all()
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')

