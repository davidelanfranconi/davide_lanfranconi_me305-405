'''@file        HW0X02.py
@brief          Simulates an elevator moving between two floors
@details        Implements a finite state machine, shown below, to simulate
                the behavior of an elevator between two floors with sensors to
                detect the elevator position. The elevator can be commanded to 
                move with an up or down button, and has built in error 
                detection so if the elevator is already at the top and the up
                button is pressed, the elevator doesn't move. The state 
                machine is set up so it also doesn't move if the elevator is 
                already at the bottom and the down button is pressed.
                The documented source code can be found at the following link:
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/HW0X02/HW0X02.py
                
@author         Davide Lanfranconi
@date           1/27/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
'''

import time
import random


def motor_cmd(cmd):
    '''@brief Commands the motor to move or stop
       @details Depending on which command is selected, the motor will either
       move upwards, downwards, or stop. Each time the motor executes the 
       command, the appropriate state message will print to the command line 
       to inform the user of the current motor state.           
       @param cmd The command to give the motor
       @return The function returns print statements stating if the motor is
       moving upwards, downwards, or stopping based on which input command is
       given to the function.
    '''
    if cmd=='UP':
        print('Motor moving upwards')
    elif cmd=='DOWN':
        print('Motor moving downwards')
    elif cmd=='STOP':
        print('Motor stopped')

def second_floor_sensor():
    '''@brief Gives a random T/F as to whether the elevator is at floor 2.
       @details This function has a random T/F generator that continously 
       returns the state of the sensor. If true is given as an output state,
       it means that the elevator is currently at the second floor. If false 
       is given as the output state, then it means the elevator is at the 
       first floor or somewhere in transit between the two floors. The output 
       of this function is then used by the state machine in state 3 to know 
       when to stop the elevator.
       @param True, False These are randomly selected by the random.choice
       utility and then given as outputs to be used by the IF statements below
       to control the elevator behavior.
       @return True or false output which is used by the IF statements to
       determine state machine behavior.
    '''
    return random.choice([True, False]) # randomly returns T or F

def first_floor_sensor():
    '''@brief Gives a random T/F as to whether the elevator is at floor 1.
       @details This function has a random T/F generator that continously 
       returns the state of the sensor. If true is given as an output state,
       it means that the elevator is currently at the first floor. If false 
       is given as the output state, then it means the elevator is at the 
       second floor or somewhere in transit between the two floors. The output 
       of this function is then used by the state machine in state 1 to know 
       when to stop the elevator.
       @param True, False These are randomly selected by the random.choice
       utility and then given as outputs to be used by the IF statements below
       to control the elevator behavior.
       @return True or false output which is used by the IF statements to
       determine state machine behavior.
    '''
    return random.choice([True, False]) # randomly returns T or F

def up_button():
    '''@brief Gives a random T/F as to whether the up button is pressed.
       @details This function has a random T/F generator that continously 
       returns the state of the sensor. If true is given as an output state,
       it means that the up button is currently selected. If false is given as 
       the output state, then it means the up button is not currently pressed.
       The output of this function is used to control the elevator at state 2
       and command it to move up to state 3. There is also built in logic 
       detection so that if the button is pressed when the elevator is 
       already at floor 2, it will stay stopped and won't move.
       @param True, False These are randomly selected by the random.choice
       utility and then given as outputs to be used by the IF statements below
       to control the elevator behavior.
       @return True or false output which is used by the IF statements to
       determine state machine behavior.
    '''
    return random.choice([True, False]) # randomly returns T or F

def down_button():
    '''@brief Gives a random T/F as to whether the down button is pressed.
       @details This function has a random T/F generator that continously 
       returns the state of the sensor. If true is given as an output state,
       it means that the down button is currently selected. If false is given 
       as the output state, then it means the down button is not currently 
       pressed. The output of this function is used to control the elevator at 
       state 4 and command it to move down to state 1. There is also built in 
       logic detection so that if the button is pressed when the elevator is 
       already at floor 1, it will stay stopped and won't move.
       @param True, False These are randomly selected by the random.choice
       utility and then given as outputs to be used by the IF statements below
       to control the elevator behavior.
       @return True or false output which is used by the IF statements to
       determine state machine behavior.
    '''
    return random.choice([True, False]) # randomly returns T or F

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state
    
    while True:
        try:
            # main program code goes here
            if state==0:
                # run state 0 (init) code
                print('S0')
                motor_cmd('DOWN') # Command motor to go downwards
                state = 1   # Updating state for next iteration
                
            elif state==1:
                # run state 1 (moving downwards) code
                print('S1')
                # If we are at the first floor, stop the motor and 
                #transition to S2
                if first_floor_sensor():
                    motor_cmd('STOP')  # Command motor to stop
                    motor_cmd('UP') # Command motor to go stop or stay stopped
                    state = 2 # Updating state for next iteration
                 
            elif state==2:
                # run state 2 (stopped at first floor) code
                print('S2')
                # If we are at the first floor and the downwards button is 
                # pressed, keep motor stopped and stay at state 2
                if down_button():
                    motor_cmd('STOP') # Command motor to stay stopped
                # if up button is pressed, start motor and transition to S3
                if up_button():
                    motor_cmd('UP') # Command motor to go upwards
                    state = 3 # Updating state for next iteration
                
            elif state==3:
                # run state 3 (moving upwards) code
                print('S3')
                # If we are at the second floor, stop the motor and 
                # transition to S4
                if second_floor_sensor():
                    motor_cmd('STOP') # Command motor to stop
                    state=4 # Updating state for next iteration
                
            elif state==4:
                # run state 4 (stopped at second floor) code
                print('S4')
                # If we are at the second floor and the upwards button is 
                # pressed, keep motor stopped and stay at state 4
                if up_button():
                    motor_cmd('STOP') # Command motor to stay stopped
                # if down button is pressed, start motor and transition to S1    
                if down_button():
                    motor_cmd('DOWN') # Command motor to go downwards
                    state = 1   # Updating state for next iteration
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
            
            # Slow down execution of FSM so we can see output in console
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break

    # Program de-initialization goes  here