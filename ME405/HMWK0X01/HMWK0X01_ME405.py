'''@file        HMWK0X01_ME405.py
@brief          This file computes the change in the fewest denominations
                possible for a given price and tender or payment amount.
                
@details        This file is what is used to calculate the change that is owed
                to a customer after a transaction. The program will prompt the
                cashier on end user to enter the price of the item, and then it
                will ask for the payment amount by asking for the amount of each
                denomination. The program then returns the total change and 
                specifies the amount of each denomination needed to make the
                total change. The program takes the price input in the format 
                of dollar.cents. For example three dolars and twentyfive cents 
                would be 3.25 with no dollar sign or the program will reprompt
                you for the value in the right format. enter the quantity of 
                bills or coins as integers with no decimals. The program also
                has error detection built in, so if any of the values are not 
                numbers it will reprompt for the value. In addition, if any of
                the prices or denomination amount values are less than zero, or
                if the payment total is less than the price the program will
                print a message letting you know the issue and reprompt you for
                the values. Once the program is started it will continue to run
                until it is ethier stopped by the debug window or by exiting
                pressing Ctrl+C.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305-405/src/master/ME405/HMWK0X01/HMWK0X01.py

@author         Davide Lanfranconi
@date           04/06/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved'''

def getChange(price, payment):
    '''
    @brief This function computes the change for a transaction with the fewest
    nominations possible
    @details This function uses modulus and floor functions to calculate the 
    correct amount of each denomination needed to make change for the transaction
    with the fewest number of denominations possible. It does so by calculating
    the number of the greatest nomination needed first and moves down to the 
    smallest nomination until the total change is the same as what was 
    calculated. The function can be interrupted or stopped by a keyboard 
    interrupt, or with the keystroke of Ctrl+C.    
    @param price An zero or positive value in dollars and cents specifing the
    price of the item 
    @param payment The total payment by the customer for the item, which is 
    calculated based on the resposnes to the prompts about the quantity of the 
    different nomination types.
    @return The function returns the number needed of each bill or coin type to 
    make correct change for the transaction.
    '''
    change = (payment - price)
    # math floor of 2000
    twentiesout = change // 2000
    # mod of 2000 and floor of 1000
    tensout = change % 2000 // 1000
    # mod of 2000 and 1000 and floor of 500
    fivesout = change % 2000 % 1000 // 500
    # mod of 2000 and 1000 and 500 and floor of 100
    onesout = change % 2000 % 1000 % 500 // 100
    # mod of 2000 and 1000 and 500 and 100 and floor of 25
    quartersout = change % 2000 % 1000 % 500 % 100 // 25
    # mod of 2000 and 1000 and 500 and 100 and 25 and floor of 10
    dimesout = change % 2000 % 1000 % 500 % 100 % 25 // 10
    # mod of 2000 and 1000 and 500 and 100 and 25 and 10 and floor of 5
    nicklesout = change % 2000 % 1000 % 500 % 100 % 25 % 10 // 5
    # mod of 2000 and 1000 and 500 and 100 and 25 and 10 and 5 and floor of 1
    penniesout = change % 2000 % 1000 % 500 % 100 % 25 % 10 % 5 //1

    
    
    if twentiesout !=0 or tensout !=0 or fivesout !=0 or onesout !=0 or quartersout != 0 or dimesout!=0 or nicklesout!=0 or penniesout!=0:
        print ('The change of $', round(payment,2)/100, 'is: $',change/100,'\n-twenties:',int(twentiesout),'\n-tens:',int(tensout),'\n-fives:',int(fivesout),'\n-dollars:',int(onesout),'\n-quarters:',int(quartersout),'\n-dimes:',int(dimesout),'\n-nickels:',int(nicklesout),'\n-pennies:',int(penniesout))
    else:
        print ('The change from $', round(payment,2)/100, 'is: $',change/100,'and no coins')

if __name__ == '__main__':   

    # The code below will run only when the script is executed as a standalone
    # program
    while True:
        try:
            price = 100*float(input("Please enter the price of the item: "))
            twentiesin = int(input("Please enter the number of $20 bills: "))
            tensin = int(input("Please enter the number of $10 bills: "))
            fivesin = int(input("Please enter the number of $5 bills: "))
            onesin = int(input("Please enter the number of $1 bills: "))
            quartersin = int(input("Please enter the number of quarters: "))
            dimesin = int(input("Please enter the number of dimes: "))
            nicklesin = int(input("Please enter the number of nickels: "))
            penniesin = int(input("Please enter the number of pennies: "))
            payment = 2000*twentiesin+1000*tensin+500*fivesin+100*onesin+25*quartersin+10*dimesin+5*nicklesin+1*penniesin
            print('The total price is $',price/100,)
            print('Tender amount is $',payment/100,)
            
                        
            if price < 0:
                print('Please enter a valid price for the item')
                
            elif twentiesin < 0:
                print('Please enter a valid tender amount')
                
            elif tensin < 0:
                print('Please enter a valid tender amount')
            
            elif fivesin < 0:
                print('Please enter a valid tender amount')
            
            elif onesin < 0:
                print('Please enter a valid tender amount')
            
            elif quartersin < 0:
                print('Please enter a valid tender amount')
                
            elif dimesin < 0:
                print('Please enter a valid tender amount')
                
            elif nicklesin < 0:
                print('Please enter a valid tender amount')
                
            elif penniesin < 0:
                print('Please enter a valid tender amount')

            while payment < price:
                print('Tender insufficient. Please enter a tender amount greater than the price')
                twentiesin = int(input("Please enter the number of $20 bills: "))
                tensin = int(input("Please enter the number of $10 bills: "))
                fivesin = int(input("Please enter the number of $5 bills: "))
                onesin = int(input("Please enter the number of $1 bills: "))
                quartersin = int(input("Please enter the number of quarters: "))
                dimesin = int(input("Please enter the number of dimes: "))
                nicklesin = int(input("Please enter the number of nickels: "))
                penniesin = int(input("Please enter the number of pennies: "))
                payment = 2000*twentiesin+1000*tensin+500*fivesin+100*onesin+25*quartersin+10*dimesin+5*nicklesin+1*penniesin
                print('Tender amount is $',payment/100,)
                
            else:
                getChange(price, payment)

        except ValueError:  # will trigger when an invalid input such as a letter is entered
            print('Please enter a valid value')
            
        except KeyboardInterrupt:  # will stop the program and display the print line below
            break 

    print('Thanks for using the change calculator. Please come again soon. :-)')