"""@file        ADCA11.py
@brief          This file contains the class which is used to measure the internal
                temperature of the microcontroller.
@details        This file uses the pyb.ADCA11 class to measure the internal 
                temperature of the microcontroller.
@author         Davide Lanfranconi
@date           06/06/21
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved
"""

import pyb

## Sets ADCAll to have a 12 bit resolution and to have all analog inputs set to active
adcall = pyb.ADCAll(12, 0x70000)

class Nucleo_Temp:
    ''' @brief  This class allows the end user to  to measure the ambient 
                temperature using the built in thermometer on the Nucleo STM32
                Board.
    '''
    
    def Nucleo_Temp(self):
        ''' @brief Measures the internal temperature of the microcontroller in 
            degrees celsius.
            @returns ADCA11_temp The Nucleo Board temperature in degrees celsius.
        '''
        adcall.read_vref()
        
        ##@brief The recorded temperature from the STM32 board in degrees Celsius
        self.ADCA11_temp = adcall.read_core_temp()
        return self.ADCA11_temp
