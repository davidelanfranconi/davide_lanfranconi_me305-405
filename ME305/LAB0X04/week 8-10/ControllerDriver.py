'''@file        ControllerDiver.py
@brief          This file is what is used to define the maximum speed for the
                motor in both directions as well as modyfing the grain value 
                used to tune the accuracy of our closed loop controller system.
                
@details        This file is what is used to determine what speed the motor 
                should be spinning at. It starts with a default gain value of 1,
                and can be updated as needed by calling the function set_Kp. 
                In the run section, the duty cucle/PWM values are setup so that 
                if a function or user specifies a value greater than the maximum
                speed, the motor will just run at the maximum speed until the 
                value required is below the maximum threshold, at which point 
                the requested value will become the set value. A user can also 
                get the current PWM value by calling get_duty and the current 
                gain value by callingget _Kp.
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X04/week%208-10/ControllerDriver.py

@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
    
@package        controller                
@author         Davide Lanfranconi
@date           03/19/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''

## This file is what is used to define the maximum speed for the motor in both 
#  directions as well as modyfing the grain value used to tune the accuracy of 
#  our closed loop controller system.
#
#  Details
#  @author Davide Lanfranconi
#  @copyright Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved
#  @date March 19, 2021
class ClosedLoop():
    
    ## Constructor for closed loop
	#
	#  Detailed info on closed loop constructor
    def __init__(self):
        self.Kp = 1 #Default Kp Value
    
    ## Runs the closed loop script to update the needed motor PWM value regularly
	#
	#  Detailed info on closed loop run method
	#  @param Omega_Des The motor speed requested/wanted by the user 
	#  @param Omega_Mes The measured motor speed from the encoder       
    def run(self, Omega_Des, Omega_Meas):
        self.PWM = self.Kp * (Omega_Des - Omega_Meas)
        
        if self.PWM >= 100:
            self.PWM = 100
        if self.PWM <= -100:
            self.PWM = -100
        return self.P
    
    ## Sets the gain value for the closed loop control function
	#
	#  Detailed info on closed loop set_Kp method
	#
	#  @param RequestedKp The requested gain value for the closed loop controller  
    def set_Kp(self, RequestedKp):
        self.Kp = RequestedKp
        pass
    
    ## Gets the gain value for the closed loop control function
	#
	#  Detailed info on closed loop get_Kp method 
    def get_Kp(self):
        return self.Kp
        pass
    
    ## Gets the value of the current duty cucle or PWM value the motor is set at
	#
	#  Detailed info on closed loop get_duty method 
    def get_duty(self):
        return self.PWM
        pass