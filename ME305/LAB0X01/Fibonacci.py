'''@file    Fibonacci.py
@brief      Computes the Fibbonaci number for the requested index
@details    The function will return the Fibonacci number integer value for 
            a given zero or positive index.
@author     Davide Lanfranconi
@date       1/21/21   
'''


def fib (idx):  #defining our function name and input parameter name

    '''
    @brief This function computes a Fibonacci number at a specific index.
    @details The function is meant to be interactive, and it also has built
    in error detection, which will prompt you for a valid input if the input 
    value is not an integer or does not fall within the range specified for 
    the function. Additionally, the function will continue to run, so you can
    look up multiple Fibonacci numbers in a row without having to recall the 
    function each time. The function can be interrupted or stopped by a
    keyboard interrupt, or with the keystroke of Ctrl+C. The source code for 
    the function can be found at the following respository directory:
    https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB%200X01/Fibonacci.py    
    @param idx An zero or positive integer specifying the index of the desired 
    Fibonacci number
    @return The function returns the Fibonacci number at the specified index
    '''

    a, b, c = 0, 1, 0 # Defining the initial conditions for the function

    if idx == 0:
        return 0

    else:
        while c <= idx:
            d = a + b
            f = a
            a = b
            b = d
            c += 1
        return (f)

if __name__ == '__main__':   

    # The code below will run only when the script is executed as a standalone
    # program
 
    while True:
        try:
            idx = int(input("Please enter an integer: "))

            if idx < 0:
                print('Please enter a positive or zero index value')

            else:    
                print ('Fibonacci number at index {:} is {:}.'.format(idx,fib(idx)))
        
        except ValueError:  # will trigger when an invalid input such as a letter is entered
            print('Please enter a valid integer')
            
        except KeyboardInterrupt:  # will stop the program and display the print line below
            break 

    print('Thanks for playing the Fibonacci Math Machine :-)')
    
    