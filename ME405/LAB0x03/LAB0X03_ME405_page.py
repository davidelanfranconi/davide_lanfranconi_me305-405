'''@page        LAB0X03_ME405_page.py LAB0X03: Pushing the Right Buttons

@section LAB0X03_summary Summary
The purpose of this lab is to build a user interface to use a digital device to 
measure analog inputs, or in this case a button push. We will acomplish this 
with a simple user interface that will facilitate the communcation between a 
user and a laptop, which will be used to visualize how input voltage changes as 
a function of time.

@section LAB0X03_design Design Requirements
There are two parts which will be needed in this lab. The first parts is a 
front-end user interface while the second part will be a back-end data 
collection program. The front-end script will send a character to the back-end
script to signal the back-end to begin data collection. Once a set time passes 
or a voltage threshold is reached, the back-end will sends the data values back 
to the front-end, which will generates a voltage Vs. time plot and .csv file 
from the data. The front-end and back-end components will communicate with each 
other via UART serial communication.

@section LAB0X03_Voltage_Results Results
A plot of the button step response is shown in Figure 1 below.

@image html Voltage_Response_Plot.jpg "Figure 1: Voltage Over Time Plot for the button step response"

@section LAB0X03_Time_constant Time Constant Calculation
The theoretical calculation of the time constant can be found with the equation 
T = RC. From the Nucleo data sheet, we find the hardware values of R = 4.8 kOhms 
and C = 100 nF, which gives us a theoretical time constant of T = 0.48 ms. 
From the step response, the time constant can be calculated by taking the inverse 
of the slope from the best fit equation. I found the slope to be 2050.24 s, and 
the inverse of that value is 1/2050.24 = 0.488 ms, giving a percent 
difference of 1.67%. This is reasonable difference in our expected Vs. measured
value, since it is within the 5% tolerance of the ADC. This tells us that our
code and Nucleo is functioning correctly, and that all of the hardware components
used in this lab fall within the allowed tolerances.

@section LAB0X03_documentation Documentation
Documentation of this lab can be found here: main0X03_ME405.py and UI0X03.py

@section LAB0X03_source Source Code 
The source code and .csv file for this lab: 
https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305-405/src/master/ME405/LAB0x03/

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserved'''