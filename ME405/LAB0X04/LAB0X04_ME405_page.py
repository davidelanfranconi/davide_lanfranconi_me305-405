'''@page        LAB0X04_ME405_page.py LAB0X04: Hot or Not?

@section lab0x04_summary Summary
The scope of this lab is to use the I2C communication protocol to collect and 
log temperature data from the MCP9808 temperature sensor as well as the onboard
temperature sensor on the Nucleo STM32 board. The data collected over the course
of 8 hours will be logged in a .csv file which is accessible onve data collection 
is complete.

@section LAB0X04_I2C I2C Communications

The documentation and instructions below have been taken from the LAB0X04 instruction
guide written by Charlie Refvem.\n

The MCP9808 temperature sensor uses an I2C communication interface to communicate 
with the Nucleo STM32 microcontroller. A variety of sensors which are used in everyday 
applications use this interface. These sensors include, but are not limited to:
accelerometers, IMUs, humidity sensors, temperature sensors, electronic compasses 
\a etc. In this case, we are using a MCP9808 with a 3.3V, GND, and two I2C 
communication pins connected as shown in Figure 1 below.\n

@image html I2C_Schematic.jpg "Figure 1: The schematic for an I2C device connected to the Nucleo MCU (Source: LAB0x04 Handout)"

The I2C interface uses two wires to carry data to and from sensors or other
devices that work with a microcontroller. It is used over short distances,
typically less than a meter or so.\n\n
The I2C communication wires are:\n
- SDA or Serial Data, which carries data one bit at a time in any needed 
direction
- SCL or Serial Clock, a clock which synchronizes data transmission and 
reception\n
.
The I2C interface is a \a bus, meaning that the SDA and SCL lines may be 
shared between many devices. To distinguish between different devices, each 
device is assigned a 7-bit I2C bus address. The address may be set at the 
factory; many devices have address control pins which can be used to change 
the address, allowing several devices of one type to share an I2C bus. A 
few devices can have their addresses programmed by the user. An I2C 
communication begins with a \a start \a condition (I2C talk for "Hey!") and 
the microcontroller sending the 7-bit address of the device with which it 
will exchange data. Along with the address, an eighth bit indicates whether 
the microcontroller will write data to the device or read data from it. Then 
one or more bytes are exchanged.\n\n

Most I2C devices must communicate many numbers with the microcontroller -- 
for example, two bytes of temperature data, another two bytes of 
configuration, and so on. These numbers are kept in \a registers. After the 
microcontroller has transmitted a device address and the read/write bit, it 
transmits a \a register \a address, the location inside the device at which 
the number of interest is found. One or several bytes of data can be 
exchanged at a time. The image in Figure 2 below shows part of an I2C 
communication as viewed with a $13 logic analyzer.\n

@image html I2C_Logic_Analyzer_Signal.jpg "Figure 2: I2C communication waveform (Source: Lab 0x04 Handout)"

@section LAB0X04_design Design Requirements
1. A script for the microcontroller which measures the internal temperature
   of the microcontroller using the pyb.ADCAll class and saves it to a file. 
   A reference for the ADCAll class is 
   https://docs.micropython.org/en/latest/library/pyb.ADC.html.
2. A Python module called mcp9808.py containig a class which allows the user 
   to communicate with an MCP9808 temperature sensor using its I2C 
   interface. The class should contain the following methods:
   - An initializer which is given a reference to an already created I2C 
     object and the address of the MCP9808 on the I2C bus
   - A method \b check() which verifies that the sensor is attached at the 
     given bus address by checking that the value in the manufacturer ID 
     register is correct
   - A method \b celsius() which returns the measured temperature in degrees 
     Celsius
   - A method \b fahrenheit() which returns the measured temperature in 
     degrees Fahrenheit
3. A main.py which uses the mcp9808 driver module and code from step 1 to 
   take temperature readings approximately every sixty seconds from the 
   STM32 and from the MCP9808, and saves those readings in a CSV file on the 
   microcontroller. The CSV file’s columns should be time, STM32 
   temperature, and ambient (MCP9808) temperature. Data should be taken 
   until the user presses Ctrl+C, at which time the program should cleanly 
   exit with no errors.
4. Take data for a period of at least eight hours, then retrieve the data file
   from the microcontroller and plot it.

@section I2C_Logic_Analyzer_Signal_results Temperature Results

@image html Ambient_Temperature.jpg "Figure 3: Ambient Temperature as measured by the Nucleo STM32 Board and the MCP9808 temperature sensor between 9:00 PM and 5:00am on 6/6/21-06/07/21 in my garage in Cupertino, CA."

As can be seen in Figure 3, the Nucleo core temperature is consistently around 
3.5°C higher than the MCP9808 temperature. This is most likey attributed to the
processor generating and dissipating heat while it is functioning. 

@section lab0x04_documentation Documentation
Documentation of this lab can be found here: ADCA11.py, main0X04_ME405.py, and MCP9808.py

@section lab0x04_source Source Code 
The source code and .csv file for this lab: 
https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305-405/src/master/ME405/LAB0X04/

- - -
@author         Davide Lanfranconi
@copyright      Copyright © 2021 Davide Lanfranconi - All Rights Reserve'''