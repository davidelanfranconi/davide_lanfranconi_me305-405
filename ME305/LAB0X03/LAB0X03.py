'''@file        LAB0X03.py
@brief          Simon Says game: The LEDs on the Nucleo board blink in patterns
                and the user is prompted to repeat them using the user button.
                
@details        The program is an emulation of the Simon Says game. The game begins
                with a welcome message to the user explaining how to play. Within this
                sequence, there are a list of words that are formed using corresponding 
                patterns for letters. These patterns are based on the Morse Code 
                convention, using dots and dashes. A library is created with the entire
                Morse Code alphabet represented by dots and dashes. These dots and dashes
                are timed to the LED on the Nucleo board, and a corresponding 
                square wave PWM signal drives the LED on and off. The first "letter", e.g "S"
                of the word ("SOS") appears on the Nucleo and the user must copy the timing
                of the LED by rhythmically pressing the blue user button B2 with
                similar duration of the LED blinking of dashes and dots. 
                If the user gets it correct, the pattern of blinking LEDs shows the 
                "S"+"O" pattern. If the user gets it wrong, it flashes 
                the beginning letter of the next word, for example "MATH". If 
                the user replicates the "S"+"O"+"S" pattern, or even gets it wrong,
                they will be directed to the next word regardless. On the last word,
                getting it wrong directs the user to the game counter, which counts
                how many games the user has won and/or lost.  
                
                The program may be stopped at any time by using the 
                Ctrl+C keystroke. 
                
                The documented source code can be found at the following link:
                
                https://bitbucket.org/davidelanfranconi/davide_lanfranconi_me305/src/master/LAB0X03/LAB0X03.py
                
                A demonstration of the program can be viewed here:
               
                https://drive.google.com/file/d/1fIcHXqUKcT_35txdHGA3gGc3CzjSbuYV/view?usp=sharing
                
@image          html FSM-LAB0X03.jpg
@image          html LAB0X03-Commit-History.jpg
@author         Davide Lanfranconi, Tim Jain
@date           02/18/21
@copyright      Copyright (C) 2021 Davide Lanfranconi - All Rights Reserved   
'''
# Import modules needed to run code on Nucleo
import pyb
import time
from sys import exit

# Define timer and Pin locations and rename for quick reference
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13) 
ButtonAction = 0
Duration = 0
GoodRounds = 0
BadRounds = 0
r = 0
start = time.ticks_ms()
nwords = 3 #change according to # of words being used
word = [] #creates list to store the words that are being used
word.append('SOS')   #appends word to word list
word.append('MATH')
word.append('W6BHZ')

#function for displaying a Morse Code dash. One dash is equal to 1.5s (3 units)
def DisplayDash():
    '''@brief This function displays a single Morse Code dash
       @details One dash is equal to 3 units. 1 unit is defined here as 0.5 s
       time.ticks_ms() creates a timestamp, time.ticks_diff takes a difference
       of this time stamp. As long as this time difference is less than 1.5s, 
       the LED will be 100% on 
    '''
    start = time.ticks_ms()
    while time.ticks_diff(time.ticks_ms(), start) <= 1500:
        t2ch1.pulse_width_percent(100)
                    
    while time.ticks_diff(time.ticks_ms(), start) <= 2000:
        t2ch1.pulse_width_percent(0)

#function for displaying a Morse Code dot. One dot is equal to 0.5 s (1 unit)        
def DisplayDot():
    '''@brief This function displays a single Morse Code dot
       @details One dot is equal to 1 unit. 1 unit is defined here as 0.5 s
       time.ticks_ms() creates a timestamp, time.ticks_diff takes a difference
       of this time stamp. As long as this time difference is less than 500s, 
       the LED will be 100% on 
    '''
    start = time.ticks_ms()
    while time.ticks_diff(time.ticks_ms(), start) <= 500:
        t2ch1.pulse_width_percent(100)
                    
    while time.ticks_diff(time.ticks_ms(), start) <= 1000:
        t2ch1.pulse_width_percent(0)

#function for displaying the spacing in between letters in a word (3 units)        
def DisplayLetterSpace():
    '''@brief This function displays the spacing in between letters in a word
       @details This uses the Morse Code convention for spacing. It is defined
       as 3 units, which is equivalent to 1.5s
       time.ticks_ms() creates a timestamp, time.ticks_diff takes a difference
       of this time stamp. As long as this time difference is less than 1.5s, 
       the LED will be 100% off 
    '''
    start = time.ticks_ms()
    while time.ticks_diff(time.ticks_ms(), start) <= 1500:
        t2ch1.pulse_width_percent(0)

#function for displaying the spacing in between words (7 units)    
def DisplayWordSpace():
    '''@brief This function displays the spacing in between words
       @details This uses the Morse Code convention for spacing. It is defined
       as 7 units, which is equivalent to 3.5s
       time.ticks_ms() creates a timestamp, time.ticks_diff takes a difference
       of this time stamp. As long as this time difference is less than 3.5s, 
       the LED will be 100% off 
    '''
    start = time.ticks_ms()
    while time.ticks_diff(time.ticks_ms(), start) <= 3500:
        t2ch1.pulse_width_percent(0)        

#function for the user button. It is the callback function when the button is pressed    
def ButtonPressIsPattern(IRQ_src):
    '''@brief This function is the callback function for when the button is pressed
       @details This takes in a parameter IRQ_src which recognizes the falling or
       rising edge. Button Action is a variable that indicates when the button is 
       pressed
       @param IRQ_src
    '''
    global ButtonAction
    ButtonAction = 1

 
def ButtonBehavior():
    '''@brief This function defines the behavior of the button
       @details Variables are defined for the rising and falling edges. The
       duration is dependent on when the button is started to be pressed and when
       it is released.    
   '''
    global StartButton
    global falling
    global rising
    global Duration
    global dot
    global dash
    #falling edge
    if falling:
        StartButton = time.ticks_ms()
        falling = False
        return
    #rising edge
    elif rising:
        falling = True
        StopButton = time.ticks_ms()
        Duration = StopButton - StartButton

#External interrupt for pyboard for blue user button
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,pull=pyb.Pin.PULL_NONE, callback=ButtonPressIsPattern)

#end game function which prints how many rounds you played and how many of those
#you won and/or lost
def EndGame():
    global GoodRounds
    global BadRounds
    global k
    print('Thank you for playing. Out of ',r+1,' rounds, you won ',GoodRounds,' rounds and you lost ',BadRounds,' rounds.') 

#Morse code library for the alphabet representing letters with the predefined
#dots and dash functions       
def DisplayLetter(c, d):
    global char
    if c == 'A':
        if d: 
           DisplayDot()
           DisplayDash()
           DisplayLetterSpace()
        char = '01'
        return 2
        
    elif c == 'B':
        if d:
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '1000'
        return 4
        
    elif c == 'C':
        if d:
            DisplayDash()
            DisplayDot()
            DisplayDash()
            DisplayDot()
            DisplayLetterSpace()
        char = '1010'
        return 4
        
    elif c == 'D':
        if d:
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '100'
        return 3
        
    elif c == 'E':
        if d:
            DisplayDot()
            DisplayLetterSpace()
        char = '0'
        return 1
        
    elif c == 'F':
        if d:    
            DisplayDot()
            DisplayDot()
            DisplayDash()
            DisplayDot()
            DisplayLetterSpace()
        char = '0010'
        return 4
        
    elif c == 'G':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDot()
            DisplayLetterSpace()
        char = '110'
        return 3
        
    elif c == 'H':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '0000'
        return 4
        
    elif c == 'I':
        if d:    
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '00'
        return 2
        
    elif c == 'J':
        if d:
            DisplayDot()
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '0111'
        return 4
        
    elif c == 'K':
        if d:
            DisplayDash()
            DisplayDot()
            DisplayDash()
            DisplayLetterSpace()
        char = '101'
        return 3
        
    elif c == 'L':
        if d:    
            DisplayDot()
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '0100'
        return 4
        
    elif c == 'M':
        if d:    
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '11'
        return 2
        
    elif c == 'N':
        if d:
            DisplayDash()
            DisplayDot()
            DisplayLetterSpace()
        char = '10'
        return 2
        
    elif c == 'O':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '111'
        return 3
        
    elif c == 'P':
        if d:
            DisplayDot()
            DisplayDash()
            DisplayDash()
            DisplayDot()
            DisplayLetterSpace()
        char = '0110'
        return 4
        
    elif c == 'Q':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDot()
            DisplayDash()
            DisplayLetterSpace()
        char = '1101'
        return 4
        
    elif c == 'R':
        if d:
            DisplayDot()
            DisplayDash()
            DisplayDot()
            DisplayLetterSpace()
        char = '010'
        return 3
        
    elif c == 'S':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '000'
        return 3
        
    elif c == 'T':
        if d:
            DisplayDash()
            DisplayLetterSpace()
        char = '1'
        return 1
        
    elif c == 'U':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDash()
            DisplayLetterSpace()
        char = '001'
        return 3
        
    elif c == 'V':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDash()
            DisplayLetterSpace()
        char = '0001'
        return 4
        
    elif c == 'W':
        if d:
            DisplayDot()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '011'
        return 3
        
    elif c == 'X':
        if d:    
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayDash()
            DisplayLetterSpace()
        char = '1001'
        return 4
        
    elif c == 'Y':
        if d:
            DisplayDash()
            DisplayDot()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '1011'
        return 4
        
    elif c == 'Z':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '1100'
        return 4   

    elif c == '0':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '11111'
        return 5
    
    elif c == '1':
        if d:
            DisplayDot()
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '01111'
        return 5
    
    elif c == '2':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '00111'
        return 5
    
    elif c == '3':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDash()
            DisplayDash()
            DisplayLetterSpace()
        char = '00011'
        return 5
    
    elif c == '4':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDash()
            DisplayLetterSpace()
        char = '00001'
        return 5
    
    elif c == '5':
        if d:
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '00000'
        return 5
    
    elif c == '6':
        if d:
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '10000'
        return 5
    
    elif c == '7':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '11000'
        return 5
    
    elif c == '8':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayDot()
            DisplayDot()
            DisplayLetterSpace()
        char = '11100'
        return 5
    
    elif c == '9':
        if d:
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayDash()
            DisplayDot()
            DisplayLetterSpace()
        char = '11110'
        return 5   

t2ch1.pulse_width_percent(0) # Set LED brighness to 0 before starting program
rising = True
falling = True
dot = False
dash = False

#Testing loop for the Finite State Machine

print('Welcome to the "Simon Says" game. Please pay attention to the green LED. '
      'This is where you will see the pattern, and you have to repeat it with '
      'the blue button. A dot is a short press, less than 0.5 seconds, while a'
      ' dash is anything longer than 0.5 seconds.')

print('You will get a print message on the screen if you get the pattern' 
      ' correct, and if you complete the pattern you will have won the round.')
      
print('If you get the pattern wrong, you will be asked if you want to play'
      ' another round, which will start a new pattern. If you answer no, the '
      'game will end and you will get a report of rounds played and rounds'
      ' won or lost.')

input('Please press ENTER when you are ready to begin.')

while True:
    try:
        #word counter
        k = 0
        #Ensures that the word counter does not exceed the number of 
        #words defined previously 
        while k <= nwords:
            i = 0
            userdot = 0
            userdash = 0
            StopButton = 0
            DisplayChar = True
            ReadChar = True
            FirstTime = True
            #while the index of the word is less than the length of the word,
            #the character will be displayed
            while i < len(word[k]):
                if DisplayChar:
                    x = 0
                    #displays the letter of the designated word
                    while x <= i:
                        L = DisplayLetter(word[k][x:x+1], True)
                        x += 1
                    print('Please enter the pattern with the Blue button.')
                    DisplayChar = False
                    x =0
                    while x <= i:
                        SubChar = 0
                        bad = False
                        L = DisplayLetter(word[k][x:x+1], False)
                        while SubChar < L:
                            if ButtonAction == 1:
                                ButtonBehavior()
                                ButtonAction = 0
                            if Duration > 0:
                                if Duration <= 500:
                                    Duration = 0
                                    dot = True
                                    dash = False
                                    if char[SubChar] != '0':
                                        bad = True
                                else:
                                    Duration = 0
                                    dash = True
                                    dot = False
                                    if char[SubChar] != '1':
                                        bad = True
                                SubChar += 1
                    
                       
                        x += 1
                i += 1
                if not bad:
                    print('Congratulations, sequence was correct. :-)')
                    DisplayChar = True
                else:
                    print('Bummer! Sequence was incorrect. :-(')
                                       
            while True:
                if bad:
                    print('You lost this round, would you like to play another?')
                    answer = input('Please enter Y or N.')
                    BadRounds += 1
                else:
                    print('You won this round, would you like to play another?')
                    answer = input('Please enter Y or N.')
                    GoodRounds += 1
                if answer == 'Y':
                    k += 1
                    r += 1
                    break
                elif answer == 'N':
                    EndGame()
                    exit()
                else:
                    print('Invalid input, please enter Y or N.')
            if k == nwords:
                k = 0   
        
    except KeyboardInterrupt:  # will stop the program with a control-C interrupt and display the print line below
        EndGame()   
        t2ch1.pulse_width_percent(0)  # Will set LED brightness to 0 before ending program
        break 